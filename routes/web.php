<?php
//Route::redirect('/admin', 'admin/admin');

Auth::routes(['register' => false]);
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
// Change Password Routes...

Route::get('/', 'KonsultasiController@index')->name('home');

Route::get('/sub_pendidikan', 'KonsultasiController@sub_pendidikan');



Route::get('save-perjanjian/konsul_lansung', 'KonsultasiController@konsul_lansung');

Route::get('/sub-kategori/{id}', 'KonsultasiController@sub_kategori');

Route::get('/home-test', 'KonsultasiController@index_test')->name('home_test');

Route::get('/layanan-konsultasi', 'KonsultasiController@layanan_konsultasi')->name('layanan_konsultasi');

Route::get('/sukses/{id_laporan_klien}/{name}', 'KonsultasiController@sukses')->name('sukses');
//Route::post('/sukses/{id_laporan_klien}/{name}', 'KonsultasiController@sukses');

Route::get('/profil-psikologi/{id_psikologi}', 'KonsultasiController@profil_psikologi');
Route::get('/bayar','BayarController@render');

Route::get('/test','KonsultasiController@test');

Route::get('/kategori-konsul', 'KonsultasiController@kategoriKonsul')->name('kategori-konsul');
Route::get('/kategori-konsul/order/{name_konsul}', 'KonsultasiController@orderKonsul')->name('order');

Route::get('/masuk-otp', 'KonsultasiController@masuk_otp')->name('masukotp');
Route::post('/masuk-otp', 'KonsultasiController@masuk_otp_post');

Route::get('/pilih-tanggal-order/{tanggal}/{id_psikologi}', 'KonsultasiController@pilihTanggalOrder');
Route::get('/kategori-konsul/order/perjanjian/{id_psikologi}/{name}', 'KonsultasiController@perjanjian');

Route::get('/cari-psikologi/{di_sub_kategori}', 'KonsultasiController@cari_psikologi');


Route::get('/kategori', 'KonsultasiController@kategori');

Route::get('/cari-psikolog', 'KonsultasiController@psikolog');



Route::get('/save-perjanjian/{name}', 'KonsultasiController@savePerjanjian')->name('order.konsul');

Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');
Route::post('/login-user', 'AuthController@login_user');
Route::post('/register-user', 'AuthController@register_user');

Route::middleware('guest')->group(function() {
    Route::get('/register', 'AuthController@register')->name('register');
    Route::post('/register', 'AuthController@register_post');

    Route::get('/otp/{otp}', 'AuthController@otp');

    Route::get('/register-psikologi', 'AuthController@registerPsikologi')->name('registerPsikologi');
    Route::post('/register-psikologi', 'AuthController@register_psikologi_post');

    Route::get('/psikolog', 'AuthController@loginPsikologi')->name('psikolog');
    Route::post('/psikolog', 'AuthController@loginPsikologi_post'); 
});
Route::group(['middleware' => ['auth'], 'prefix' => 'klien', 'as' => 'klien.'], function () {
    Route::get('/data-profil', 'Klien\KlienController@data_profil')->name('data_profil');
    Route::get('/update-profil', 'Klien\KlienController@update_profil');
    Route::post('/update-profil', 'Klien\KlienController@update_profil_post');
    Route::get('/ubah-password', 'Klien\KlienController@ubah_password');
    Route::get('/pemesanan/{name}', 'Klien\KlienController@pemesanan');
    Route::post('/pemesanan/{name}', 'Klien\KlienController@pemesanan');
    Route::get('/bayar','Klien\KlienController@bayar');
});
Route::group(['middleware' => ['auth'], 'prefix' => 'psikolog', 'as' => 'psikolog.'], function () {

    Route::get('/profil', 'Psikolog\PsikologController@profil');

    Route::post('/upload-img', 'Psikolog\PsikologController@upload_img');

    Route::get('/data-profil', 'Psikolog\PsikologController@data_profil')->name('data_profil_psikolog');
    
    Route::post('/update-profil-user', 'Psikolog\PsikologController@update_profil_user_post');

    Route::get('/update-profil', 'Psikolog\PsikologController@update_profil')->name('update.profil');
    Route::post('/update-profil', 'Psikolog\PsikologController@update_profil_post');

    Route::get('/ubah-password', 'Psikolog\PsikologController@ubah_password');

    Route::get('/kategori', 'Psikolog\PsikologController@kategori')->name('kategori');
    Route::get('/delete/{id}', 'Psikolog\PsikologController@kategoriDelete')->name('kategoriDelete');

    Route::get('/create-kategori', 'Psikolog\PsikologController@create_kategori')->name('create_kategori');
    Route::post('/create-kategori', 'Psikolog\PsikologController@create_kategori_post');

    Route::get('/data_klien', 'Psikolog\PsikologController@dataKlien')->name('data_klien');

    Route::get('/jadwal-konsul', 'Psikolog\PsikologController@jadwal_konsul')->name('jadwal_konsul');
    Route::get('/create-jadwal', 'Psikolog\PsikologController@create_jadwal');

    Route::get('/create-jam/{jam}/{hari}', 'Psikolog\PsikologController@create_jam');
    Route::get('/delete-jam/{jam}/{hari}', 'Psikolog\PsikologController@delete_jam');
    
    Route::get('/ambil-jadwal', 'Psikolog\PsikologController@ambil_jadwal'); 
    
});
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    
    Route::post('/save-perjanjian/{name}', 'KonsultasiController@savePerjanjian');
    Route::get('/invoice/{id_laporan_klien}', 'KonsultasiController@invoice')->name('invoice.klien');
    Route::post('/invoice/{id_laporan_klien}', 'KonsultasiController@invoice');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/save-perjanjian/{name}', 'KonsultasiController@savePerjanjian')->name('order.konsul');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'HomeController@index')->name('admin');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::delete('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::delete('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::delete('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');

    Route::resource('transaksi', 'Admin\TransaksiController');
    Route::post('transaksi_mass_destroy', ['uses' => 'Admin\TransaksiController@massDestroy', 'as' => 'transaksi.mass_destroy']);
    Route::put('transaksi/{id}/restore', ['as' => 'TransaksiController.restore', 'uses' => 'Admin\TransaksiController@restore']);
    Route::put('transaksi/{id}/forcedelete', ['as' => 'TransaksiController.forcedelete', 'uses' => 'Admin\TransaksiController@forcedelete']);
    
    Route::get('/transaksi/views/{id}', 'Admin\TransaksiController@view')->name('transaksi.views');
    Route::get('/transaksi/detail/{id}', 'Admin\TransaksiController@detail')->name('transaksi.detail');
    Route::get('/transaksi/data-suda-bayar/{id}', 'Admin\TransaksiController@data_suda_bayar');

    Route::get('/kategori-psikolog', 'Admin\KategoriPsikologController@index');
    Route::get('/kategori-psikolog/views/{id}', 'Admin\KategoriPsikologController@view');

    Route::get('/pendapatan-psikolog', 'Admin\PendapatanPsikologController@index');
    Route::get('/pendapatan/views/{id}', 'Admin\PendapatanPsikologController@view')->name('pendapatan.views');
    Route::get('/pendapatan/detail', 'Admin\PendapatanPsikologController@detail');

    Route::get('/permintaan-psikolog', 'Admin\PermintaanPsikologController@index')->name('permintan.psikolog');
    Route::get('/permintaan-psikolog/terima/{id}', 'Admin\PermintaanPsikologController@terima');
    Route::get('/permintaan-psikolog/tolak/{id}', 'Admin\PermintaanPsikologController@tolak');
    Route::get('/permintaan-psikolog/profil', 'Admin\PermintaanPsikologController@profil');
});

// Route::group(['prefix' => 'agen', 'middleware' => ['AdminMiddleware']], function() {
//     Route::resource('/pemesanan', 'Admin\OrderController', ['as' => 'admin']);
// });