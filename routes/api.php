<?php
    use Illuminate\Http\Request;
    Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    
});
Route::post('payment','KonsultasiController@payment');

Route::get('cek-data-transaksi','TransaksiController@cek_transaksi');

Route::get('/reting', 'TransaksiController@reting');
