<?php


namespace App;

class Lib
{   
    public static function getNamaHari($date)
    {
        $namahari = date('D', strtotime($date));
        //Function date(String1, strtotime(String2)); adalah fungsi untuk mendapatkan nama hari
        return Lib::getHari($namahari);
    }


    public static function getHari($hari)
    {
        switch ($hari) {
            case 'Mon':
                return "Senin";
                break;
            case 'Tue':
                return "Selasa";
                break;
            case 'Wed':
                return "Rabu";
                break;
            case 'Thu':
                return "Kamis";
                break;
            case 'Fri':
                return "Jumat";
                break;
            case 'Sat':
                return "Sabtu";
                break;
            case 'Sun':
                return "Minggu";
                break;
        }
    }

    public static function getUmur($tgllahir)
    {
        $datetime1 = new \DateTime($tgllahir);
        $datetime2 = new \DateTime('now', new \DateTimeZone('UTC'));
        $diff = $datetime1->diff($datetime2);

        return $diff->y;

    }

    public static function getUmurDetail($tgllahir)
    {
        $awal = date_create($tgllahir);
        $akhir = date_create(); // waktu sekarang
        $diff = date_diff($awal, $akhir);

        return $diff->y . ' tahun ' . $diff->m . ' bulan ' . $diff->d . ' hari';
    }
    public static function getUmurHari($tgllahir)
    {
        $awal = date_create($tgllahir);
        $akhir = date_create(); // waktu sekarang
        $diff = date_diff($awal, $akhir);
        return $diff->d . ' hari';
    }

    public static function getUmurBulan($tgllahir)
    {
        $awal = date_create($tgllahir);
        $akhir = date_create(); // waktu sekarang
        $diff = date_diff($awal, $akhir);

        return $diff->m;
    }

    public static function rupiah($angka){
	
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
     
    }
}
?>