<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Biodata extends Model
{
    protected $table = "biodata";
    protected $primaryKey = 'id_biodata';
    protected $fillable = ['jenis_kelamin','alamat','status','nama','tanggal_lahir','id_user','img','jenis_identitas','no_identitas','tempat_lahir','agama','pendidikan_terakhir','status_perkawinan','pekerjaan','pendidikan_terakhir','no_hp'];

}