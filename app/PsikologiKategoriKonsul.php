<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class PsikologiKategoriKonsul extends Model
{
    protected $table = "psikologi_kategori_konsul";
    protected $primaryKey = 'id_psikologi_kategori_konsul';
    protected $fillable = ['id_psikologi','hari','jam','time_create','time_update','user_create','user_update'];
    
    public function psikologi(){
        return $this->hasOne('App\Psikologi','id_psikologi','id_psikologi');
    }
}