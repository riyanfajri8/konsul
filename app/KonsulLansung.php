<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class KonsulLansung extends Model
{
    protected $table = "konsul_lansung";
    protected $primaryKey = 'id_konsul_lansung';
}