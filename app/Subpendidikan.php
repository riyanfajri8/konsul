<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Subpendidikan extends Model
{
    protected $table = "sub_pendidikan";

}