<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Psikologi extends Model
{
    protected $table = "psikologi";
    protected $primaryKey = 'id_psikologi';

    public function biodata(){
        return $this->hasOne('App\Biodata','id_biodata','id_biodata');
    }
    public function pendidikan(){
        return $this->hasOne('App\Pendidikan','id_pendidikan','id_pendidikan');
    }

    public function subpendidikan(){
        return $this->hasOne('App\Subpendidikan','id_sub_pendidikan','id_sub_pendidikan');
    }
}