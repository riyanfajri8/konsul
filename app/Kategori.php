<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Kategori extends Model
{
    protected $table = "kategori_konsul";
    protected $fillable = ['name_konsul','harga','time_create','time_update','user_create','user_update'];

}