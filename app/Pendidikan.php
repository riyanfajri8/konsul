<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Pendidikan extends Model
{
    protected $table = "pendidikan";

    public function psikologi(){
        return $this->hasOne('App\Psikologi','id_pendidikan','id_pendidikan');
    }

    

}