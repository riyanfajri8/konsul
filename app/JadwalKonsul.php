<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class JadwalKonsul extends Model
{
    protected $table = "jadwal_konsul";
    protected $primaryKey = 'id_jadwal_konsul';
    
}