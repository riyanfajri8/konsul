<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Media extends Model
{
    protected $table = "media";
    protected $fillable = ['nama_media','harga','status','time_create','time_update','user_create','user_update'];

}