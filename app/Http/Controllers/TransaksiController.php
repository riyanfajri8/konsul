<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\Kategori;
use App\LaporanKlien;
use App\LaporanPsikologi;
use App\Lib;
use App\Media;
use App\Psikologi;
use App\PsikologiKategoriKonsul;
use App\Transaksi;
use App\User;
use Illuminate\Http\Request;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    public function cek_transaksi()
    {
        date_default_timezone_set('Asia/Jakarta');
        $cek = Transaksi::where('status_bayar','settlement')
                ->leftJoin('media','media.id_media','=','transaksi.id_media')
                ->where('jadwal','>=', date('Y-m-d'))
                ->where('reminder',null)
                ->get();
        foreach($cek as $cek1){
            $awal  = new \DateTime($cek1->jadwal.' '.$cek1->jam); //waktu awal
            $akhir = new \DateTime(date('Y-m-d H:i:s')); //waktu akhir
            $jumlah_jadwal = $awal->diff($akhir);
            
            if($jumlah_jadwal->h == 0 && $jumlah_jadwal->d == 0 && $cek1->reminder == ''){
                $transaksi = Transaksi::where('id_transaksi',$cek1->id_transaksi)->first();
                $transaksi->reminder = 1;
                $transaksi->save();

                $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();

                $psikologi = Psikologi::
                            leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')  
                            ->where('psikologi.id_psikologi',$transaksi->id_psikologi)->first();
                
                $kategori = DB::table('kategori_konsul_sub_kategori')->where('id_sub_kategori',
                $transaksi->id_kategori_konsul)->leftJoin('kategori_konsul','kategori_konsul.id_kategori_konsul','=','kategori_konsul_sub_kategori.id_kategori_konsul')
                ->first();

                $kategori_sub = DB::table('sub_kategori')->where('id_sub_kategori',
                $transaksi->id_kategori_konsul)
                ->first();

                $message = "Yth : ".$biodata1->nama. "\n\nReminder:\nJangan lupa ya hari ini kamu ada jawal konsultasi dengan psikolog 1 jam lagi.\n\nNama klien : ".$biodata1->nama."\nTanggal Konsultasi: ".date('d-M-Y', strtotime(date($cek1->jadwal)))."\nJam Konsultasi : ".$cek1->jam."\nNama Psikolog : ".$psikologi->nama. "\nMedia : ".$cek1->nama_media."\nKategori Konsultasi : ".$kategori->nama_konsul."\nSub Kategori : ".$kategori_sub->nama_sub_kategori."\nDetail Konsultasi : ".$transaksi->masalah."\n\nHarap untuk dapat melakukan konsultasi sesuai jadwal";
                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();

                $message = "Yth : ".$psikologi->nama. "\n\nReminder:\nJangan lupa ya hari ini kamu ada jawal konsultasi dengan klien 1 jam lagi.\n\nNama klien : ".$biodata1->nama."\nTanggal Konsultasi: ".date('d-M-Y', strtotime(date($cek1->jadwal)))."\nJam Konsultasi : ".$cek1->jam."\nNama Psikolog : ".$psikologi->nama. "\nMedia : ".$cek1->nama_media."\nKategori Konsultasi : ".$kategori->nama_konsul."\nSub Kategori : ".$kategori_sub->nama_sub_kategori."\nDetail Konsultasi : ".$transaksi->masalah."\n\nHarap untuk dapat melakukan konsultasi sesuai jadwal";

                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $psikologi->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();
            }
        }
        
        $cek = Transaksi::where('status_bayar','settlement')
                ->leftJoin('media','media.id_media','=','transaksi.id_media')
                ->where('jadwal','<', date('Y-m-d'))
                ->where('notif_reting',null)
                ->get();
        foreach($cek as $cek1){
            $transaksi = Transaksi::where('id_transaksi',$cek1->id_transaksi)->first();
            $transaksi->notif_reting = 1;
            $transaksi->save();

            $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();

            $psikologi = Psikologi::
                        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')  
                        ->where('psikologi.id_psikologi',$transaksi->id_psikologi)->first();

            $message = "Yth : ".$biodata1->nama. "\nKasih reting konsul anda untuk psikolog ".$psikologi->nama." dengan mengklik link https://konsul.yukbiz.com terimakasih";
            $version = new Version2X("http://109.106.255.28:8000/");
            $client = new Client($version);
            $client->initialize();
            $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
            $client->close();
        }
    }
    public function reting()
    {
        echo "d";
        exit;
    }
    public function opt_update(){
        $cek_token = User::where('id',$_GET['id'])->where('aktif',0)->first();
        $cek_token->otp = rand(10,10000);
        $cek_token->save();
        session()->flash('success','OTP di kirim ulang ke Whatsapp anda');
    }
}