<?php

namespace App\Http\Controllers\Klien;

use App\Biodata;
use Spatie\Permission\Models\Permission;
use App\Kategori;
use App\Lib;
use App\Media;
use App\Http\Controllers\Controller;
use App\JadwalKonsul;
use App\Psikologi;
use App\PsikologiKategoriKonsul;
use App\User;
use App\Transaksi;
use Illuminate\Http\Request;
use Auth;
use DB;


class KlienController extends Controller
{

    public function data_profil(){
        $biodata = Biodata::where('id_user',Auth::user()->id)->first();

        return view('konsul.user.data_profil',compact('biodata'));
    }

    public function update_profil(){
        $biodata = Biodata::where('id_user',Auth::user()->id)->first();

        return view('konsul.user.update_profil',compact('biodata'));
    }

    public function ubah_password(){
        $psikolog = Psikologi::
                    leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->where('psikologi.id_user',Auth::user()->id)->first();
        return view('konsul.user.ubah_password',compact('psikolog'));
    }

    public function kategori(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $kategori = Kategori::
                        leftJoin('kategori_konsul_sub_kategori','kategori_konsul_sub_kategori.id_kategori_konsul','=',
                        'kategori_konsul.id_kategori_konsul')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                        ->leftJoin('psikologi_kategori_konsul','psikologi_kategori_konsul.id_kategori_konsul','=','sub_kategori.id_sub_kategori')
                        ->leftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                        ->where('id_user', Auth::user()->id)
                        ->get();

        return view('konsul.psikologi.kategori',compact('psikolog','kategori'));
    }

    public function kategoriDelete($id){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        $psikologi_kategori = PsikologiKategoriKonsul::where('id_psikologi_kategori_konsul',$id)->first();
        $psikologi_kategori->delete();

        $sub_kategori = DB::table('sub_kategori')->get();

        return redirect()->route('psikolog.kategori');
    }

    public function dataKlien(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        return view('konsul.psikologi.data_klien',compact('psikolog'));
    }

    public function jadwal_konsul(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $hari = DB::table('hari')->get();
        
        $jadwal = JadwalKonsul::where('id_psikologi', $psikolog->id_psikologi)->get();

        return view('konsul.psikologi.jadwal_konsul',compact('psikolog','jadwal','hari'));
    }

    public function create_jadwal(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        return view('konsul.psikologi.create_jadwal',compact('psikolog'));
    }

    public function delete_jam($jam, $hari){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        
        $jadwal = JadwalKonsul::where('id_psikologi',$psikolog->id_psikologi)->where('jadwal',$jam)->where('hari',$hari)->first();

        $jadwal->delete();
        return 'berhasil';
    }

    public function create_jam($jam, $hari){
    
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        $cek = JadwalKonsul::where('id_psikologi',$psikolog->id_psikologi)->where('jadwal',$jam)->where('hari',$hari)->first();

        if(!empty($cek)){
            return 'jadwal di hari '.$hari.' dan di jam '.$jam.' sudah ada';
        }

        $jadwal = New JadwalKonsul();
        $jadwal->jadwal = $jam;
        $jadwal->hari = $hari;
        $jadwal->user_created = Auth::user()->id;
        $jadwal->id_psikologi = $psikolog->id_psikologi;
        $jadwal->save();
        return "berhasil";
    }

    public function ambil_jadwal(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $hari = DB::table('hari')->get();
        $jadwal = JadwalKonsul::where('id_psikologi', $psikolog->id_psikologi)->get();

        return view('konsul.psikologi.jadwal',compact('psikolog','jadwal','hari'));
    }

    public function create_kategori(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        $sub_kategori = DB::table('sub_kategori')->get();
        return view('konsul.psikologi.create_kategori',compact('psikolog','sub_kategori'));
    }

    public function create_kategori_post(Request $request){
        // $q->validate([
        //     'sub_kategori' => 'required',
        // ]);
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        foreach($_POST['sub_kategori'] as $data){
            $cek = PsikologiKategoriKonsul::where('id_psikologi',$psikolog['id_psikologi'])->where('id_kategori_konsul',$data)->first();

            if(!empty($cek)){
                $cek->id_psikologi =  $psikolog->id_psikologi;
                $cek->created_at = date('Y-m-d H:i:s');
                $cek->id_kategori_konsul = $data;
                $cek->harga = $_POST['harga'];
                $cek->user_create = Auth::user()->id;
                $cek->save();
            }else{
                $psikologi_kategori = New PsikologiKategoriKonsul();
                $psikologi_kategori->id_psikologi =  $psikolog->id_psikologi;
                $psikologi_kategori->created_at = date('Y-m-d H:i:s');
                $psikologi_kategori->id_kategori_konsul = $data;
                $psikologi_kategori->harga = $_POST['harga'];
                $psikologi_kategori->user_create = Auth::user()->id;
                $psikologi_kategori->save();
            }
            
        }
        
        

        $sub_kategori = DB::table('sub_kategori')->get();
        return redirect()->route('psikolog.kategori');
    }

    public function update_profil_post(Request $request){
        $request->validate([
            'nama' => 'required',
            'jenis_identitas' =>'required',
            'no_identitas' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'status_perkawinan' => 'required',
            'agama' =>'required',
            'pendidikan_terakhir' =>'required',
            'no_hp' => 'required',
            'pekerjaan' => 'required',
            'no_hp' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $user = User::find(Auth::user()->id);
            //$user->email = $email;
            //$user->password = Hash::make($password);
            //$user->updated_at = date('Y-m-d H:i:s');
            $user->name = $request->nama;
            $user->save();
            
            $biodata = Biodata::where('id_user', Auth::user()->id)->first();
            $biodata->alamat = $request->alamat;
            $biodata->pengalaman = $request->pengalaman;
            $biodata->universitas = $request->universitas;
            $biodata->no_hp = $request->no_hp;
            $biodata->pekerjaan = $request->pekerjaan;
            
            $biodata->nama = $request->nama;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
            $biodata->jenis_kelamin = $request->jenis_kelamin;

            $biodata->jenis_identitas = $request->jenis_identitas;
            $biodata->no_identitas = $request->no_identitas;
            $biodata->tempat_lahir = $request->tempat_lahir;
            $biodata->agama = $request->agama;
            $biodata->id_user = Auth::user()->id;
            $biodata->pendidikan_terakhir = $request->pendidikan_terakhir;
            $biodata->status_perkawinan = $request->status_perkawinan;
            
            $biodata->updated_at = date('Y-m-d H:i:s');

            if(!$biodata->save()){
                
            }
            //$biodata->save();

            DB::commit();
            session()->flash('success','Sukses Update Data Profil');
            return redirect()->route('klien.data_profil');
            

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }
    }

    public function update_profil_user_post(Request $request){

        DB::beginTransaction();
        try {
                if(empty($request->nama)){
                    return 'tidak lengkap';
                }
                if(empty($request->jenis_identitas)){
                    return 'tidak lengkap';
                }
                if(empty($request->no_identitas)){
                    return 'tidak lengkap';
                }
                if(empty($request->tempat_lahir)){
                    return 'tidak lengkap';
                }
                if(empty($request->tanggal_lahir)){
                    return 'tidak lengkap';
                }
                if(empty($request->jenis_kelamin)){
                    return 'tidak lengkap';
                }
                if(empty($request->status_perkawinan)){
                    return 'tidak lengkap';
                }
                if(empty($request->agama)){
                    return 'tidak lengkap';
                }
                if(empty($request->pendidikan_terakhir)){
                    return 'tidak lengkap';
                }
            
            
            $user = User::find(Auth::user()->id);
            //$user->email = $email;
            //$user->password = Hash::make($password);
            //$user->updated_at = date('Y-m-d H:i:s');
            $user->name = $request->nama;
            $user->save();
            
            $biodata = Biodata::where('id_user', Auth::user()->id)->first();
            // if( $_GET['ajax'] != 1){
            //     $pasikologi = Psikologi::where('id_user',Auth::user()->id)->first();
            //     $pasikologi->nama = $request->nama;;
            //     $pasikologi->save();

            //     $biodata = Biodata::where('id_biodata', $pasikologi->id_biodata)->first();
            //     $biodata->alamat = $request->alamat;
            //     $biodata->pengalaman = $request->pengalaman;
            //     $biodata->universitas = $request->universitas;
            //     $biodata->no_hp = $request->no_hp;
            //     $biodata->pekerjaan = $request->pekerjaan;
            // }
            
            $biodata->nama = $request->nama;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
            $biodata->jenis_kelamin = $request->jenis_kelamin;

            $biodata->jenis_identitas = $request->jenis_identitas;
            $biodata->no_identitas = $request->no_identitas;
            $biodata->tempat_lahir = $request->tempat_lahir;
            $biodata->agama = $request->agama;
            $biodata->id_user = Auth::user()->id;
            $biodata->pendidikan_terakhir = $request->pendidikan_terakhir;
            $biodata->status_perkawinan = $request->status_perkawinan;
            
            $biodata->updated_at = date('Y-m-d H:i:s');

            if(!$biodata->save()){
                
            }

            $psikolog = Psikologi::
                        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                        ->where('psikologi.id_user',Auth::user()->id)->first();
            DB::commit();
                return 'berhasil';
            

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }
    }

    public function pemesanan($name){
        if(empty(Auth::user()->id)){
            session()->flash('danger','Untuk Melanjutkan, Login Terlebih Dahulu');
            return redirect()->route('home');
        }
        date_default_timezone_set('Asia/Jakarta');

        if(!empty($_POST['result_data'])){

            $status = json_decode($_POST['result_data'],true);
            $transaksi = Transaksi::where('no_transaksi',$status['order_id'])->first();
            $transaksi->pdf_url = $status['pdf_url'];
            $transaksi->finish_redirect_url = $status['finish_redirect_url'];
            $transaksi->save();
    
        }
        
        $biodata = Biodata::where('id_user',Auth::user()->id)->first();
        $laporan_klien = DB::table('laporan_klien')
                    ->select('transaksi.*','psikologi.nama as nama_psikolog','sub_kategori.nama_sub_kategori','users.name','media.nama_media','laporan_klien.total_konsul','transaksi.created_at','transaksi.status_konsul')
                    ->leftJoin('psikologi','psikologi.id_psikologi','=','laporan_klien.id_psikologi')
                    ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','laporan_klien.id_kategory')
                    ->leftJoin('users','users.id','=','laporan_klien.id_user')
                    ->leftJoin('transaksi','transaksi.id_transaksi','=','laporan_klien.id_transaksi')
                    ->leftJoin('media','media.id_media','=','transaksi.id_media')
                    ->where('laporan_klien.id_user',Auth::user()->id)
                    ->orderBy('created_at','desc')
                    ->get();
        foreach($laporan_klien as $klien){
            $awal  = new \DateTime($klien->created_at); //waktu awal
            $akhir = new \DateTime(date('Y-m-d H:i:s')); //waktu akhir
            $waktu_transaksi = $awal->diff($akhir);
            if($waktu_transaksi->h > 3 && $klien->status_bayar == 'pending'){
                $transaksi_klien = Transaksi::where('id_transaksi',$klien->id_transaksi)->first();
                $transaksi_klien->status_bayar = 'expire';
                if(!$transaksi_klien->save()){
                    echo '<pre>';
                    print_r($transaksi_klien);
                    exit;
                }
                
            }
        }
        return view('konsul.user.pesanan',compact('laporan_klien','biodata'));
    }

    public function bayar(){
        
        $transaksi = DB::table('transaksi')
                    ->leftJoin('media','media.id_media','=','transaksi.id_media')
                    ->where('no_transaksi',$_GET['no_transaksi'])->first();

        $laporan_klien = DB::table('laporan_klien')
                        ->leftJoin('psikologi','psikologi.id_psikologi','=','laporan_klien.id_psikologi')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','laporan_klien.id_kategory')
                        //->leftJoin('users','users.id','=','laporan_klien.id_user')
                        ->where('id_transaksi',$transaksi->id_transaksi)->first();

        \Midtrans\Config::$serverKey = 'SB-Mid-server-9FNvZL-HfHUT948G6vYe5C1B';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $biodata = DB::table('biodata')->where('id_user',Auth::user()->id)->first();
        
        $params = array(
            'transaction_details' => array(
                'order_id' => $transaksi->no_transaksi,
                'gross_amount' => $laporan_klien->total_konsul,
            ),
            'customer_details' => array(
                'first_name' => Auth::user()->name,
                'last_name' => '',
                'email' => Auth::user()->email,
                'phone' => $biodata->no_hp,
            ),
        );
        $snapToken = \Midtrans\Snap::getSnapToken($params);
        return $snapToken;
    }
     
}
