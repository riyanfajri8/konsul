<?php

namespace App\Http\Controllers\Admin;

use App\Transaksi;
use App\Kategori;
use App\Psikologi;
use App\PsikologiKategoriKonsul;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Image;
use DB;

class PendapatanPsikologController extends Controller
{
    /**
     * Display a listing of Brand.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = Transaksi::select('id_psikologi')->groupBy('id_psikologi')->get();

        return view('admin.pendapatan.index', compact('transaksi'));
    }

    public function view($id){
        
        $psikolog = Psikologi::where('id_psikologi',$id)->first();

        return view('admin.pendapatan.views', compact('psikolog','id'));
    }

    public function detail(){
        $id = $_GET['id'];
        $dateForm = $_GET['dateForm'];
        $dateTo = $_GET['dateTo'];
        $psikolog = Psikologi::where('id_psikologi',$id)->first();
        $transaksi = Transaksi::select('id_psikologi')
                    ->where('status_bayar','settlement')
                    ->where('id_psikologi',$id)->groupBy('id_psikologi')
                    ->get();

        $list = Transaksi::leftJoin('laporan_klien','transaksi.id_transaksi','=','laporan_klien.id_transaksi')->where('status_bayar','settlement')->where('laporan_klien.id_psikologi',$id)
                    ->where('jadwal','>=',$dateForm)
                    ->where('jadwal','<=',$dateTo)
                    ->get();

        return view('admin.pendapatan.detail', compact('kategori','id','transaksi','dateForm','dateTo','list'));
    }


    /**
     * Show the form for creating new Brand.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $id_topik = Transaksi::get()->pluck('name', 'id')->prepend('Please select', '');
        return view('admin.transaksi.create',compact('id_topik'));
    }

    public static function autonumber(){
        //$barang = "TN";
        $datet=date('y-m-d');
        $code_tahun = Keuangan::orderBy('code', 'desc')->LIMIT(1)->first();
        if($code_tahun==null){

        }else{
        $thn_akhir = $code_tahun->code;
        $ambil_thn = substr($thn_akhir,0,5);
        }
        
        date_default_timezone_set('Asia/Jakarta');
        $date = date('y');
        $tahn = substr($date,-2);
        
        $primary = "code";
        $prefix = "UA-";
        $q=Keuangan::select(DB::raw('MAX(RIGHT('.$primary.',8)) as kd_max '));
        $prx = "UA-".$tahn;


        if($q->count()>0)
        {
            if($prx==$ambil_thn)
            {
                $unk="UA-";
                foreach($q->get() as $k)
                {
                $tmp = ((int)$k->kd_max)+1  ;
                $kd = $unk.sprintf("%06s", $tmp);
                }
            }else{
                $kd = $prx."000001";
            }
            
        }
        else
        {
            $kd = $prx."000001";
        }

        return $kd;
    }

    /**
     * Brand a newly created Brand in storage.
     *
     * @param  \App\Http\Requests\BrandBrandsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $number= $this->autonumber();
        $keuangans = new Keuangan();
        $keuangans->created_by = $request->input('created_by');
        $keuangans->id_topik = $request->input('id_topik');
        $nameimg = $request->input('judul');

        $file = $request->file('img');

            if($file != ""){
                $year_folder = date("Y");
                $month_folder = date("m");
                $day_file = date("d_h_i_s");
                $path = '/uploads/keuangans/'. $year_folder . '/' . $month_folder .'/';
                    
                    if (!file_exists(public_path().$path)) { 
                        mkdir(public_path() .$path, 0777, true); 

                    }
                $ext = $file->getClientOriginalExtension();
                $fileName = strtolower($nameimg . '_' . $day_file .  '.' .$ext);
                $image = Image::make($request->file('img'));
                $keuangans->img = $path . $fileName ;
                $image->save(public_path().$path. $fileName);
                //$path = public_path('uploads/' . $fileName);
                //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            }


        $keuangans->judul = $request->input('judul');
        $keuangans->code = $number;
        $keuangans->name = $request->input('name');
        $keuangans->save();

         $id = Keuangan::where('code',$number)->first();

        foreach ($request->input('isi') as $isi) {
            if($isi == null){

            }else{
                $isinews = new Isinew();
                $isinews->isi = $isi;
                $isinews->id_keuangan = $id->id;
                $isinews->save();
            }

        }

        return redirect()->route('admin.keuangans.index');
    }


    /**
     * Show the form for editing Brand.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $keuangans = Keuangan::findOrFail($id);
        $isi = Isinew::where('id_keuangan',$id)->get();

        $id_topik = Topik::get()->pluck('name', 'id')->prepend('Please select', '');

        return view('admin.keuangans.edit', compact('keuangans','isi','id_topik'));
    }

    /**
     * Update Brand in storage.
     *
     * @param  \App\Http\Requests\UpdateBrandsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $keuangans = Keuangan::findOrFail($id);
        $keuangans->updated_by = $request->input('created_by');
        $keuangans->id_topik = $request->input('id_topik');
        $nameimg = $request->input('judul');

        $file = $request->file('img');

            if($file != ""){
                $year_folder = date("Y");
                $month_folder = date("m");
                $day_file = date("d_h_i_s");
                $path = '/uploads/keuangans/'. $year_folder . '/' . $month_folder .'/';
                    
                    if (!file_exists(public_path().$path)) { 
                        mkdir(public_path() .$path, 0777, true); 

                    }
                $ext = $file->getClientOriginalExtension();
                $fileName = strtolower($nameimg . '_' . $day_file .  '.' .$ext);
                $image = Image::make($request->file('img'));
                $keuangans->img = $path . $fileName ;
                $image->save(public_path().$path. $fileName);
                //$path = public_path('uploads/' . $fileName);
                //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            }


        $keuangans->judul = $request->input('judul');
        $keuangans->name = $request->input('name');
        $keuangans->save();

        $cek = Isinew::where('id_keuangan',$id)->pluck('id');
        $cek1 = $request->input('idisi');
        $cek2 =null;

           foreach($cek as $cek){
            $cek2[] = $cek;  
        }

        if($cek1 != null){

        $TampungArray = array_diff($cek2,$cek1);
        foreach ($TampungArray as $TampungArray) {
            $isinews = Isinew::findOrFail($TampungArray);
            $isinews->forceDelete();
        }

         $no = -1;
            foreach($request->input('idisi') as $idisi ) {
                $no++;
                
                if($idisi == null){

                }else{
                     DB::table('isinews')
                        ->where('id', $idisi)
                        ->update(['isi' => $request->input('isi')[$no]]);
                }
            }

        }else{
                if($cek2 !=null){
                    foreach ($cek2 as $cek2) {
                    $isinews = Isinew::findOrFail($cek2);
                    $isinews->forceDelete();
                }
            }
        }
        


            foreach($request->input('isi1') as $isi1 ){
            if($isi1 == null){

            }else{
                $isinews = new Isinew();
                $isinews->isi = $isi1;
                $isinews->id_keuangan = $id;
                $isinews->save();
            }

        }

        return redirect()->route('admin.keuangans.index');
    }


    /**
     * Display Brand.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        

        return view('admin.keuangans.show', compact('keuangans'));
    }


    /**
     * Remove Brand from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $keuangans = Keuangan::findOrFail($id);
        $keuangans->delete();

        return redirect()->route('admin.keuangans.index');
    }

    /**
     * Delete all selected Brand at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {

        if ($request->input('ids')) {
            $entries = Keuangan::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    public function restore($id)
    {

        $keuangans = Keuangan::withTrashed()->findOrFail($id);
        $keuangans->restore();
        

        return redirect()->route('admin.keuangans.index');
    }


    public function forcedelete($id)
    {
        $keuangans = Keuangan::withTrashed()->findOrFail($id);
        $keuangans->forceDelete();
        

        return redirect()->route('admin.keuangans.index');
    }

}
