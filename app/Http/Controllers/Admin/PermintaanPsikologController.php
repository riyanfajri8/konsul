<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Kategori;
use App\Lib;
use App\PsikologiKategoriKonsul;
use App\Psikologi;
use App\Media;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;


class PermintaanPsikologController extends Controller
{
    public function index(){
        $permintaan_psikolog = Psikologi::orwhere('status','!=','aktif')->get();

        return view('admin.permintaan_psikolog.index', compact('permintaan_psikolog'));
    }
    public function terima($id){
        $permintaan_psikolog = Psikologi::where('status','pending')->where('id_psikologi',$id)->first();
        $permintaan_psikolog->status="aktif";
        $permintaan_psikolog->save();
    }
    public function tolak($id){
        $permintaan_psikolog = Psikologi::where('status','pending')->where('id_psikologi',$id)->first();
        $permintaan_psikolog->status="tolak";
        $permintaan_psikolog->save();
    }
    public function profil(){
        $id_psikologi = $_GET['id'];
        $psikologi = Psikologi::
                    leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->where('psikologi.id_psikologi',$_GET['id'])
                    ->first();
                    
        $kategori_psi =  PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi','nama_sub_kategori')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->where('psikologi_kategori_konsul.id_psikologi',$_GET['id'])
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','nama_sub_kategori')
                    ->get();
        // echo "<pre>";
        // print_r($psikologi);
        // exit;
        return view('admin.permintaan_psikolog.profil_psikologi',compact('psikologi','id_psikologi','kategori_psi'));

    }
}