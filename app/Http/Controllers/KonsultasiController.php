<?php

namespace App\Http\Controllers;

use App\Biodata;
use App\LaporanKlien;
use App\LaporanPsikologi;
use App\Lib;
use App\Kategori;
use App\Media;
use App\Psikologi;
use App\PsikologiKategoriKonsul;
use App\Transaksi;
use App\JadwalKonsul;
use App\User;
use App\KonsulLansung;
use Illuminate\Http\Request;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;

class KonsultasiController extends Controller
{
    public function index()
    {
  
        $version = new Version2X("https://www.psyqonsul.com:4000");
        $client = new Client($version);
        $client->initialize();
        $client->emit('send data', ['nomor' => 'Ada Pesan Dari Velotow Coba Dicek','id_unik'=>'18259325dce4256a']);
        $client->close();

        if(!empty($_GET['coba'])){
            // $transaksi = Transaksi::
            // leftJoin('media','media.id_media','=','transaksi.id_media')
            // ->leftJoin('laporan_klien','laporan_klien.id_transaksi','=','transaksi.id_transaksi')
            // ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','transaksi.id_kategori_konsul')
            // ->where('no_transaksi','TRN20210622100654')->first();
            // echo "<pre>";
            // print_r($transaksi);
            // exit;
        }
        

        $kategori3 = Kategori::get();
        $psikologi = PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi','psikologi.sip','biodata.universitas','psikologi.pengalaman')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->where('psikologi.status','aktif')
                    ->LeftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','psikologi.sip','biodata.universitas','psikologi.pengalaman')
                    //->limit(4)
                    ->get();
        
        if(!empty($_GET['reting'])){
            $reting = 0;
            if($_GET['reting'] == 4){
                $reting = 1;
            }
            if($_GET['reting'] == 3){
                $reting = 2;
            }
            if($_GET['reting'] == 2){
                $reting = 3;
            }
            if($_GET['reting'] == 1){
                $reting = 4;
            }

            $transaksi = Transaksi::kasih_reting();
            $transaksi->reting = $reting;
            $transaksi->notif_reting = 1;
            $transaksi->save();
        }

        return view('konsul.home.home',compact('kategori3','psikologi'));
    }

    public function sub_pendidikan(){
        $sub_pendidikan = DB::table('sub_pendidikan')->where('id_pendidikan',$_GET['id_pendidikan'])->get();
        if(count($sub_pendidikan) > 0){
            if(!empty($_GET['id_sub_pendidikan'])){
                $id_sub_pendidikan = $_GET['id_sub_pendidikan'];
                return view('konsul.sub_pendidikan',compact('sub_pendidikan','id_sub_pendidikan'));
            }else{
                return view('konsul.sub_pendidikan',compact('sub_pendidikan'));
            }
            
        }
        
    }

    public function kategori_sub($id_kategori){
        exit;
        // if(!empty($_GET['id_kategori'])){
        //     $name_sub_kategori = DB::table('kategori_konsul_sub_kategori')
        //         ->leftJoin('sub_kategori','sub_kategori','=','kategori_konsul_sub_kategori.sub_kategori')
        //         ->where('id_kategori_konsul',$_GET['id_kategori'])->all();
        //     return view('konsul.kategori-konsul.subkategori',compact('name_sub_kategori'));
        // }
    }

    public function index_test()
    {
        return view('konsul.home.home_test');
    }

    public function layanan_konsultasi()
    {
        return view('konsul.home.layanan_konsultasi');
    }

    public function kategoriKonsul(){
        $kategori = Kategori::get();
        return view('konsul.kategori-konsul.index',compact('kategori'));
    }
    public function orderKonsul($name_konsul){
        
        if(empty(Auth::user()->id)){
            session()->flash('danger','Untuk Melanjutkan, Login Terlebih Dahulu');
            return redirect()->route('home');
        }
        $kategoripilih = DB::table('kategori_konsul_sub_kategori')->where('id_sub_kategori',$name_konsul)->first();
        $kategori = Kategori::get();

        $name_sub_kategori = DB::table('sub_kategori')->where('id_sub_kategori',$name_konsul)->first();

        $psikologi = PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->where('psikologi.status','aktif')
                    ->where('sub_kategori.id_sub_kategori',$name_konsul)
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi')
                    ->get();
        
        if(!empty($_GET['gejala'])){
            $gejala = $_GET['gejala'];
        }else{
            $gejala = '';
        }
                    
        return view('konsul.kategori-konsul.order',compact('kategoripilih','gejala','psikologi','kategori','name_konsul','name_sub_kategori'));
    }
    public function pilihTanggalOrder($tanggal,$id_psikologi){
        
        if(empty(Auth::user()->id)){
            session()->flash('danger','Untuk Melanjutkan, Login Terlebih Dahulu');
            return redirect()->route('home');
        }
        date_default_timezone_set('Asia/Jakarta');

        $hari = Lib::getNamaHari($tanggal);

        if(date("Y-m-d", strtotime($tanggal)) <= date('Y-m-d')){
            return 'Tanggal tidak boleh mundur dari hari sekarang, tanggal harus lewat dari hari ini';
        }else{
            
        }
        if($hari == 'Senin'){
            $hari = 'Senen';
        }
        $psikologi = DB::table('jadwal_konsul')
                    ->select('jadwal_konsul.id_jadwal_konsul','psikologi.foto','psikologi.nama','psikologi.id_psikologi','jadwal_konsul.jadwal','jadwal_konsul.jadwal_akhir','jadwal_konsul.harga','jadwal_konsul.durasi')
                    ->leftJoin('psikologi','jadwal_konsul.id_psikologi','=','psikologi.id_psikologi')
                    ->where('jadwal_konsul.hari',$hari)
                    ->where('psikologi.status','aktif')
                    ->where('jadwal_konsul.id_psikologi',$id_psikologi)
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','jadwal_konsul.jadwal','jadwal_konsul.id_jadwal_konsul','jadwal_konsul.jadwal_akhir','jadwal_konsul.harga','jadwal_konsul.durasi')
                    ->get();
        
        if(count($psikologi) > 0){
            if(!empty($_GET['durasi'])){
                $id_jadwal_konsul = $_GET['id_jadwal_konsul'];
                $psikologi = PsikologiKategoriKonsul::
                    select('jadwal_konsul.id_jadwal_konsul','psikologi.foto','psikologi.nama','psikologi.id_psikologi','jadwal_konsul.jadwal','jadwal_konsul.jadwal_akhir','jadwal_konsul.harga','jadwal_konsul.durasi')
                    ->leftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->leftJoin('jadwal_konsul','jadwal_konsul.id_psikologi','=','psikologi.id_psikologi')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->where('jadwal_konsul.hari',$hari)
                    ->where('jadwal_konsul.id_jadwal_konsul',$_GET['id_jadwal_konsul'])
                    ->where('psikologi.status','aktif')
                    ->where('jadwal_konsul.id_psikologi',$id_psikologi)
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','jadwal_konsul.jadwal','jadwal_konsul.id_jadwal_konsul','jadwal_konsul.jadwal_akhir','jadwal_konsul.harga','jadwal_konsul.durasi')
                    ->get();
                return view('konsul.psikologi.durasi',compact('psikologi','tanggal','id_jadwal_konsul'));
            }
            return view('konsul.psikologi.index',compact('psikologi','tanggal'));
        }else{
            return 'Tidak ada jadwal mohon pilih tanggal yang lain';
        }
        
    }

    public function perjanjian($id_psikologi,$name = null){
        if(empty(Auth::user()->id)){
            session()->flash('danger','Untuk Melanjutkan Login Terlebih Dahulu');
            return redirect()->route('home');
        }
        $masalah = '';
        if(!empty($_GET['masalah'])){
            $masalah = $_GET['masalah'];
        }
        $psikologi = PsikologiKategoriKonsul::
            select('psikologi.*')
            ->LeftJoin('kategori_konsul','kategori_konsul.id_kategori_konsul','=','psikologi_kategori_konsul.id_kategori_konsul')
            ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
            ->LeftJoin('users','users.id','=','psikologi.id_user')
            ->where('psikologi.status','aktif')
            ->where('psikologi_kategori_konsul.id_psikologi',$id_psikologi)
            ->first();

        $psikolog = Psikologi::
            leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
            ->where('psikologi.id_user',Auth::user()->id)->first();
        
        $kategori_sub = DB::table('sub_kategori')
                        ->leftJoin('psikologi_kategori_konsul','psikologi_kategori_konsul.id_kategori_konsul','=','sub_kategori.id_sub_kategori')
                        ->where('id_psikologi',$id_psikologi)
                        ->get();
        
        $kategoripilih = DB::table('kategori_konsul_sub_kategori')->where('id_sub_kategori',$name)->first();

        $kategori = Kategori::get();

        $biodata = Biodata::where('id_user',Auth::user()->id)->first();
        // echo "<pre>";
        // print_r($biodata);
        // exit;
        $media = Media::where('status','Aktif')->get();
        return view('konsul.psikologi.perjanjian',compact('psikologi','media','name','id_psikologi','biodata','psikolog','kategori_sub','masalah','kategori','kategoripilih'));
    }

    public function savePerjanjian(Request $request, $name){

        if(!empty($_GET['lansung'])){
            if(!empyt(Auth::user()->id)){
                $konsul_lansung = New KonsulLansung();
                $konsul_lansung->id_user = Auth::user()->id;
                $kosunl_lansung->media = $_GET['media'];
                $konsul_lansung->harga = 300000;
                $konsul_lansung->save();
                
            }else{

            }
        }

        if(empty(Auth::user()->id)){
            session()->flash('danger','Untuk Melanjutkan, Login Terlebih Dahulu');
            return redirect()->route('home');
        }

        date_default_timezone_set('Asia/Jakarta');
        DB::beginTransaction();

        try {
            $jadwal_konsul = JadwalKonsul::where('id_jadwal_konsul',$_GET['jadwalJam'])->first();

            $kategori = DB::table('sub_kategori')->where('id_sub_kategori',$name)->first();
            $media = Media::where('id_media',$_GET['media'])->first();
            $psikologi_kategori= DB::table('psikologi_kategori_konsul')->where('id_psikologi',$_GET['id_psikologi'])->where('id_kategori_konsul',$name)->first();
            $transaksi = new Transaksi();
            $transaksi->no_transaksi = 'TRN'.date('YmdHis');
            $transaksi->id_user = Auth::user()->id;
            $transaksi->id_psikologi = $_GET['id_psikologi'];
            $transaksi->id_kategori_konsul = $_GET['sub_kategori'];
            $transaksi->jadwal = date('Y-m-d', strtotime($_GET['date']));
            $transaksi->jam = substr($_GET['jamDurasi'],0,5).":00";
            $transaksi->jadwal_awal_akhir = $_GET['jamDurasi'];
            $transaksi->id_jadwal_konsul = $jadwal_konsul->id_jadwal_konsul;
            $transaksi->id_media = $_GET['media'];
            $transaksi->status_bayar = 'pending';
            $transaksi->masalah = $_GET['masalah'];
            $transaksi->jam_transaksi = date('H:i:s');
            $transaksi->save();

            $jam = explode (" - ",$_GET['jamDurasi']);
            $awal  = date_create(date('Y-m-d').' '.$jam[0]);
            $akhir = date_create(date('Y-m-d').' '.$jam[1]);
            $diff  = date_diff( $awal, $akhir );

            if($diff->h == 1){
                $durasi_jam = 1;
                $harga_jam = 150000;
            }
            if($diff->h == 2){
                $durasi_jam = 2;
                $harga_jam = 250000;
            }
            if($diff->i == 30){
                $durasi_jam = '1.5';
                $harga_jam = 200000;
            }
            
            //$total_harga = $psikologi_kategori->harga + $media->harga + $harga_jam;
            $total_harga = $harga_jam;

            $laporan_psikologi = new LaporanPsikologi();
            $laporan_psikologi->id_psikologi = $_GET['id_psikologi'];
            $laporan_psikologi->id_user = Auth::user()->id;
            $laporan_psikologi->total_konsul = $total_harga;
            $laporan_psikologi->id_kategory = $name;
            $laporan_psikologi->id_media = $_GET['media'];
            $laporan_psikologi->id_transaksi = $transaksi['id_transaksi'];
            $laporan_psikologi->save();

            $laporan_klien = new LaporanKlien();
            $laporan_klien->id_psikologi = $_GET['id_psikologi'];
            $laporan_klien->id_user = Auth::user()->id;
            $laporan_klien->total_konsul = $total_harga;
            $laporan_klien->id_kategory = $name;
            $laporan_klien->id_media = $_GET['media'];
            $laporan_klien->id_transaksi = $transaksi['id_transaksi'];
            $laporan_klien->save();

            DB::commit();
            return $laporan_klien['id'];

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }
    }
    public function sukses($id_laporan_klien,$name){
        if(empty(Auth::user()->id)){
            session()->flash('danger','Untuk Melanjutkan, Login Terlebih Dahulu');
            return redirect()->route('home');
        }
        date_default_timezone_set('Asia/Jakarta');

        if(!empty($_GET['result_data'])){
            $status = json_decode($_GET['result_data'],true);
            $transaksi = Transaksi::where('no_transaksi',$status['order_id'])->first();
            $transaksi->pdf_url = $status['pdf_url'];
            $transaksi->finish_redirect_url = $status['finish_redirect_url'];

            if($status['payment_type'] == 'echannel'){
                $transaksi->bill_key  = $status['bill_key'];
                $transaksi->bank  = 'Mandiri';
                $transaksi->type_pembayaran = $status['payment_type'];
                $transaksi->va_number  =  $status['bill_key'];
                $transaksi->biller_code  = $status['biller_code'];
            }
            $transaksi->save();

            $laporan_klien = DB::table('laporan_klien')
                        ->leftJoin('psikologi','psikologi.id_psikologi','=','laporan_klien.id_psikologi')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','laporan_klien.id_kategory')
                        //->leftJoin('users','users.id','=','laporan_klien.id_user')
                        ->where('id_laporan_klien',$id_laporan_klien)->first();

            $transaksi = DB::table('transaksi')
                    ->leftJoin('media','media.id_media','=','transaksi.id_media')
                    ->where('id_transaksi',$laporan_klien->id_transaksi)->first();
                
            $id_laporan_klien = $laporan_klien->id_laporan_klien;

            $snapToken =$status['transaction_id'];

            return view('konsul.psikologi.final',compact('id_laporan_klien','transaksi','snapToken','laporan_klien'));
    
        }

        $laporan_klien = DB::table('laporan_klien')
                        ->leftJoin('psikologi','psikologi.id_psikologi','=','laporan_klien.id_psikologi')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','laporan_klien.id_kategory')
                        //->leftJoin('users','users.id','=','laporan_klien.id_user')
                        ->where('id_laporan_klien',$id_laporan_klien)->first();

        $transaksi = DB::table('transaksi')
                    ->leftJoin('media','media.id_media','=','transaksi.id_media')
                    ->where('id_transaksi',$laporan_klien->id_transaksi)->first();
        
        if(!empty($transaksi->type_pembayaran)){
            $snapToken = '';
        
            return view('konsul.psikologi.final',compact('id_laporan_klien','transaksi','snapToken','laporan_klien',));
            
        }else{
            \Midtrans\Config::$serverKey = 'Mid-server-YCnurrMjrJGr0jdTE3JW7OQe';
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            \Midtrans\Config::$isProduction = true;
            // Set sanitization on (default)
            \Midtrans\Config::$isSanitized = true;
            // Set 3DS transaction for credit card to true
            \Midtrans\Config::$is3ds = true;

            $biodata = DB::table('biodata')->where('id_user',Auth::user()->id)->first();
            
            $params = array(
                'transaction_details' => array(
                    'order_id' => $transaksi->no_transaksi,
                    'gross_amount' => $laporan_klien->total_konsul,
                ),
                'customer_details' => array(
                    'first_name' => Auth::user()->name,
                    'last_name' => '',
                    'email' => Auth::user()->email,
                    'phone' => $biodata->no_hp,
                ),
            );
            
            $snapToken = \Midtrans\Snap::getSnapToken($params);
            
            return view('konsul.psikologi.final',compact('id_laporan_klien','transaksi','snapToken','laporan_klien'));
        }
 
    }
    public function test(){
        $array = Array (
                "id" => "USER1",
                "name" => "Steve Jobs",
                "company" => "Apple"
        );
        $json = json_encode($array);
        $json = json_decode($json,true);
        $transaksi = Transaksi::
                        leftJoin('media','media.id_media','=','transaksi.id_media')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','transaksi.id_kategori_konsul')
                        ->where('no_transaksi','TRN20210404210547')->first();

        $biodata = Biodata::where('id_user',$transaksi->id_user)->first();

        $psikologi = Psikologi::where('id_psikologi',$transaksi->id_psikologi)->first();

        $message = "Info dari admin bawah ada klien memsan perjanjian psikologi Anda\nNomor Transaksi : \nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTanggal Konsultasi : ".$transaksi->jadwal."\nJam Konsultasi : ".$transaksi->jadwal."\nNama Nama Klien : ".$biodata->nama."\nMedia Konsultasi : ".$transaksi->nama_media."\nKategori Konsultasi : ".$transaksi->nama_sub_kategori."\nCatatan Masalah : ".$transaksi->masalah."\nTotal Harga : ".$transaksi->harga."\nStatus : Lunas";
        echo '<pre>';
        print_r($message);
        exit;
    }
    public function payment(Request $request){

        $transaksi = Transaksi::
                        leftJoin('media','media.id_media','=','transaksi.id_media')
                        ->leftJoin('laporan_klien','laporan_klien.id_transaksi','=','transaksi.id_transaksi')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','transaksi.id_kategori_konsul')
                        ->where('no_transaksi',$request['order_id'])->first();
        if(!empty($transaksi)){
            $kategori = DB::table('kategori_konsul_sub_kategori')->where('id_sub_kategori',
            $transaksi->id_kategori_konsul)->leftJoin('kategori_konsul','kategori_konsul.id_kategori_konsul','=','kategori_konsul_sub_kategori.id_kategori_konsul')->first();

            $transaksi->status_bayar = $request['transaction_status'];
            $transaksi->type_pembayaran = $request['payment_type'];
            // $transaksi->pdf_url = $status['pdf_url'];
            // $transaksi->finish_redirect_url = $status['finish_redirect_url'];
            if($request['payment_type'] == 'cstore'){
                $transaksi->store = $request['store'];
                $transaksi->payment_code  = $request['payment_code'];
            }
            if($request['payment_type'] == 'echannel'){
                $transaksi->bill_key  = $request['bill_key'];
                $transaksi->biller_code  = $request['biller_code'];
            }
            if($request['payment_type'] == 'bank_transfer'){
                if(!empty($request['permata_va_number'])){
                    $transaksi->va_number = $request['permata_va_number'];
                    $transaksi->bank = 'Permata';
                }else{
                    $transaksi->bank = $request['va_numbers'][0]['bank'];
                    $transaksi->va_number = $request['va_numbers'][0]['va_number'];
                }
                
            }

            

            $transaksi->save();

            if($request['payment_type'] == 'cstore'){
                $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();

                $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran dilakukan di ".$request['store']." dengan kode ".$request['payment_code'];

                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();
            }

            if($request['payment_type'] == 'bank_transfer'){
                if(!empty($request['permata_va_number'])){
                    $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();
                    $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui bank transfer Permata no virtual acount ".$request['permata_va_number'];
                    $version = new Version2X("http://109.106.255.28:8000/");
                    $client = new Client($version);
                    $client->initialize();
                    $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                    $client->close();
                }else{
                    $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();
                    $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui bank transfer ".$request['va_numbers'][0]['bank']." no virtual acount ".$request['va_numbers'][0]['va_number'];;
                    $version = new Version2X("http://109.106.255.28:8000/");
                    $client = new Client($version);
                    $client->initialize();
                    $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                    $client->close();
                }
                
            }

            if($request['payment_type'] == 'echannel'){
                $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();
                $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui ".$request['payment_type']." nomor bill ".$request['biller_code'];
                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();
            }
            

            if($transaksi->status_bayar == 'settlement'){
                $biodata1 = Biodata::where('id_user',$transaksi->id_user)->first();

                $psikologi = Psikologi::where('id_psikologi',$transaksi->id_psikologi)->first();

                $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTanggal Konsultasi : ".$transaksi->jadwal."\nJam Konsultasi : ".$transaksi->jam."\nNama Psikolog : ".$psikologi->nama."\nMedia Konsultasi : ".$transaksi->nama_media."\nKategori Konsultasi : ".$transaksi->nama_sub_kategori."\nCatatan Masalah : ".$transaksi->masalah."\nTotal Harga : ".Lib::rupiah($transaksi->total_konsul)."\nStatus : Lunas \nTerimakasih telah melakukan pembayaran, Admin kami akan segera menghubungi anda untuk info perjanjian dengan psikolog ";
                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();

                $psikologi = Psikologi::where('id_psikologi',$transaksi->id_psikologi)->first();
                $biodata = Biodata::where('id_user',$psikologi->id_user)->first();

                $message = "Kamu memiliki jadwal konsultasi baru.\nBerikut detail jadwal konsultasi:\n\nNama Klien : ".$biodata1->nama."\nTanggal Konsultasi: ".date('d-M-Y', strtotime($transaksi->jadwal))."\nJam Konsultasi : ".$transaksi->jam."\nMedia Konsultasi : ".$transaksi->nama_media."\nKategori Konsultasi : ".$kategori->nama_konsul."\nSub Kategori : ".$transaksi->nama_sub_kategori."\nDetail Konsultasi : ".$transaksi->masalah."\n\nHarap untuk dapat melakukan konsultasi sesuai jadwal";

                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $biodata->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();

                $version = new Version2X("https://www.psyqonsul.com:4000");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['sendNotif' => 1, 'user' => $transaksi->id_user]);
                $client->close();
            }
        }else{
            $cek_transaksi = KonsulLansung::where('no_transaksi',$request['order_id'])->first();
            if(!empty($cek_transaksi)){
                $cek_transaksi->status_bayar = $request['transaction_status'];
                $cek_transaksi->type_pembayaran = $request['payment_type'];
    
                if(!empty($cek_transaksi->id_user)){
                    $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
                    $cek_transaksi->nama = $biodata1->nama;
                    $cek_transaksi->tanggal_lahir = $biodata1->tanggal_lahir;
                    $cek_transaksi->no_hp = $biodata1->no_hp;
                    $cek_transaksi->jenis_kelamin = $biodata1->jenis_kelamin;
                    $cek_transaksi->status_perkawinan = $biodata1->status_perkawinan;
                    $cek_transaksi->pendidikan_terakhir = $biodata1->pendidikan_terakhir;
                    $cek_transaksi->kota_tinggal = $biodata1->alamat;
                }
    
                if($request['payment_type'] == 'cstore'){
                    $cek_transaksi->store = $request['store'];
                    $cek_transaksi->payment_code  = $request['payment_code'];
                }
                if($request['payment_type'] == 'echannel'){
                    $cek_transaksi->bill_key  = $request['bill_key'];
                    $cek_transaksi->biller_code  = $request['biller_code'];
                }
                
                if($request['payment_type'] == 'bank_transfer'){
                    if(!empty($request['permata_va_number'])){
                        $cek_transaksi->va_number = $request['permata_va_number'];
                        $cek_transaksi->bank = "Permata";
                    }else{
                        $cek_transaksi->bank = $request['va_numbers'][0]['bank'];
                        $cek_transaksi->va_number = $request['va_numbers'][0]['va_number'];
                    }
                }

                $cek_transaksi->save();

                if($request['payment_type'] == 'cstore'){
                    if(!empty($cek_transaksi->id_user)){
                        $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
    
                        $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran dilakukan di ".$request['store']." dengan kode ".$request['payment_code'];
        
                        $version = new Version2X("http://109.106.255.28:8000/");
                        $client = new Client($version);
                        $client->initialize();
                        $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                        $client->close();
                    }else{
                        $message = "Yth, ".$cek_transaksi->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran dilakukan di ".$request['store']." dengan kode ".$request['payment_code'];
        
                        $version = new Version2X("http://109.106.255.28:8000/");
                        $client = new Client($version);
                        $client->initialize();
                        $client->emit('send data', ['nomor' => $cek_transaksi->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                        $client->close();
                    }
                }
    
                if($request['payment_type'] == 'bank_transfer'){
                    if(!empty($request['permata_va_number'])){
                        if(!empty($cek_transaksi->id_user)){
                            $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
                            $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui bank transfer Permata no virtual acount ".$request['permata_va_number'];
                            $version = new Version2X("http://109.106.255.28:8000/");
                            $client = new Client($version);
                            $client->initialize();
                            $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                            $client->close();
                        }else{
                            $message = "Yth, ".$cek_transaksi->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui bank transfer pembayaran melalui bank transfer Permata no virtual acount ".$request['permata_va_number'];
                            $version = new Version2X("http://109.106.255.28:8000/");
                            $client = new Client($version);
                            $client->initialize();
                            $client->emit('send data', ['nomor' => $cek_transaksi->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                            $client->close();
                        }
                    }else{
                        if(!empty($cek_transaksi->id_user)){
                            $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
                            $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui bank transfer ".$request['va_numbers'][0]['bank']." no virtual acount ".$request['va_numbers'][0]['va_number'];;
                            $version = new Version2X("http://109.106.255.28:8000/");
                            $client = new Client($version);
                            $client->initialize();
                            $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                            $client->close();
                        }else{
                            $message = "Yth, ".$cek_transaksi->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui bank transfer ".$request['va_numbers'][0]['bank']." no virtual acount ".$request['va_numbers'][0]['va_number'];;
                            $version = new Version2X("http://109.106.255.28:8000/");
                            $client = new Client($version);
                            $client->initialize();
                            $client->emit('send data', ['nomor' => $cek_transaksi->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                            $client->close();
                        }

                    }
                }
    
                if($request['payment_type'] == 'echannel'){
                    if(!empty($cek_transaksi->id_user)){
                        $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
                        $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui ".$request['payment_type']." nomor bill ".$request['biller_code'];
                        $version = new Version2X("http://109.106.255.28:8000/");
                        $client = new Client($version);
                        $client->initialize();
                        $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                        $client->close();       
                    }else{
                        $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
                        $message = "Yth, ".$cek_transaksi->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nTotal yang harus anda bayar sejumlah ".Lib::rupiah($request['gross_amount']).", pembayaran melalui ".$request['payment_type']." nomor bill ".$request['biller_code'];
                        $version = new Version2X("http://109.106.255.28:8000/");
                        $client = new Client($version);
                        $client->initialize();
                        $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                        $client->close(); 
                    }
                    
                }
    
                if($cek_transaksi->status_bayar == 'settlement'){
                    if(!empty($cek_transaksi->id_user)){
                        $biodata1 = Biodata::where('id_user',$cek_transaksi->id_user)->first();
    
                        $message = "Yth, ".$biodata1->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nStatus : Lunas \nTerimakasih telah melakukan pembayaran, Admin kami akan segera menghubungi anda untuk info perjanjian dengan psikolog ";
                        $version = new Version2X("http://109.106.255.28:8000/");
                        $client = new Client($version);
                        $client->initialize();
                        $client->emit('send data', ['nomor' => $biodata1->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                        $client->close();
                    }else{
                        $message = "Yth, ".$cek_transaksi->nama."\nNomor Transaksi : ".$request['order_id']."\nTanggal : ".date('d-M-Y', strtotime(date('Y-m-d')))."\nStatus : Lunas \nTerimakasih telah melakukan pembayaran, Admin kami akan segera menghubungi anda untuk info perjanjian dengan psikolog ";
                        $version = new Version2X("http://109.106.255.28:8000/");
                        $client = new Client($version);
                        $client->initialize();
                        $client->emit('send data', ['nomor' => $cek_transaksi->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                        $client->close();
                    }
                    
                }
            }
        }
        
        

    }

    public function invoice($id_laporan_klien) {
        
        $invoice = LaporanKlien::
                    leftJoin('transaksi','transaksi.id_transaksi','=','laporan_klien.id_transaksi')
                    ->leftJoin('users','users.id','=','laporan_klien.id_user')
                    ->where('id_laporan_klien', $id_laporan_klien)->first();
                    
        return view('konsul.psikologi.invoice',  compact('invoice'));
    }

    public function kategori(){
        $kategori = Kategori::get();
        
        if(!empty($_GET['k'])){
            return view('konsul.home.kategori1',compact('kategori'));
        }else{
            return view('konsul.home.kategori',compact('kategori'));
        }
        

    }
    public function masuk_otp(){
        if($_GET['id']){
            if(!empty($_GET['kirim_ulang'])){
                $cek_token = User::where('id',$_GET['id'])->where('aktif',0)->first();
                $cek_token->otp = rand(10,10000);
                $cek_token->save();
                session()->flash('success','OTP di kirim ulang ke Whatsapp anda');
                $message = '(RAHASIA) jangan berikan kepada siapapun. Kode verifikasi untuk register anda adalah '.$cek_token->otp;
                $cek_biodata = Biodata::where('id_user', $cek_token->id)->first();

                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $cek_biodata->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();
                return 'berhasil';
            }
        }
        return view('konsul.home.masuk_otp');
    }

    

    public function masuk_otp_post(Request $request){
        $cek_otp = User::where('otp', $_POST['otp'])->where('id',$_POST['id_user'])->where('aktif',0)->first();
        if(!empty($cek_otp)){
            $cek_otp->aktif = 1;
            $cek_otp->otp = rand(10,10000);
            $cek_otp->save();
            session()->flash('success','Sekarang anda bisa Login');
            return redirect()->route('home');
        }else{
            session()->flash('danger','no otp salah!');
            return redirect()->route('masukotp',['id' => $_POST['id_user']]);
        }
        

        
    }
    public function cari_psikologi($di_sub_kategori){
        // print_r($di_sub_kategori);
        // exit;
        if(!empty($_GET['id_kategori'])){
            $name_sub_kategori = DB::table('kategori_konsul_sub_kategori')
                ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                ->where('id_kategori_konsul',$_GET['id_kategori'])->get();
            
            if(!empty($_GET['id_sub'])){
                $id_sub = $_GET['id_sub'];
            }else{
                $id_sub = '';
            }
            if(!empty($_GET['perjanjian'])){
                return view('konsul.kategori-konsul.subkategori1',compact('name_sub_kategori','id_sub'));
            }else{
                return view('konsul.kategori-konsul.subkategori',compact('name_sub_kategori','id_sub'));
            }
            
        }
        if(!empty($_GET['nama'])){
            $nama= $_GET['nama'];
        }else{
            $nama='';
        }
        $psikologi = PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->where('sub_kategori.id_sub_kategori',$di_sub_kategori)
                    ->where('psikologi.nama','like','%'.$nama.'%')
                    ->where('psikologi.status','aktif')
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi')
                    ->get();

        return view('konsul.kategori-konsul.psikologi',compact('psikologi','di_sub_kategori'));
    }
    public function profil_psikologi($id_psikologi){
        $psikologi = PsikologiKategoriKonsul::
                    select('psikologi.sip','psikologi.foto','psikologi.nama','psikologi.id_psikologi','biodata.universitas','biodata.pengalaman','biodata.tentang_psikologi','psikologi.pengalaman')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->where('psikologi.id_psikologi',$id_psikologi)
                    ->where('psikologi.status','aktif')
                    ->groupBy('psikologi.sip','psikologi.foto','psikologi.nama','psikologi.id_psikologi','biodata.universitas','biodata.pengalaman','biodata.tentang_psikologi','psikologi.pengalaman')
                    ->first();
        
        $kategori_psi =  PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi','nama_sub_kategori')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->where('psikologi_kategori_konsul.id_psikologi',$id_psikologi)
                    ->where('psikologi.status','aktif')
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','nama_sub_kategori')
                    ->get();

        $kategori_k = DB::table('kategori_konsul_sub_kategori')
                    ->select('kategori_konsul_sub_kategori.id_kategori_konsul','kategori_konsul.nama_konsul')
                    ->LeftJoin('psikologi_kategori_konsul','psikologi_kategori_konsul.id_kategori_konsul','=','kategori_konsul_sub_kategori.id_sub_kategori')
                    ->LeftJoin('kategori_konsul','kategori_konsul.id_kategori_konsul','=','kategori_konsul_sub_kategori.id_kategori_konsul')
                    ->where('psikologi_kategori_konsul.id_psikologi',$id_psikologi)
                    ->groupBy('kategori_konsul_sub_kategori.id_kategori_konsul','kategori_konsul.nama_konsul')
                    ->get();

        // echo "<pre>";
        // print_r($psikologi);
        // exit;
        $hari = DB::table('hari')->get();
        $jadwal = JadwalKonsul::where('id_psikologi', $id_psikologi)->get();
        
        return view('konsul.kategori-konsul.profil_psikologi',compact('psikologi','id_psikologi','kategori_psi','kategori_k','jadwal','hari'));
    }

    
    // public function invoice_pdf($invoice_id) {
    //     $data['invoice'] = Order::where('customer_id', Auth::user()->id_user)->where('id', $invoice_id)->firstOrFail();
    //     $pdf = PDF::loadView('front.invoice_pdf', $data)->setPaper('a4', 'landscape');
    //     return $pdf->stream('pdf/'.$invoice_id.'.pdf');
    // }
    public function sub_kategori($id){

        $kategori_head = Kategori::where('id_kategori_konsul',$id)->first();

        return view('konsul.kategori-konsul.sub_kategori',compact('kategori_head'));
    }
    public function psikolog(){
        $psikologi = PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi','psikologi.sip','biodata.universitas')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->LeftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->where('psikologi.status','aktif')
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','psikologi.sip','biodata.universitas')
                    ->get();

        if(!empty($_GET['nama_psikolog'])){
            $psikologi = PsikologiKategoriKonsul::
                    select('psikologi.foto','psikologi.nama','psikologi.id_psikologi','psikologi.sip','biodata.universitas')
                    ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                    ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                    ->LeftJoin('users','users.id','=','psikologi.id_user')
                    ->LeftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->where('psikologi.status','aktif')
                    ->where('psikologi.nama','like','%'.$_GET['nama_psikolog'].'%')
                    ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','psikologi.sip','biodata.universitas')
                    ->get();
        }
        
        return view('konsul.home.cari_psikolog',compact('psikologi'));
    }

    public function konsul_lansung(){
        // echo "<pre>";
        // print_r($_GET);
        // exit;
        if(!empty($_GET['durasi'])){
            if($_GET['durasi'] == '1'){
                $harga_jam = 150000;
            }
            if($_GET['durasi'] == '2.5'){
                $harga_jam = 250000;
            }
            if($_GET['durasi'] == '2'){
                $harga_jam = 200000;
            }
        }

        if(!empty($_GET['result_type'])){
            return redirect()->route('home');
        }
        if(empty($_GET['akun'])){
                if(!empty($_GET['result_data'])){
                
                $status = json_decode($_GET['result_data'],true);
                $transaksi = KonsulLansung::where('no_transaksi',$status['order_id'])->first();

                $transaksi->pdf_url = $status['pdf_url'];
                $transaksi->finish_redirect_url = $status['finish_redirect_url'];
                $transaksi->save();
                $snapToken =$status['transaction_id'];

                return redirect()->route('home');
            }
        }

        if(!empty(Auth::user()->id)){

            \Midtrans\Config::$serverKey = 'Mid-server-YCnurrMjrJGr0jdTE3JW7OQe';
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            \Midtrans\Config::$isProduction = true;
            // Set sanitization on (default)
            \Midtrans\Config::$isSanitized = true;
            // Set 3DS transaction for credit card to true
            \Midtrans\Config::$is3ds = true;
            $no_transaksi = Auth::user()->id.'TRN'.date('YmdHis');
            
            $biodata = DB::table('biodata')->where('id_user',Auth::user()->id)->first();
  
            $params = array(
                'transaction_details' => array(
                    'order_id' => $no_transaksi,
                    'gross_amount' => $harga_jam,
                ),
                'customer_details' => array(
                    'first_name' => Auth::user()->name,
                    'last_name' => '',
                    'email' => Auth::user()->email,
                    'phone' => $biodata->no_hp,
                ),
            );
            $snapToken = \Midtrans\Snap::getSnapToken($params);  

            $cek_transaksi = KonsulLansung::where('no_transaksi',$no_transaksi)->first();
            if(empty($cek_transaksi)){
                $konsul_lansung = New KonsulLansung();
                $konsul_lansung->id_user = Auth::user()->id;
                $konsul_lansung->tanggal_order = date('Y-m-d H:i:s');
                $konsul_lansung->status_konsul = "Belum";
                $konsul_lansung->harga = $harga_jam;
                $konsul_lansung->id_media = $_GET['media'];
                $konsul_lansung->no_transaksi = $no_transaksi;
                $konsul_lansung->save();

                return  $snapToken;
            }else{
                return '';
            }
        }else{
            
            \Midtrans\Config::$serverKey = 'Mid-server-YCnurrMjrJGr0jdTE3JW7OQe';
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            \Midtrans\Config::$isProduction = true;
            // Set sanitization on (default)
            \Midtrans\Config::$isSanitized = true;
            // Set 3DS transaction for credit card to true
            \Midtrans\Config::$is3ds = true;
            $no_transaksi = 'TRN'.date('YmdHis');
            
            $params = array(
                'transaction_details' => array(
                    'order_id' => $no_transaksi,
                    'gross_amount' => $harga_jam
                ),
                'customer_details' => array(
                    'first_name' => $_GET['name'],
                    'last_name' => $_GET['name'],
                    'email' =>  $_GET['email'],
                    'phone' => $_GET['no_telp']
                ),
            );
            $snapToken = \Midtrans\Snap::getSnapToken($params);           

            $konsul_lansung = New KonsulLansung();
            $konsul_lansung->tanggal_order = date('Y-m-d H:i:s');
            $konsul_lansung->status_konsul = "Belum";
            $konsul_lansung->harga = $harga_jam;
            $konsul_lansung->nama = $_GET['name'];
            $konsul_lansung->id_media = $_GET['media'];
            $konsul_lansung->no_transaksi = $no_transaksi;
            $konsul_lansung->no_hp = $_GET['no_telp'];
            $konsul_lansung->tanggal_lahir = $_GET['tanggal_lahir'];
            $konsul_lansung->jenis_kelamin = $_GET['jenis_kelamin'];
            $konsul_lansung->status_perkawinan = $_GET['status_perkawinan'];
            $konsul_lansung->pendidikan_terakhir = $_GET['pendidikan_terakhir'];
            $konsul_lansung->pekerjaan = $_GET['pekerjaan'];
            $konsul_lansung->kota_tinggal = $_GET['alamat'];
            $konsul_lansung->email = $_GET['email'];
            //$konsul_lansung->no_transaksi = $_GET['no_transaksi'];
            $konsul_lansung->save();
            return $snapToken;
        }
    }

    
}
