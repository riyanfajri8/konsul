<?php

namespace App\Http\Controllers;

use App\Biodata;
use Illuminate\Http\Request;
use App\User;
use App\Models\Roles\Customer;
use App\Psikologi;
use Hash;
use Auth;
use Image;
use DB;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotify;

class AuthController extends Controller
{
    public function loginPsikologi() {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
        ];
        return view('konsul.psikologi.login', ['pageConfigs'=>$pageConfigs]); 
    }

    public function loginPsikologi_post(Request $r) {
        // dd($r->has('from_vacation'));
    
        $r->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);


        $auth = Auth::attempt($r->only('email','password'));
        // dd($auth);
        if(!$auth) {
            session()->flash('danger','Email atau password salah!');
            return redirect()->route('home');
        }else{
            return redirect()->route('admin');
        }
    }

    public function login_user(Request $r){
        $r->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $cek_token = User::where('email',$r->email)->where('aktif',1)->first();
        if(!empty($cek_token)){
            $auth = Auth::attempt($r->only('email','password'));
            // dd($auth);
            if(!$auth) {
                session()->flash('danger','Email atau password salah!');
                return redirect()->route('home');
            }else{
                session()->flash('success','Login Berhasil');
                return redirect()->route('home');
            }
        }else{
            $cek_token = User::where('email',$r->email)->where('aktif',0)->first();
            if(!empty($cek_token)){
                session()->flash('success','Cek pesan di Whatsapp No kami akan mengirim token anda');
                $cek_token->otp = rand(10,10000);
                $cek_token->save();

                $cek_biodata = Biodata::where('id_user', $cek_token->id)->first();
                $message = '(RAHASIA) Jangan berikan kepada siapapun. Kode verifikasi register anda adalah '.$cek_token->otp."\n\nJika kamu ada pertanyaan, jangan ragu menghubungi kami.\n\nSalam\nPsyQonsul\n\npsyqonsul@gmail.com\n0761-848844\n+6281268968909";
                
                $version = new Version2X("http://109.106.255.28:8000/");
                $client = new Client($version);
                $client->initialize();
                $client->emit('send data', ['nomor' => $cek_biodata->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
                $client->close();

                // Mail::to($cek_token->email)->send(new MailNotify($cek_token));
            }else{
                session()->flash('danger','Email atau password salah!');
                return redirect()->route('home');
            }

            session()->flash('success','lihat whatsapp anda kami mengirimkan OTP untuk login');
            return redirect()->route('masukotp',['id' => $cek_token->id]);
        }
        
    }

    public function register() {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
        ];
        return view('auth.register', ['pageConfigs'=>$pageConfigs]); 
    }

    public function registerPsikologi() {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
        ];
        return view('auth.register-psikologi', ['pageConfigs'=>$pageConfigs]); 
    }

    public function register_psikologi_post(Request $r){
        $r->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'no_telp' => 'required|numeric',
            'alamat' => 'required|string',
        ]);
        DB::beginTransaction();
        try {
            $name = $r->name;
            $email = $r->email;
            $password = $r->password;
            $no_telp = $r->no_telp;
            $address = $r->alamat;
            $customer_id = 'CUST'.date('YmdHis');
            $jenis_kelamin = $r->jenis_kelamin;
            $otp = rand(10,10000);
            $user = new User;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->created_at = date('Y-m-d H:i:s');
            $user->name = $name;
            $user->otp = $otp;
            $user->save();

            $biodata = new Biodata();
            $biodata->alamat = $address;
            $biodata->no_hp = $no_telp;
            $biodata->nama = $name;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($r->tanggal_lahir));
            $biodata->id_user = $user->id;
            $biodata->status = 'Aktif';
            $biodata->pekerjaan = $r->pekerjaan;
            $biodata->pendidikan_terakhir = $r->pendidikan_terakhir;
            $biodata->jenis_kelamin = $jenis_kelamin;
            $biodata->status_perkawinan = $r->status_perkawinan;
            $biodata->tempat_lahir = $r->tempatlahir;
            $biodata->tentang_psikologi = $_POST['tentang'];
            $biodata->universitas = $_POST['universitas'];
            $biodata->pengalaman = $_POST['pengalaman'];
            $biodata->save();
            

            $psikologi = new Psikologi();
            $psikologi->id_user = $user->id;
            $psikologi->status = 'pending';
            $psikologi->nama = $name;
            $psikologi->sip = $r->sipp;
            $psikologi->id_biodata = $biodata->id_biodata;
            $psikologi->sarat_ketentuan = 1;
            $psikologi->id_pendidikan = $_POST['pilih_pendidikan'];
            if(!empty($_POST['pilih_pendidikan_sub'])){
                $psikologi->id_sub_pendidikan = $_POST['pilih_pendidikan_sub'];
            }
            $psikologi->pengalaman = $_POST['pengalaman'];

            $file = $r->file('img');
            if($file != ""){
                $year_folder = date("Y");
                $month_folder = date("m");
                $day_file = date("dhis");
                $path = '/uploads/psikologi/';
                    
                    if (!file_exists(public_path().$path)) { 
                        mkdir(public_path() .$path, 0777, true); 

                    }
                $ext = $file->getClientOriginalExtension();
                $fileName = strtolower($day_file .  '.' .$ext);
                $image = Image::make($r->file('img'));
                $psikologi->foto = $path . $fileName ;
                $image->save(public_path().$path. $fileName);
                //$path = public_path('uploads/' . $fileName);
                //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            }

            $file = $r->file('foto_ktp');
            if($file != ""){
                $year_folder = date("Y");
                $month_folder = date("m");
                $day_file = date("dhis");
                $path = '/uploads/psikologi/ktp/';
                    
                    if (!file_exists(public_path().$path)) { 
                        mkdir(public_path() .$path, 0777, true); 

                    }
                $ext = $file->getClientOriginalExtension();
                $fileName = strtolower($day_file .  '.' .$ext);
                $image = Image::make($r->file('foto_ktp'));
                $psikologi->foto_ktp = $path . $fileName ;
                $image->save(public_path().$path. $fileName);
                //$path = public_path('uploads/' . $fileName);
                //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            }

            $psikologi->save();

            $message = '(RAHASIA) Jangan berikan kepada siapapun. Kode verifikasi register anda adalah '.$otp."\n\nJika kamu ada pertanyaan, jangan ragu menghubungi kami.\n\nSalam\nPsyQonsul\n\npsyqonsul@gmail.com\n0761-848844\n+6281268968909";

            $version = new Version2X("http://109.106.255.28:8000/");
            $client = new Client($version);
            $client->initialize();
            $client->emit('send data', ['nomor' => $no_telp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
            $client->close();

            //permintaan psikolog
            $admin = Biodata::where('id_user',1)->first();
            $message = 'Ada permintaan menjadi Psikolog dengan nama '.$name.' email '.$r->email;
            $version = new Version2X("http://109.106.255.28:8000/");
            $client = new Client($version);
            $client->initialize();
            $client->emit('send data', ['nomor' => $admin->no_hp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
            $client->close();


            //permintaan psikolog
            $admin = Biodata::where('id_user',1)->first();
            $message = 'Terima kasih sudah mendaftar sebagai psikolog di PsyQonsul. Mohon menunggu 1 x 24 untuk persetujuan verifikasi data anda. Silahkan login dan lengkapi data diri anda Terima kasih';
            $version = new Version2X("http://109.106.255.28:8000/");
            $client = new Client($version);
            $client->initialize();
            $client->emit('send data', ['nomor' => $no_telp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
            $client->close();

            Mail::to($user->email)->send(new MailNotify($user));

            DB::commit();
            
            session()->flash('success','Sukses Daftar, Cek pesan Whatsapp anda sekarang');
            return redirect()->route('masukotp',['id' => $user->id]);

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }
    }

    public function register_post(Request $r) {
        $r->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'no_telp' => 'required|numeric',
            'address' => 'required|string',
        ]);
// echo "<pre>";
// print_r($r);
// exit;
        DB::beginTransaction();
        try {
            $name = $r->name;
            $email = $r->email;
            $password = $r->password;
            $no_telp = $r->no_telp;
            $address = $r->address;
            $customer_id = 'CUST'.date('YmdHis');
            $jenis_kelamin = $r->jenis_kelamin;
            $otp = rand(10,10000);
            $user = new User;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->created_at = date('Y-m-d H:i:s');
            $user->name = $name;
            $user->otp = $otp;
            $user->aktif = 0;
            $user->save();

            $biodata = new Biodata();
            $biodata->alamat = $address;
            $biodata->no_hp = $no_telp;
            $biodata->nama = $name;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($r->tanggal_lahir));
            $biodata->id_user = $user->id;
            $biodata->status = 'Aktif';
            $biodata->jenis_kelamin = $jenis_kelamin;

            // $file = $r->file('img');
            // if($file != ""){
            //     $year_folder = date("Y");
            //     $month_folder = date("m");
            //     $day_file = date("d_h_i_s");
            //     $path = '/uploads/user/'. $year_folder . '/' . $month_folder .'/';
                    
            //         if (!file_exists(public_path().$path)) { 
            //             mkdir(public_path() .$path, 0777, true); 
            //         }

            //     $ext = $file->getClientOriginalExtension();
            //     $fileName = strtolower('_' . $day_file .  '.' .$ext);
            //     $image = Image::make($r->file('img'));
            //     $biodata->img = $path . $fileName ;
            //     $image->save(public_path().$path. $fileName);
            //     //$path = public_path('uploads/' . $fileName);
            //     //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            // }

            $biodata->save();

            $message = '(RAHASIA) Jangan berikan kepada siapapun. Kode verifikasi register anda adalah '.$otp."\n\nJika kamu ada pertanyaan, jangan ragu menghubungi kami.\n\nSalam\nPsyQonsul\n\npsyqonsul@gmail.com\n0761-848844\n+6281268968909";

            $version = new Version2X("http://109.106.255.28:8000/");
            $client = new Client($version);
            $client->initialize();
            $client->emit('send data', ['nomor' => $no_telp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
            $client->close();
           
            Mail::to($user->email)->send(new MailNotify($user));
            //exit;
            DB::commit();
            session()->flash('success','Sukses Daftar, Cek pesan Whatsapp anda sekarang');
            return redirect()->route('masukotp',['id' => $user->id]);

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }

    }

    public function otp($otp){
        $cek_otp = User::where('otp', $otp)->where('aktif',0)->first();
        $cek_otp->aktif = 1;
        $cek_otp->otp = rand(10,10000);
        $cek_otp->save();

        session()->flash('success','Sekarang anda bisa Login');
        return redirect()->route('home');
    }

    public function forgot() {

    }

    public function verify() {

    }

    public function logout(Request $r) {
        Auth::logout();
        session()->flash('toast.success', "Sukses Logout"); 
        return redirect("/");
    }

    public function register_user(Request $r) {
        $r->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'no_telp' => 'required|numeric',
        ]);
// echo "<pre>";
// print_r($r);
// exit;
        DB::beginTransaction();
        try {
            $name = $r->name;
            $email = $r->email;
            $password = $r->password;
            $no_telp = $r->no_telp;
            $customer_id = 'CUST'.date('YmdHis');
            $jenis_kelamin = $r->jenis_kelamin;
            $otp = rand(10,10000);
            $user = new User;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->created_at = date('Y-m-d H:i:s');
            $user->name = $name;
            $user->otp = $otp;
            $user->aktif = 0;
            $user->save();

            $biodata = new Biodata();
            $biodata->no_hp = $no_telp;
            $biodata->nama = $name;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($r->tanggal_lahir));;
            $biodata->id_user = $user->id;
            $biodata->status = 'Aktif';
            $biodata->jenis_kelamin = $jenis_kelamin;

            $file = $r->file('img');
            if($file != ""){
                $year_folder = date("Y");
                $month_folder = date("m");
                $day_file = date("d_h_i_s");
                $path = '/uploads/user/'. $year_folder . '/' . $month_folder .'/';
                    
                    if (!file_exists(public_path().$path)) { 
                        mkdir(public_path() .$path, 0777, true); 
                    }

                $ext = $file->getClientOriginalExtension();
                $fileName = strtolower('_' . $day_file .  '.' .$ext);
                $image = Image::make($r->file('img'));
                $biodata->img = $path . $fileName ;
                $image->save(public_path().$path. $fileName);
                //$path = public_path('uploads/' . $fileName);
                //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            }

            $biodata->save();

            $message = '(RAHASIA) Jangan berikan kepada siapapun. Kode verifikasi(OTP) register anda adalah '.$otp."\n\nJika kamu ada pertanyaan, jangan ragu menghubungi kami.\n\nSalam\nPsyQonsul\n\npsyqonsul@gmail.com\n0761-848844\n+6281268968909";

            $version = new Version2X("http://109.106.255.28:8000/");
            $client = new Client($version);
            $client->initialize();
            $client->emit('send data', ['nomor' => $no_telp, 'message'=>$message,'jml_nomor' => 1,'otp'=>1]);
            $client->close();
           
            Mail::to($user->email)->send(new MailNotify($user));
            //exit;
            DB::commit();
            session()->flash('success','Sukses Daftar, Cek pesan Whatsapp anda sekarang');
            return redirect()->route('masukotp',['id' => $user->id]);

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }

    }
}
