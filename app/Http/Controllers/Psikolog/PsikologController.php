<?php

namespace App\Http\Controllers\Psikolog;

use App\Biodata;
use Spatie\Permission\Models\Permission;
use App\Kategori;
use App\Lib;
use App\Media;
use App\Http\Controllers\Controller;
use App\JadwalKonsul;
use App\Psikologi;
use App\Transaksi;
use App\PsikologiKategoriKonsul;
use App\User;
use Illuminate\Http\Request;
use Image;
use Auth;
use DB;


class PsikologController extends Controller
{
    public function profil(){

        $psikolog = Psikologi::where('id_user',Auth::user()->id)->first();

        if(Auth::user()->id == 1){
            return redirect()->route('admin.permintan.psikolog');
        }

        if(!empty($psikolog)){
            return view('konsul.psikologi.beranda',compact('psikolog'));
        }else{
            $biodata = Biodata::where('id_user',Auth::user()->id)->first();
            return view('konsul.user.beranda',compact('biodata'));
        }
        
    }

    public function data_profil(){
        $psikolog = Psikologi::
                        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                        ->where('psikologi.id_user',Auth::user()->id)->first();

        return view('konsul.psikologi.data_profil',compact('psikolog'));
    }

    public function update_profil(){
        $psikolog = Psikologi::
                        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                        ->where('psikologi.id_user',Auth::user()->id)->first();

        return view('konsul.psikologi.update_profil',compact('psikolog'));
    }

    public function ubah_password(){
        $psikolog = Psikologi::
                    leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                    ->where('psikologi.id_user',Auth::user()->id)->first();
        return view('konsul.psikologi.ubah_password',compact('psikolog'));
    }

    public function kategori(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $kategori = Kategori::
                        leftJoin('kategori_konsul_sub_kategori','kategori_konsul_sub_kategori.id_kategori_konsul','=',
                        'kategori_konsul.id_kategori_konsul')
                        ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                        ->leftJoin('psikologi_kategori_konsul','psikologi_kategori_konsul.id_kategori_konsul','=','sub_kategori.id_sub_kategori')
                        ->leftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                        ->where('id_user', Auth::user()->id)
                        ->get();
        $kategori1 = Kategori::get();
        return view('konsul.psikologi.kategori',compact('psikolog','kategori','kategori1'));
    }

    public function kategoriDelete($id){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        $psikologi_kategori = PsikologiKategoriKonsul::where('id_psikologi_kategori_konsul',$id)->first();
        $psikologi_kategori->delete();

        $sub_kategori = DB::table('sub_kategori')->get();

        return redirect()->route('psikolog.kategori');
    }

    public function dataKlien(){
        $psikolog = Psikologi::
            leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
            ->where('psikologi.id_user',Auth::user()->id)->first();

        $laporan_klien = DB::table('laporan_klien')
                    ->select('transaksi.*','psikologi.nama as nama_psikolog','sub_kategori.nama_sub_kategori','users.name','media.nama_media','laporan_klien.total_konsul','transaksi.created_at')
                    ->leftJoin('psikologi','psikologi.id_psikologi','=','laporan_klien.id_psikologi')
                    ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','laporan_klien.id_kategory')
                    ->leftJoin('users','users.id','=','laporan_klien.id_user')
                    ->leftJoin('transaksi','transaksi.id_transaksi','=','laporan_klien.id_transaksi')
                    ->leftJoin('media','media.id_media','=','transaksi.id_media')
                    ->where('transaksi.id_psikologi',$psikolog->id_psikologi)
                    ->orderBy('created_at','desc')
                    ->get();
        
        if(!empty($_GET['status_selesai'])){
            $transaksi = Transaksi::where('id_transaksi',$_GET['id_transaksi'])->first();
            $transaksi->status_konsul = 1;
            $transaksi->save();
        }

        return view('konsul.psikologi.data_klien',compact('psikolog','laporan_klien'));
    }

    public function jadwal_konsul(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $hari = DB::table('hari')->get();
        
        $jadwal = JadwalKonsul::where('id_psikologi', $psikolog->id_psikologi)->get();

        return view('konsul.psikologi.jadwal_konsul',compact('psikolog','jadwal','hari'));
    }

    public function create_jadwal(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        return view('konsul.psikologi.create_jadwal',compact('psikolog'));
    }

    public function delete_jam($jam, $hari){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        
        $jadwal = JadwalKonsul::where('id_psikologi',$psikolog->id_psikologi)->where('jadwal',$jam)->where('hari',$hari)->first();

        $jadwal->delete();
        return 'berhasil';
    }

    public function create_jam($jam, $hari){
    
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();

        $cek = JadwalKonsul::where('id_psikologi',$psikolog->id_psikologi)->where('jadwal',$jam)->where('hari',$hari)->first();

        if(!empty($cek)){
            return 'jadwal di hari '.$hari.' dan di jam '.$jam.' sudah ada';
        }

        $jadwal = New JadwalKonsul();
        $jadwal->jadwal = $jam;
        $jadwal->jadwal_akhir = $_GET['jam2'];
        $jadwal->hari = $hari;
        $jadwal->harga = $_GET['harga'];
        $jadwal->durasi = $_GET['durasi'];
        $jadwal->user_created = Auth::user()->id;
        $jadwal->id_psikologi = $psikolog->id_psikologi;
        $jadwal->save();
        return "berhasil";
    }

    public function ambil_jadwal(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $hari = DB::table('hari')->get();
        $jadwal = JadwalKonsul::where('id_psikologi', $psikolog->id_psikologi)->get();

        return view('konsul.psikologi.jadwal',compact('psikolog','jadwal','hari'));
    }

    public function create_kategori(){
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        $kategori = Kategori::get();
        $sub_kategori = DB::table('sub_kategori')->get();
        return view('konsul.psikologi.create_kategori',compact('psikolog','sub_kategori','kategori'));
    }

    public function create_kategori_post(Request $request){
        // $q->validate([
        //     'sub_kategori' => 'required',
        // ]);
        $psikolog = Psikologi::
        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
        ->where('psikologi.id_user',Auth::user()->id)->first();
        // echo "<pre>";
        // print_r($_POST['sub_kategori']);
        // exit;
        foreach($_POST['sub_kategori'] as $data){
            $cek = PsikologiKategoriKonsul::where('id_psikologi',$psikolog['id_psikologi'])->where('id_kategori_konsul',$data)->first();

            if(!empty($cek)){
                $harga_sub = DB::table('sub_kategori')->where('id_sub_kategori',$data)->first();
                $cek->id_psikologi =  $psikolog->id_psikologi;
                $cek->created_at = date('Y-m-d H:i:s');
                $cek->id_kategori_konsul = $data;
                $cek->harga = $harga_sub->harga;
                $cek->user_create = Auth::user()->id;
                $cek->save();
            }else{
                $harga_sub = DB::table('sub_kategori')->where('id_sub_kategori',$data)->first();
                $psikologi_kategori = New PsikologiKategoriKonsul();
                $psikologi_kategori->id_psikologi =  $psikolog->id_psikologi;
                $psikologi_kategori->created_at = date('Y-m-d H:i:s');
                $psikologi_kategori->id_kategori_konsul = $data;
                $psikologi_kategori->harga = $harga_sub->harga;
                $psikologi_kategori->user_create = Auth::user()->id;
                $psikologi_kategori->save();
            }
            
        }
        
        

        $sub_kategori = DB::table('sub_kategori')->get();
        return redirect()->route('psikolog.kategori');
    }

    public function upload_img(Request $request){
        $pasikologi = Psikologi::where('id_user',Auth::user()->id)->first();
        $file = $request->file('img');
        if($file != ""){
            $year_folder = date("Y");
            $month_folder = date("m");
            $day_file = date("dhis");
            $path = '/uploads/psikologi/';
                
                if (!file_exists(public_path().$path)) { 
                    mkdir(public_path() .$path, 0777, true); 

                }
            $ext = $file->getClientOriginalExtension();
            $fileName = strtolower($day_file .  '.' .$ext);
            $image = Image::make($request->file('img'));
            $pasikologi->foto = $path . $fileName ;
            $image->save(public_path().$path. $fileName);
            //$path = public_path('uploads/' . $fileName);
            //Image::make($file->getRealPath())->resize(120, 120)->save($path);
        }
        $pasikologi->save();
        return redirect()->route('psikolog.data_profil_psikolog');
    }

    public function update_profil_post(Request $request){
            // $request->validate([
            //     'nama' => 'required',
            //     'jenis_identitas' =>'required',
            //     'no_identitas' => 'required',
            //     'tempat_lahir' => 'required',
            //     'tanggal_lahir' => 'required',
            //     'jenis_kelamin' => 'required',
            //     'status_perkawinan' => 'required',
            //     'agama' =>'required',
            //     'no_hp' => 'required',
            //     'pekerjaan' => 'required',
            //     'no_hp' => 'required',
            // ]);
            

        DB::beginTransaction();
        try {

            $user = User::find(Auth::user()->id);
            //$user->email = $email;
            //$user->password = Hash::make($password);
            //$user->updated_at = date('Y-m-d H:i:s');
            $user->name = $request->nama;
            $user->save();

            $pasikologi = Psikologi::where('id_user',Auth::user()->id)->first();
            $pasikologi->nama = $request->nama;
            $pasikologi->id_pendidikan = $request->pilih_pendidikan;
            $pasikologi->pengalaman = $request->pengalaman;
            $pasikologi->id_sub_pendidikan = $request->pilih_pendidikan_sub;
            $pasikologi->sip = $request->sip;

            $file = $request->file('img');
            if($file != ""){
                $year_folder = date("Y");
                $month_folder = date("m");
                $day_file = date("dhis");
                $path = '/uploads/psikologi/';
                    
                    if (!file_exists(public_path().$path)) { 
                        mkdir(public_path() .$path, 0777, true); 

                    }
                $ext = $file->getClientOriginalExtension();
                $fileName = strtolower($day_file .  '.' .$ext);
                $image = Image::make($request->file('img'));
                $pasikologi->foto = $path . $fileName ;
                $image->save(public_path().$path. $fileName);
                //$path = public_path('uploads/' . $fileName);
                //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            }

            $pasikologi->save();

            $biodata = Biodata::where('id_biodata', $pasikologi->id_biodata)->first();
            $biodata->alamat = $request->alamat;
            $biodata->pengalaman = $request->pengalaman;
            $biodata->universitas = $request->universitas;
            $biodata->no_hp = $request->no_hp;
            $biodata->pekerjaan = $request->pekerjaan;
            $biodata->tentang_psikologi = $request->tentang_psikologi;
            
            $biodata->nama = $request->nama;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
            $biodata->jenis_kelamin = $request->jenis_kelamin;

            $biodata->jenis_identitas = $request->jenis_identitas;
            $biodata->no_identitas = $request->no_identitas;
            $biodata->tempat_lahir = $request->tempat_lahir;
            $biodata->agama = $request->agama;
            $biodata->id_user = Auth::user()->id;
            $biodata->status_perkawinan = $request->status_perkawinan;
            
            $biodata->updated_at = date('Y-m-d H:i:s');

            if(!$biodata->save()){
                
            }
            //$biodata->save();
            $psikolog = Psikologi::
                        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                        ->where('psikologi.id_user',Auth::user()->id)->first();
            DB::commit();
            
            session()->flash('success','Sukses Update Data Profil');
            return redirect()->route('psikolog.data_profil',['psikolog' => $psikolog]);

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }
    }

    public function update_profil_user_post(Request $request){

        DB::beginTransaction();
        try {
                if(empty($request->nama)){
                    return 'tidak lengkap';
                }
                if(empty($request->alamat)){
                    return 'tidak lengkap';
                }
                if(empty($request->no_hp)){
                    return 'tidak lengkap';
                }
                if(empty($request->tanggal_lahir)){
                    return 'tidak lengkap';
                }
                if(empty($request->jenis_kelamin)){
                    return 'tidak lengkap';
                }
                if(empty($request->status_perkawinan)){
                    return 'tidak lengkap';
                }
                if(empty($request->pekerjaan)){
                    return 'tidak lengkap';
                }
                
                
            // //$d = $_file['img'];
            // $file = $_FILES['img'];
            // $extension =  explode ("/",$file['type']);
            // $extension = $extension['1'];
            // echo '<pre>';
            // print_r($file);
            // exit;
            // if($file != ""){
            //     $year_folder = date("Y");
            //     $month_folder = date("m");
            //     $day_file = date("d_h_i_s");
            //     $path = '/uploads/user/'. $year_folder . '/' . $month_folder .'/';
                    
            //         if (!file_exists(public_path().$path)) { 
            //             mkdir(public_path() .$path, 0777, true); 
            //         }

            //     $ext = $extension;
            //     $fileName = strtolower('_' . $day_file .  '.' .$ext);
            //     $image = Image::make($file);
            //     $biodata->img = $path . $fileName ;
            //     $image->save(public_path().$path. $fileName);
            //     //$path = public_path('uploads/' . $fileName);
            //     //Image::make($file->getRealPath())->resize(120, 120)->save($path);
            // }


            
            $user = User::find(Auth::user()->id);
            //$user->email = $email;
            //$user->password = Hash::make($password);
            //$user->updated_at = date('Y-m-d H:i:s');
            $user->name = $request->nama;
            $user->save();
            
            $biodata = Biodata::where('id_user', Auth::user()->id)->first();
            // if( $_GET['ajax'] != 1){
            //     $pasikologi = Psikologi::where('id_user',Auth::user()->id)->first();
            //     $pasikologi->nama = $request->nama;;
            //     $pasikologi->save();

            //     $biodata = Biodata::where('id_biodata', $pasikologi->id_biodata)->first();
            //     $biodata->alamat = $request->alamat;
            //     $biodata->pengalaman = $request->pengalaman;
            //     $biodata->universitas = $request->universitas;
            //     $biodata->no_hp = $request->no_hp;
            //     $biodata->pekerjaan = $request->pekerjaan;
            // }

            $file = $request->file('img');
                if($file != ""){
                    $files = $_FILES['img'];
                    // echo "<pre>";
                    // print_r($files['size']);
                    // exit;
                    if($files['size'] > 500000){
                        return 'file_besar';
                    }

                    $year_folder = date("Y");
                    $month_folder = date("m");
                    $day_file = date("d_h_i_s");
                    $path = '/uploads/klien/'. $year_folder . '/' . $month_folder .'/';
                        
                        if (!file_exists(public_path().$path)) { 
                            mkdir(public_path() .$path, 0777, true); 
    
                        }
                    $ext = $file->getClientOriginalExtension();
                    $fileName = strtolower('_' . $day_file .  '.' .$ext);
                    $image = Image::make($request->file('img'));
                    $biodata->img = $path . $fileName ;
                    $image->save(public_path().$path. $fileName);
                    //$path = public_path('uploads/' . $fileName);
                    //Image::make($file->getRealPath())->resize(120, 120)->save($path);
                }

            $biodata->nama = $request->nama;
            $biodata->tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
            $biodata->jenis_kelamin = $request->jenis_kelamin;

            $biodata->jenis_identitas = $request->jenis_identitas;
            $biodata->no_identitas = $request->no_identitas;
            $biodata->tempat_lahir = $request->tempat_lahir;
            $biodata->agama = $request->agama;
            $biodata->id_user = Auth::user()->id;
            $biodata->status_perkawinan = $request->status_perkawinan;
            $biodata->alamat = $request->alamat;
            $biodata->pekerjaan = $request->pekerjaan;
            
            $biodata->updated_at = date('Y-m-d H:i:s');

            if(!$biodata->save()){
                
            }

            $psikolog = Psikologi::
                        leftJoin('biodata','biodata.id_biodata','=','psikologi.id_biodata')
                        ->where('psikologi.id_user',Auth::user()->id)->first();
            DB::commit();
                return 'berhasil';
            

        } catch (\Exception  $e) {
            DB::rollback();
            print_r($e->getMessage());
            exit;
            return back()->withError($e->getMessage())->withInput();
        }
    }
    
        
}
