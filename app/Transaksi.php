<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class Transaksi extends Model
{
    protected $table = "transaksi";
    protected $primaryKey = 'id_transaksi';

    public function klien()
    {
        return $this->hasOne('App\User','id','id_user');
    }

    public function psikologi(){
        return $this->hasOne('App\Psikologi','id_psikologi','id_psikologi');
    }

    public function subKategori(){
        return $this->hasOne('App\SubKategori','id_sub_kategori','id_kategori_konsul');
    }
    public function media(){
        return $this->hasOne('App\Media','id_media','id_media');
    }

    public function laporanKlien(){
        return $this->hasOne('App\LaporanKlien','id_transaksi','id_transaksi');
    }
    public function jadwalKonsul(){
        return $this->hasOne('App\JadwalKonsul','id_jadwal_konsul','id_jadwal_konsul');
    }
    public static function kasih_reting(){
        $cek = array();
        if(!empty(Auth::user()->id)){
            $cek = Transaksi::where('status_bayar','settlement')
            ->leftJoin('media','media.id_media','=','transaksi.id_media')
            ->where('jadwal','<', date('Y-m-d'))
            ->where('reting',null)
            ->where('id_user', Auth::user()->id)
            ->orderBy('jadwal','asc')
            ->first();
        }
        return $cek;
    }
}