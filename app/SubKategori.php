<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $code
*/
class SubKategori extends Model
{
    protected $table = "sub_kategori";
    protected $primaryKey = 'id_sub_kategori';

}