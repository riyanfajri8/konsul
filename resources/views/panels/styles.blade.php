{{-- Vendor Styles --}}
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600">
<link rel="stylesheet" href="/vendors/css/vendors.min.css">
<link rel="stylesheet" href="/vendors/css/ui/prism.min.css">

<!-- Theme Styles -->
<link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('css/components.css') }}">

{{-- {!! Helper::applClasses() !!} --}}



    <!-- <link rel="stylesheet" href="{{ url('css/themes/dark-layout.css') }}"> -->

    <link rel="stylesheet" href="{{ url('css/themes/semi-dark-layout.css') }}">


<!-- Page Styles -->
<link rel="stylesheet" href="{{ url('css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" href="{{ url('css/style.css') }}">