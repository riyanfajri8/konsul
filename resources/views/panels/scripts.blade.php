{{-- Vendor Scripts --}}
<script src="/vendors/js/vendors.min.js"></script>
<script src="/vendors/js/ui/prism.min.js"></script>

<!-- Theme Scripts -->
<script src="{{ url('js/core/app-menu.js') }}"></script>
<script src="{{ url('js/core/app.js') }}"></script>

<!-- Page Scripts -->