@extends('konsul.layouts.app2')
@section('content')

<section class="ftco2-section bg-light">
<div class="reset-password">
    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <h2 style="color:#555; font-weight:500;">Reset Password</h2>
            <p class="mb-3" style="color:#c4c4c4;">Silahkan masukkan email anda untuk reset password</p>
            <hr class="mb-5">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-4">
            <form method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <!-- <h1>
                            <div class="login-logo">
                                <a href="#">
                                    PSYQONSUL
                                </a>
                                <a href="" class="btn btn-secondary2"> Kembali</a>
                            </div>
                        </h1> -->
                <p class="text-muted"></p>
                <div>
                    {{ csrf_field() }}
                    <div class="form-group-bulat">
                        <label class="label-bulat" for="name">Email</label>
                        <input type="email" name="email" class="form-control-bulat" required="autofocus" placeholder="">
                        @if($errors->has('email'))
                        <em class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </em>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-success">
                            Reset Password
                        </button>
                        <button type="submit" class="btn btn-primary" value="Go Back" onclick="history.back(-1)">
                            Batal
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</section>
@endsection