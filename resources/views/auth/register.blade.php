<!-- Description -->
@extends('konsul.layouts.app1')

@section('content')
<style>
body {
    font-family: 'Montserrat', sans-serif;
    color: #5b5757;
}
</style>
<section class="row " style="padding-top:150px;">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">    
        <div class="card bg-authentication rounded-0 mb-0">
            <div class="row m-0">
           
                <div class="col-lg-12 col-12 p-0">
                    <div class="card rounded-0 mb-0 p-2">
                        <div class="card-header pt-50 pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Buat Akun</h4>
                            </div>
                        </div>
                        <p class="px-2">Silahkan isi data dengan lengkap.</p>
                        <div class="card-content" style="background-color: white;">
                            <div class="card-body pt-0">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                <form action="" method="POST" files="true" enctype="multipart/form-data">
                                @csrf
                                    <div class="form-group">
                                        <label for="inputName">Nama lengkap</label>
                                        <input type="text" id="inputName" class="form-control" placeholder="Nama anda" required name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Tanggal Lahir</label>
                                            <input id="datepicker" name="tanggal_lahir" class="form-control date">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Jenis Kelamin</label>
                                        <br>
                                        <label class="radio-inline"  style="color:black"><input type="radio"  name="jenis_kelamin" value="Laki-laki" checked>Laki - laki</label>
                                        <label class="radio-inline" style="color:black"><input type="radio" name="jenis_kelamin" value="Perempuan">Perempuan</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input type="email" id="inputEmail" class="form-control" placeholder="Email" required name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword">Password</label>
                                        <input type="password"  class="form-control" placeholder="Password" required name="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputConfPassword">Confirm Password</label>
                                        <input type="password" id="inputConfPassword" class="form-control" placeholder="Konfirmasi Password" required name="password_confirmation">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword">No telp</label>
                                        <input type="number"  class="form-control" placeholder="Nomor Telepon" required name="no_telp">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword">Alamat</label>
                                        <textarea class="form-control" placeholder="Alamat lengkap" required name="address"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-12 form-group ">
                                              <div class="vs-checkbox-con vs-checkbox-primary">
                                                <input  type="checkbox" checked>
                                                <span class="vs-checkbox">
                                                  <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                  </span>
                                                </span>
                                                <span class="" style='color: red;'> Saya setuju akan memberikan data-data yang benar dan jujur sesuai dengan masalah yang ingin dikonsultasikan dan atau yang diminta oleh psikolog yang menangani. Dengan mengklik persetujuan dibawah yang berarti saya berkomitment untuk mengikuti / menjalani arahan-arahan dari psikolog semampu mungkin.</span>
                                              </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            {!! Form::label('img', 'Image', ['class' => 'control-label']) !!}
                                            <br>
                                        
                                            <img id="uploadPreview"  style="width: 150px;" />
                                            <input class="form-control" id="uploadImage" type="file" name="img" onchange="PreviewImage();" />
                                            
                                            <p class="help-block"></p>
                                            @if($errors->has('img'))
                                                <p class="help-block">
                                                    {{ $errors->first('img_name') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>  

                                    <a href="{{url('/')}}" class="btn btn-outline-primary float-left btn-inline mb-50">Back</a>
                                    <button type="submit" class="btn btn-primary float-right btn-inline mb-50">Register</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ HTML Markup -->

@endsection
<script type="text/javascript">  
    function PreviewImage() {  
        var oFReader = new FileReader();  
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);  
        oFReader.onload = function (oFREvent) {  
            document.getElementById("uploadPreview").src = oFREvent.target.result;  
        };  
    };  
</script> 

@section('scripts')
@parent

@endsection