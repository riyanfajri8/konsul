<!-- Description -->
@extends('konsul.layouts.app2')

@section('content')
<style>
body {
    font-family: 'Montserrat', sans-serif;
}
.datepicker table {
    margin: 0;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    color: #2d2c2b;
}

@media (min-width: 992px) {
    .heading-section{
    margin-top: 150px;
}
}
@media (min-width: 1200px) {
    .heading-section{
    margin-top: 150px;
}
}
</style>

<div class="container" data-aos="zoom-out" data-aos-delay="100" style="margin-top: 90px;">    
    <div class="heading-section">
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <h2 class="text-center">Form Pendaftaran Psikolog</h2>
        <div class="col-md-8 offset-md-2 text-center">
            <blockquote class="blockquote text-center mb-4">
                <footer class="blockquote-footer">Mohon diisi dengan data yang benar dan lengkap.</footer>
                <footer class="blockquote-footer">Verifikasi data calon Psikolog PsyQonsul akan dilakukan dalam 1 x 24 jam.</footer>
            </blockquote>
        </div>
    <div class="login-wrap2 p-4 mt-3">
        <form action="" method="POST" files="true" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="name">Nama Lengkap dan Gelar</label>
                        <input type="text" id="inputName" class="form-control" placeholder="" required name="name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="name">Email</label>
                        <input type="email" id="inputEmail" class="form-control" placeholder="" required name="email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="email">No.Handphone / WA</label>
                        <input type="number"  class="form-control"  required name="no_telp">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                            <label class="label" for="jk">Jenis Kelamin</label>
                            <select id="personal-1-jenis_kelamin" required class="form-control" name="jenis_kelamin">
                                <option value="Laki-laki">Laki-laki
                                </option>
                                <option value="Perempuan">Perempuan
                                </option>
                            </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input type="password"  class="form-control" placeholder="" required name="password">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="inputConfPassword">Ulangi Password</label>
                        <input type="password" id="inputConfPassword" class="form-control" placeholder="" required name="password_confirmation">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="tempatlahir">Tempat Lahir</label>
                        <input type="text"  class="form-control"  required name="tempatlahir">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="tgllahir">Tanggal Lahir</label>
                        <input id="datepicker" name="tanggal_lahir" class="form-control date">
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label>Pendidikan Terakhir</label>
                        <select id="personal-1-id_pendidikan" required class="form-control" name="pendidikan_terakhir">
                            <option value="">Pilih</option>
                            <option value="Belum Sekolah">Belum Sekolah</option>
                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                            <option value="TK">TK</option>
                            <option value="SD">SD</option>
                            <option value="SLTP">SLTP</option>
                            <option value="SLTA">SLTA</option>
                            <option value="D1">D1</option>
                            <option value="D3">D3</option>
                            <option value="D4">D4</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                            <option value="dr">dr</option>
                            <option value="Belum Tamat Sekolah">Belum Tamat Sekolah</option>
                        </select>
                    </div>
                </div> -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Status Perkawinan</label>
                        <select id="personal-1-status_perkawinan" required class="form-control" name="status_perkawinan">
                            <option value="">---</option>
                            <option value="Belum Menikah">Belum Menikah</option>
                            <option value="Menikah" >Menikah </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Pekerjaan Sekarang</label>
                        <select id="personal-1-pekerjaan" required class="form-control" name="pekerjaan">
                            <option value="">---</option>
                            <option value="Akademik/Pengajar">Akademik/Pengajar</option>
                            <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                            <option value="Karyawan BUMN">Karyawan BUMN</option>
                            <option value="Karyawan Swasta" data-select2-id="6">Karyawan Swasta</option>
                            <option value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS) </option>
                            <option value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>
                            <option value="Pemerintahan / Militer">Pemerintahan / Militer</option>
                            <option value="Pensiunan">Pensiunan</option>
                            <option value="Petani">Petani</option>
                            <option value="Profesional ( Dokter, Pengacara, Dll )">Profesional ( Dokter, Pengacara, Dll )</option>
                            <option value="Tidak Bekerja">Tidak Bekerja</option>
                            <option value="Wiraswasta">Wiraswasta</option>
                            <option value="Lain-lain">Lain-lain</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="label" for="sipp">SIPP</label>
                        <input type="text"  class="form-control"  required name="sipp">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="label" for="pengalaman">Pengalaman Psikolog selama (Tahun)</label>
                        <input type="number"  class="form-control"  required name="pengalaman">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <?php 
                    
                        $pendidikan = DB::table('pendidikan')->get();
                    ?>
                        <label class="label" for="sipp">Pendidikan</label>
                        <select onchange="pendidikanPilih()" id="pilih_pendidikan" required class="form-control" name="pilih_pendidikan">
                        <option value="">---</option>
                        @foreach($pendidikan as $pendidikan1)
                            <option  value="{{$pendidikan1->id_pendidikan}}">{{$pendidikan1->nama}}</option>
                        @endforeach
                        </select>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group" id="pendidikan_sub">
                    
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="universitas">Universitas</label>
                        <input type="text"  class="form-control"  required name="universitas">
                    </div>
                </div>
            </div>      
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="email">Alamat Lengkap</label>
                        <textarea type="text"  class="form-control"  required name="alamat"></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="label" for="tentang diri">Informasi Tentang Diri Anda</label>
                        <textarea name="tentang" class="form-control" required> </textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    {!! Form::label('img', 'Upload KTP (Format file : png, jpg, jpeg . Ukuran Maks 1 MB)', ['class' => 'control-label']) !!}
                    <br>
                
                    <img id="uploadPreview"  style="width: 150px;" />
                    <input class="form-control" id="uploadImage" type="file" name="foto_ktp" onchange="PreviewImage();" style="line-height: 2.5;" />
                    
                    <p class="help-block"></p>
                    @if($errors->has('img'))
                        <p class="help-block">
                            {{ $errors->first('img_name') }}
                        </p>
                    @endif
                </div>
                <div class="col-md-6 form-group">
                    {!! Form::label('img', 'Upload Foto Profil ( Aspek Ratio 1 : 1 atau persegi. Ukuran Maks 1 MB)', ['class' => 'control-label']) !!}
                    <br>
                
                    <img id="uploadPreview1"  style="width: 150px;" />
                    <input class="form-control" id="uploadImage1" type="file" name="img" onchange="PreviewImagea();" style="line-height: 2.5;" />
                    
                    <p class="help-block"></p>
                    @if($errors->has('img'))
                        <p class="help-block">
                            {{ $errors->first('img_name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row mt-2 mb-2">
                <div class="col-md-8 offset-md-2 form-group text-center">
                    <div class="vs-checkbox-con vs-checkbox-primary">
                        <input  type="checkbox" checked>
                        <span class="vs-checkbox">
                            <span class="vs-checkbox--check">
                            <i class="vs-icon feather icon-check"></i>
                            </span>
                        </span>
                        <span class="syarat"> Saya setuju dan menerima syarat, kesepakatan hak dan kewajiban, serta ketentuan - ketentuan yang berlaku seperti yang telah ditetapkan oleh PSYQONSUL.</span>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 offset-md-4">
                    <button type="submit" class="btn btn-primary">Daftar Sebagai Psikolog</button>
                </div>
            </div>
        </form>    
      </div>


        <!-- <div class="card bg-authentication rounded-0 mb-0">
            <div class="row m-0">
           
                <div class="col-lg-12 col-12 p-0">
                    <div class="card rounded-0 mb-0 p-2">
                        <div class="card-header pt-50 pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Buat Akun</h4>
                            </div>
                        </div>
                        <p class="px-2">Silahkan isi data dengan lengkap.</p>
                        <div class="card-content">
                            <div class="card-body pt-0">
                            @if($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                <form action="" method="POST" files="true" enctype="multipart/form-data">
                                @csrf
                                    <div class="form-group">
                                        <label for="inputName">Nama lengkap</label>
                                        <input type="text" id="inputName" class="form-control" placeholder="Nama anda" required name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Tanggal Lahir</label>
                                            <input id="datepicker" name="tanggal_lahir" class="form-control date">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Jenis Kelamin</label>
                                        <br>
                                        <label class="radio-inline"><input type="radio" value="Laki - laki"  name="jenis_kelamin" checked>Laki - laki</label>
                                        <label class="radio-inline"><input type="radio" value="Perempuan" name="jenis_kelamin">Perempuan</label>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input type="email" id="inputEmail" class="form-control" placeholder="Email" required name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword">Password</label>
                                        <input type="password"  class="form-control" placeholder="Password" required name="password">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputConfPassword">Confirm Password</label>
                                        <input type="password" id="inputConfPassword" class="form-control" placeholder="Konfirmasi Password" required name="password_confirmation">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword">No telp</label>
                                        <input type="number"  class="form-control" placeholder="Nomor Telepon" required name="no_telp">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword">Alamat</label>
                                        <textarea class="form-control" placeholder="Alamat lengkap" required name="address"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-12 form-group ">
                                              <div class="vs-checkbox-con vs-checkbox-primary">
                                                <input  type="checkbox" checked>
                                                <span class="vs-checkbox">
                                                  <span class="vs-checkbox--check">
                                                    <i class="vs-icon feather icon-check"></i>
                                                  </span>
                                                </span>
                                                <span class=""> Saya terima syarat dan ketentuan ini.</span>
                                              </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            {!! Form::label('img', 'Image', ['class' => 'control-label']) !!}
                                            <br>
                                        
                                            <img id="uploadPreview"  style="width: 150px;" />
                                            <input class="form-control" id="uploadImage" type="file" name="img" onchange="PreviewImage();" />
                                            
                                            <p class="help-block"></p>
                                            @if($errors->has('img'))
                                                <p class="help-block">
                                                    {{ $errors->first('img_name') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>  

                                    <a href="{{url('/')}}" class="btn btn-outline-primary float-left btn-inline mb-50">Back</a>
                                    <button type="submit" class="btn btn-primary float-right btn-inline mb-50">Register</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
<!--/ HTML Markup -->

@endsection
<script type="text/javascript">  
    
    function PreviewImage() {  
        var oFReader = new FileReader();  
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);  
        oFReader.onload = function (oFREvent) {  
            document.getElementById("uploadPreview").src = oFREvent.target.result;  
        };  
    };  

    function PreviewImagea() {  
        var oFReader = new FileReader();  
        oFReader.readAsDataURL(document.getElementById("uploadImage1").files[0]);  
        oFReader.onload = function (oFREvent) {  
            document.getElementById("uploadPreview1").src = oFREvent.target.result;  
        };  
    }; 
</script> 
@section('scripts')
<script>
    function pendidikanPilih(){
        var id_pendidikan =$('#pilih_pendidikan').val();
            $('#pendidikan_sub').load('{{url("/sub_pendidikan")}}?id_pendidikan='+id_pendidikan, function(e) {});
    }
</script>
@parent

@endsection