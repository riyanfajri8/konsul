<?php
use App\Transaksi;
?>
@extends('layouts.admin1')

@section('content')
    <h3 class="page-title">Transaksi</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            
        </div>

        <div class="card-body">
            <div class="row">
            @foreach ($transaksi as $transaksi2)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua">
                            <img src="{{asset($transaksi2->psikologi->foto)}}">
                        </span>
                        <?php
                            $total_tranksasi_bayar = Transaksi::select('id_psikologi')->where('status_bayar','settlement')->where('id_psikologi',$transaksi2->id_psikologi)->where('jadwal','>',date('Y-m-d'))->groupBy('id_psikologi')->count();
                        ?>
                        <div class="info-box-content">
                            <span class="info-box-text">{{ $transaksi2->psikologi->nama}}</span>
                                <label></label><span class="info-box-number">{{$total_tranksasi_bayar}} <small>Akan Berlangsung</small></span>
                        </div>
                    <!-- /.info-box-content -->
                    </div>
                </div>
                <!-- /.info-box -->
            @endforeach
            </div>
        </div>

            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                    <thead>
                        <tr>
                        <th width="10">

                        </th>
                            <th>Psikolog</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @if (count($transaksi) > 0)
                            @foreach ($transaksi as $transaksi1)
                                <tr data-entry-id="{{ $transaksi1->id_psikologi }}">
                                    
                                        <td></td>
                                    <td>{{ $transaksi1->psikologi->nama}}</td>
                                    <td>
                                        <a href="{{ route('admin.transaksi.views',[$transaksi1->id_psikologi]) }}" class="btn btn-xs btn-info">Lihat Transaksi</a>                                    
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.transaksi.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Permission:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection