<?php
use App\Lib;
?>
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

.container {
    max-width: 1400px !important;
}

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
.modal-lg, .modal-xl {
    max-width: 1400px;
}
</style>
<div class="row">
    <div class="col-lg-4">
        <div class="panel-body"> 
            <img src="{{$transaksi->klien->biodata->img}}" style="width: 421px;" class="img-rounded" alt="Cinque Terre">
        </div>
    </div>
  
    <div class="col-lg-8">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tabs">
                    
                        <div class="m-portlet__body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="m_tabs_6_1"
                                    role="tabpanel">
                                    <div class="row">
                                        <div class="col-lg-6">
                                        <h3>Data Klien</h3>
                                            <div class="m-section">
                                                <div class="m-section__content">
                                                    <table
                                                        class="table m-table m-table--head-separator-secondary">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Nama
                                                                    Lengkap</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->nama}}</th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Nomor
                                                                    Identitas</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->no_identitas}}</th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Jenis
                                                                    Kelamin</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">
                                                                {{$transaksi->klien->biodata->jenis_kelamin}}</th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">
                                                                    Tempat &amp; Tgl Lahir
                                                                </th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->tanggal_lahir}} ,
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">
                                                                    Status Perkawinan</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->status_perkawinan}}</th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Agama
                                                                </th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->agama}}
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">
                                                                    Pendidikan</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->pendidikan_terakhir}}
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">
                                                                    Pekerjaan</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">
                                                                {{$transaksi->klien->biodata->pekerjaan}}</th>
                                                            </tr>
                                                        </thead>

                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">
                                                                    Alamat KTP</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">{{$transaksi->klien->biodata->alamat}}</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Nomor
                                                                    HP</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">
                                                                {{$transaksi->klien->biodata->no_hp}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Email
                                                                </th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">
                                                                    {{$transaksi->klien->email}}
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <h3>Data Perjanjian</h3>
                                            <div class="m-section">
                                                <div class="m-section__content">
                                                    <table
                                                        class="table m-table m-table--head-separator-secondary">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">No Transaksi </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->no_transaksi}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Nama Psikolog </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->psikologi->nama}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Jadwal </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->jadwal}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Jam </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->jam}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Media </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->media->nama_media}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Masalah </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->masalah}} </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Type Pembayaran </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->type_pembayaran}} </th>
                                                            </tr>
                                                        </thead>
                                                        @if(!empty($transaksi->bank))
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Bank </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->bank}} </th>
                                                            </tr>
                                                        </thead>
                                                        @endif
                                                        @if(!empty($transaksi->va_number))
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Va Number </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->va_number}} </th>
                                                            </tr>
                                                        </thead>
                                                        @endif
                                                        @if(!empty($transaksi->biller_code))
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Biller Code </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->biller_code}} </th>
                                                            </tr>
                                                        </thead>
                                                        @endif
                                                        @if(!empty($transaksi->bill_key))
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Bill Key </th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->bill_key}} </th>
                                                            </tr>
                                                        </thead>
                                                        @endif
                                                        @if(!empty($transaksi->store))
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Store</th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->store}} </th>
                                                            </tr>
                                                        </thead>
                                                        @endif
                                                        @if(!empty($transaksi->payment_code))
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Store</th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left">{{$transaksi->payment_code}} </th>
                                                            </tr>
                                                        </thead>
                                                        @endif
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Pembayaran Psikolog</th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left"><?= Lib::rupiah($psikolgi_kateogri->harga) ?> </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Pembayaran Media</th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left"><?= Lib::rupiah($transaksi->media->harga) ?> </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Total Media + Psikolog</th>
                                                                <th class="text-center">:</th>
                                                                <th class="text-left"><?= Lib::rupiah($psikolgi_kateogri->harga + $transaksi->media->harga) ?> </th>
                                                            </tr>
                                                        </thead>
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">Status </th>
                                                                <th class="text-center">:</th>
                                                                @if($transaksi->status_bayar == 'pending')
                                                                    <th class="text-left">Belum Bayar</th>
                                                                @elseif($transaksi->status_bayar == 'settlement')
                                                                    <th class="text-left">Sudah Dibayar</th>
                                                                @else
                                                                    <th class="text-left">{{$transaksi->status_bayar}} </th>
                                                                @endif
                                                                
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="tab-pane" id="m_tabs_6_3" role="tabpanel">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="m-section">
                                                <div class="m-section__content">
                                                    <table
                                                        class="table m-table m-table--head-separator-secondary">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-right">
                                                                    Pekerjaan</th>
                                                                <th class="text-center">:
                                                                </th>
                                                                <th class="text-left">
                                                                {{$transaksi->klien->biodata->pekerjaan}}</th>
                                                            </tr>
                                                        </thead>
                                                        

                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div><!-- end row -->
    </div>
</div>
