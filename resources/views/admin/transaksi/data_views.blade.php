
                    @if (count($transaksi) > 0)
                        @foreach ($transaksi as $transaksi)
                            <tr data-entry-id="{{ $transaksi->id_psikologi }}">
                                
                                    <td></td>
                                <td> {{ $transaksi->psikologi->nama}} <label onclick="lihatPerjanjian('<?= $transaksi->id_transaksi ?>')" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Lihat</label></td>
                                <td>{{ $transaksi->klien->name}}</td>
                                <td>{{ $transaksi->no_transaksi}}</td>
                                <td>{{ $transaksi->subKategori->nama_sub_kategori}}</td>
                                <td>{{ $transaksi->media->nama_media}}</td>
                                <td>{{ $transaksi->jadwal}}</td>
                                <td>{{ $transaksi->jam}}</td>
                                <td>{{ $transaksi->masalah}}</td>
                                <td>{{ $transaksi->laporanKlien->total_konsul}}</td>
                                @if($transaksi->status_bayar == 'pending')
                                    <td>Belum Bayar</td>
                                @elseif($transaksi->status_bayar == 'settlement')
                                    <td>Sudah Dibayar</td>
                                @else
                                    <td>{{$transaksi->status_bayar}}</td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">Data Tidak Ada</td>
                        </tr>
                    @endif

