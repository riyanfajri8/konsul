<?php
use App\Lib;
?>
@extends('layouts.admin1')

@section('content')
    <h3 class="page-title">Transaksi</h3>

    <div class="panel panel-default">
    <div calss="row">
            <div class="col-lg-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black" style="background: url('../dist/img/photo1.png') center center;">
                        <h3 class="widget-user-username">{{$psikolog->nama}}</h3>
                        <h5 class="widget-user-desc">Psikolog</h5>
                        </div>
                        <div class="widget-user-image">
                        <img class="img-circle" src="{{asset($psikolog->foto)}}" alt="User Avatar">
                        </div>
                        <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$total_tranksasi_bayar}}</h5>
                                <span class="description-text">Perjanjian</span>
                            </div>
                            <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$total_tranksasi_bayar_sudah}}</h5>
                                <span class="description-text">Perjanjian Sukses</span>
                            </div>
                            <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">{{$pending}}</h5>
                                <span class="description-text">Belum Bayar</span>
                            </div>
                            <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                    <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>

            <div class="col-lg-4">
                <div class="btn-group btn-group-lg">
                    <button onclick="dataTransaksi('all')" type="button" class="btn btn-primary">All</button>
                    <button onclick="dataTransaksi('settlement')" type="button" class="btn btn-primary">Sudah bayar</button>
                    <button onclick="dataTransaksi('pending')" type="button" class="btn btn-primary">Pending</button>
                    <button onclick="dataTransaksi('expire')" type="button" class="btn btn-primary">Expired</button>
                </div> 
            </div>

        </div>

        <div class="card-body" >
        

        <div class="panel-heading">
            
        </div>

            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                    <thead>
                        <tr>
                        <th width="10">

                        </th>
                            <th>Psikolog</th>
                            <th>Nama Klien</th>
                            <th>No Transaksi</th>
                            <th>Kategori Konsul</th>
                            <th>Media</th>
                            <th>Jadwal</th>
                            <th>Jam</th>
                            <th>Masalah</th>
                            <th>Total Konsul</th>
                            <th>Status</th>
                            
                        </tr>
                    </thead>
                    
                    <tbody id="dataTransaksi">
                        @if (count($transaksi) > 0)
                            @foreach ($transaksi as $transaksi)
                                <tr data-entry-id="{{ $transaksi->id_psikologi }}">
                                    
                                        <td></td>
                                    <td> {{ $transaksi->psikologi->nama}} <label onclick="lihatPerjanjian('<?= $transaksi->id_transaksi ?>')" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Lihat</label></td>
                                    <td>{{ $transaksi->klien->name}}</td>
                                    <td>{{ $transaksi->no_transaksi}}</td>
                                    <td>{{ $transaksi->subKategori->nama_sub_kategori}}</td>
                                    <td>{{ $transaksi->media->nama_media}}</td>
                                    <td>{{ $transaksi->jadwal}}</td>
                                    <td>{{ $transaksi->jam}}</td>
                                    <td>{{ $transaksi->masalah}}</td>
                                    <td><?= Lib::rupiah($transaksi->laporanKlien->total_konsul) ?></td>
                                    @if($transaksi->status_bayar == 'pending')
                                        <td>Belum Bayar</td>
                                    @elseif($transaksi->status_bayar == 'settlement')
                                        <td>Sudah Dibayar</td>
                                    @else
                                        <td>{{$transaksi->status_bayar}}</td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Transaksi</h4>
      </div>
      <div class="modal-body" id="detail_transaksi">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('scripts')
@parent
<script>
function dataTransaksi(id){
    $('#dataTransaksi').load('{{url("admin/transaksi/views")}}/<?=$id?>?status='+id, function(e) {});
}
function lihatPerjanjian(id){
    $('#detail_transaksi').load('{{url("admin/transaksi/detail")}}/'+id, function(e) {});
}
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.transaksi.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Permission:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection