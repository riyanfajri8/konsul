<div class="panel panel-primary">

    <div class="panel-body">
        <div class="row">
            <div class="col-xl-5">
                <div class="modal-content">
                            <img
                                    src="{{asset($psikologi->foto)}}"
                                    class="img-thumbnail" alt="profile-image">
                </div>
                <!-- <img src="{{asset($psikologi->foto)}}" class="img-rounded" alt=""> -->
            </div>
            <div class="col-xl-7">
                <div class="post-block_keterangan">
                   
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td class="table-key"><i class="fas fa-chair"></i> Nama Psikologi</td>
                                    <td>
                                        {{$psikologi->nama}}
                                    </td>
                                </tr>
    
                                <tr>
                                    <td class="table-key"><i class="fas fa-chair"></i> SIP</td>
                                    <td>
                                        {{$psikologi->sip}}
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td> Pendidikan</td>
                                    <td>
                                        {{$psikologi->universitas}}
                                    </td>
                                </tr>

                                <tr>
                                    <td> Pengalaman</td>
                                    <td>
                                        {{$psikologi->pengalaman}}
                                    </td>
                                </tr>
                                <tr>
                                    <td> Bidang Keahliah</td>
                                    <td>
                                        <ul>   
                                            @foreach($kategori_psi as $kategori_psi) 
                                                <li>{{$kategori_psi->nama_sub_kategori}}</li>      
                                            @endforeach                                  
                                        </ul>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>

                </div>
            </div>
        </div>
    </div>
</div>