@extends('layouts.admin1')

@section('content')
<h3 class="page-title">Permintaan Psikolog</h3>

    <div class="panel panel-default">
        <div class="card-body" >
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
                    <thead>
                        <tr>
                        <th width="10">

                        </th>
                            <th>Psikolog</th>
                            <th>SIP</th>
                            <th>tanggal lahir</th>
                            <th>Jenis Kelamin</th>
                            <th>Status</th>
                            <th>No Hp</th>
                            <th>Agama</th>
                            <th></th>
                        </tr>
                    </thead>
                    
                    <tbody id="dataTransaksi">
                        @if (count($permintaan_psikolog) > 0)
                            @foreach ($permintaan_psikolog as $permintaan_psikolog)
                                <tr data-entry-id="{{ $permintaan_psikolog->id_psikologi }}">
                                    
                                        <td></td>
                                    <td> {{ $permintaan_psikolog->nama}} <label onclick="lihatDetail('<?= $permintaan_psikolog->id_psikologi ?>')" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Lihat</label></td>
                                    <td>{{ $permintaan_psikolog->sip }}</td>
                                    <td>{{ $permintaan_psikolog->biodata->tanggal_lahir}}</td>
                                    <td>{{ $permintaan_psikolog->biodata->jenis_kelamin}}</td>
                                    <td>{{ $permintaan_psikolog->status}}</td>
                                    <td>{{ $permintaan_psikolog->biodata->no_hp}}</td>
                                    <td>{{ $permintaan_psikolog->biodata->agama}}</td>
                                    <td><button onclick="terima('<?= $permintaan_psikolog->id_psikologi ?>')" type="button" class="btn btn-block btn-primary">Terima</button>
                                    <button onclick="tolak('<?= $permintaan_psikolog->id_psikologi ?>')" type="button" class="btn btn-block btn-danger btn-xs">Tolak</button></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">tidak ada data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Profil Psikolog</h4>
      </div>
      <div class="modal-body" id="profil_psikolog">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('scripts')
@parent
<script>
function terima(id){
    if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
            method: 'GET',
            url: "{{url('admin/permintaan-psikolog/terima')}}/"+id,
            data: 'D'
        })
        .done(function () { 
            location.reload() 
        })
    }
}

function tolak(id){
    if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
            method: 'GET',
            url: "{{url('admin/permintaan-psikolog/tolak')}}/"+id,
            data: 'D'
        })
        .done(function () { 
            location.reload() 
        })
    }
}
function lihatDetail(id){
    $('#profil_psikolog').load('{{url("admin/permintaan-psikolog/profil")}}/?id='+id, function(e) {});
}
function lihatPerjanjian(id){
    $('#detail_transaksi').load('{{url("admin/transaksi/detail")}}/', function(e) {});
}
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.transaksi.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Permission:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection