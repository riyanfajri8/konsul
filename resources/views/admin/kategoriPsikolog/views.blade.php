
@extends('layouts.admin1')

@section('content')

<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
    <div class="inner">
        <h3>{{$countKategori}}<sup style="font-size: 20px">Kategori</sup></h3>

        <p>{{$kategoriPsikolog->psikologi->nama}}</p>
    </div>
    <div class="icon">
        <i class="ion ion-stats-bars"></i>
    </div>
    <a href="{{url('admin/kategori-psikolog/views',[$kategoriPsikolog->id_psikologi])}}" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
    </a>
    </div>
</div>

<div class="row">
    @foreach($kategori as $kategoris)
    <?php
        $sub_kategoris = DB::table('kategori_konsul_sub_kategori')
            ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
            ->leftJoin('psikologi_kategori_konsul','psikologi_kategori_konsul.id_kategori_konsul', '=', 'sub_kategori.id_sub_kategori')
            ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategoris->id_kategori_konsul)
            ->where('psikologi_kategori_konsul.id_psikologi',$id)
            ->get();
    ?>
    @if(count($sub_kategoris) > 0)
        <div class="col-md-4">
            <div class="box box-warning box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$kategoris->nama_konsul}}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="">
                    <ul>
                        @foreach($sub_kategoris as $sub_kategori)
                            <li>{{$sub_kategori->nama_sub_kategori}} <b>Rp {{$sub_kategori->harga}}</b></li>
                        @endforeach
                    </ul>
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    @endif
    @endforeach
</div>

@endsection
@section('scripts')
@parent

@endsection