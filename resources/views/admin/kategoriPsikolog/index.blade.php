<?php
use App\Transaksi;
use App\PsikologiKategoriKonsul;
?>
@extends('layouts.admin1')

@section('content')
<div class="row">
    @foreach($kategoriPsikolog as $kategoriPsikolog)
        <?php
             $countKategori = PsikologiKategoriKonsul::select('id_psikologi')->where('id_psikologi',$kategoriPsikolog->id_psikologi)->groupBy('id_psikologi')->count();
        ?>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
            <div class="inner">
                <h3>{{$countKategori}}<sup style="font-size: 20px">Kategori</sup></h3>

                <p>{{$kategoriPsikolog->psikologi->nama}}</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url('admin/kategori-psikolog/views',[$kategoriPsikolog->id_psikologi])}}" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
            </div>
        </div>
    @endforeach
</div>
   
@endsection
@section('scripts')
@parent

@endsection