<?php
    use App\Transaksi;
    use App\LaporanKlien;
    use App\Lib;
?>
<div class="row">
    @foreach ($transaksi as $transaksi2)
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <img src="{{asset($transaksi2->psikologi->foto)}}">
                </span>
                <?php
                    $total_tranksasi_bayar = LaporanKlien::select(DB::raw('SUM(total_konsul) as total'), 'laporan_klien.id_psikologi as id_psikologi')->leftJoin('transaksi','transaksi.id_transaksi','=','laporan_klien.id_transaksi')->where('status_bayar','settlement')->where('laporan_klien.id_psikologi',$transaksi2->id_psikologi)
                    ->where('jadwal','>=',$dateForm)
                    ->where('jadwal','<=',$dateTo)
                    ->groupBy('laporan_klien.id_psikologi')->get();

                    if(!empty($total_tranksasi_bayar[0]['total'])){
                        $total = $total_tranksasi_bayar[0]['total'];
                    }else{
                        $total = '0';
                    }
                ?>
                <div class="info-box-content">
                    <span class="info-box-text">{{ $transaksi2->psikologi->nama}}</span>

                        <label></label><span class="info-box-number"><?= Lib::rupiah($total) ?> <small>Total Pendapatan</small></span>
                </div>
            <!-- /.info-box-content -->
            </div>
        </div>
        <!-- /.info-box -->
    @endforeach
</div>

<div class="table-responsive">
    <table class=" table table-bordered table-striped table-hover datatable datatable-Permission">
        <thead>
            <tr>
            <th width="10">

            </th>
                <th>Psikolog</th>
                <th>Nama Klien</th>
                <th>No Transaksi</th>
                <th>Kategori Konsul</th>
                <th>Media</th>
                <th>Jadwal</th>
                <th>Jam</th>
                <th>Masalah</th>
                <th>Total Konsul</th>
                <th>Status</th>
                
            </tr>
        </thead>
        
        <tbody id="dataTransaksi">
            @if (count($list) > 0)
                @foreach ($list as $list)
                    <tr data-entry-id="{{ $list->id_psikologi }}">
                        
                            <td></td>
                        <td> {{ $list->psikologi->nama}} <label onclick="lihatPerjanjian('<?= $list->id_transaksi ?>')" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Lihat</label></td>
                        <td>{{ $list->klien->name}}</td>
                        <td>{{ $list->no_transaksi}}</td>
                        <td>{{ $list->subKategori->nama_sub_kategori}}</td>
                        <td>{{ $list->media->nama_media}}</td>
                        <td>{{ $list->jadwal}}</td>
                        <td>{{ $list->jam}}</td>
                        <td>{{ $list->masalah}}</td>
                        <td><?= Lib::rupiah($list->laporanKlien->total_konsul) ?></td>
                        @if($list->status_bayar == 'pending')
                            <td>Belum Bayar</td>
                        @elseif($list->status_bayar == 'settlement')
                            <td>Sudah Dibayar</td>
                        @else
                            <td>{{$list->status_bayar}}</td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>