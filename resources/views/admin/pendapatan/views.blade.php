<?php
use App\Transaksi;
use App\LaporanKlien;
use App\Lib;
?>
@extends('layouts.admin2')

@section('content')




<div class="col-md-12">
<!-- general form elements -->
<div class="box box-primary">
<div class="box-header with-border">
    <h3 class="box-title">Lihat Pendapatan</h3>
</div>


    <div class="box-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Pilih Range Tanggal</label>
            <input onchange="pilihTanggal()" type="text" class="form-control" name="daterange" value="" />
            <input type="text" hidden class="form-control" id="dateForm" name="dateForm" value="" />
            <input type="text" hidden class="form-control" id="dateTo" name="dateTo" value="" />
        </div>
        <div class="form-group" id="dataPendapatan">
            
        </div>
    </div>
          <!-- /.box -->
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Transaksi</h4>
      </div>
      <div class="modal-body" id="detail_transaksi">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
<script>
    function pilihTanggal(){
        var dateForm = $('#dateForm').val();
        var dateTo = $('#dateTo').val();
        $('#dataPendapatan').load('{{url("admin/pendapatan/detail")}}?id=<?= $id ?>&dateForm='+dateForm+'&dateTo='+dateTo, function(e) {});
    }
    function lihatPerjanjian(id){
        $('#detail_transaksi').load('{{url("admin/transaksi/detail")}}/'+id, function(e) {});
    }
</script>

@section('scripts')
@parent

@endsection