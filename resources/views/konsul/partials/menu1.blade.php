<form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i><a href="persona_quality@yahoo.com">psyqonsul@gmail.com</a>
        <i class="icofont-phone"></i> (0761) 848844
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></a>
      </div>
    </div>
  </section>

  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <a href="{{url('/')}}"><img src="{{url('konsul')}}/assets/img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{url('/')}}">Home</a></li>
          <li><a href="#">Tentang PsyQonsul</a></li>
          <li><a href="#">Info Jiwa Sehat</a></li>
          <li><a href="#">Kamus Psikolog</a></li>
          <li><a href="#">Cendera Hati</a></li>
          <li><a href="#">Tanya Kami</a></li>
          @if(!empty(Auth::user()->name))
            <li class="drop-down"><a href="#">{{ Auth::user()->name }}</a>
              <ul>
                <li><a href="{{url('/psikolog/profil')}}">Profil</a></li>
                <li><a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                      <i class="nav-icon fas fa-fw fa-sign-out-alt">

                      </i>
                      {{ trans('global.logout') }}
                  </a></li>
              </ul>
            </li>
          @endif
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header>