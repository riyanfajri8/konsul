<!-- ======= Top Bar ======= -->
<style>
#header .logo img {
  max-height: 139px;
}
</style>
<form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>


  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top" style="position: unset; padding: 11px;" >
    <div class="container d-flex align-items-center">

      <!-- <h1 class="logo mr-auto"><a href="{{url('/')}}">PSYQONSUL<span>.</span></a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="{{url('/')}}" class="logo mr-auto"><img src="{{asset('logo/logo.png')}}" alt="" style="width: 121px; max-height: -1px;"></a>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{url('/')}}" style="font-size: 25px;">Home</a></li>
          <li><a href="{{url('/layanan-konsultasi')}}" style="font-size: 25px;">Services</a></li>
          <!-- <li><a href="#about">About</a></li>
          
          <li><a href="#">Portfolio</a></li>
          <li><a href="#team">Team</a></li> -->
          @if(!empty(Auth::user()->name))
            <li class="drop-down"><a href="#" style="font-size: 25px;">{{ Auth::user()->name }}</a>
              <ul>
                <li><a href="{{url('/psikolog/profil')}}">Profil</a></li>
                <li><a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                      <i class="nav-icon fas fa-fw fa-sign-out-alt">

                      </i>
                      {{ trans('global.logout') }}
                  </a></li>
              </ul>
            </li>
          @endif

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->