<form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<style>
p {
    display: contents;
    margin-top: 0;
    margin-bottom: 1rem;
}
.icon-foto{
  background-color: #dedede;
  width: 25px;
  height: 25px;
  border-radius: 200px;
    -webkit-border-radius: 200px;
    -moz-border-radius: 200px;
}



/*--------------------------------------------------------------
# Navigation Menu
--------------------------------------------------------------*/

/* Desktop Navigation */
.nav-menu, .nav-menu * {
  margin: 0;
  padding: 0;
  list-style: none;
}

.nav-menu > ul > li {
  position: relative;
  white-space: nowrap;
  float: left;
}

.nav-menu a {
	display: block;
	position: relative;
	color: #555;
	padding: 15px 7px;
	transition: 0.3s;
	font-size: 13px;
	font-weight: 500;
	font-family: 'Montserrat', sans-serif;
	text-transform: uppercase;
}

.nav-menu a:hover, .nav-menu .active > a, .nav-menu li:hover > a {
  color: #a80d0d;
  text-decoration: none;
}

.nav-menu .drop-down ul {
  display: block;
  position: absolute;
  left: 20px;
  top: calc(100% + 30px);
  z-index: 99;
  opacity: 0;
  visibility: hidden;
  padding: 10px 0;
  background: #fff;
  box-shadow: 0px 0px 30px rgba(127, 137, 161, 0.25);
  transition: ease all 0.3s;
}

.nav-menu .drop-down:hover > ul {
  opacity: 1;
  top: 100%;
  visibility: visible;
}

.nav-menu .drop-down li {
  min-width: 180px;
  position: relative;
}

.nav-menu .drop-down ul a {
  padding: 10px 20px;
  font-size: 14px;
  font-weight: 500;
  text-transform: none;
}

.nav-menu .drop-down ul a:hover, .nav-menu .drop-down ul .active > a, .nav-menu .drop-down ul li:hover > a {
  color: #a80d0d;
}

.nav-menu .drop-down > a:after {
  content: "\ea99";
  font-family: IcoFont;
  padding-left: 5px;
}

.nav-menu .drop-down .drop-down ul {
  top: 0;
  left: calc(100% - 30px);
}

.nav-menu .drop-down .drop-down:hover > ul {
  opacity: 1;
  top: 0;
  left: 100%;
}

.nav-menu .drop-down .drop-down > a {
  padding-right: 35px;
}

.nav-menu .drop-down .drop-down > a:after {
  content: "\eaa0";
  font-family: IcoFont;
  position: absolute;
  right: 15px;
}

@media (max-width: 1366px) {
  .nav-menu .drop-down .drop-down ul {
    left: -90%;
  }
  .nav-menu .drop-down .drop-down:hover > ul {
    left: -100%;
  }
  .nav-menu .drop-down .drop-down > a:after {
    content: "\ea9d";
  }
}

/* Mobile Navigation */
.mobile-nav-toggle {
  position: fixed;
  right: 15px;
  top: 22px;
  z-index: 9998;
  border: 0;
  background: none;
  font-size: 20px;
  transition: all 0.4s;
  outline: none !important;
  line-height: 1;
  cursor: pointer;
  text-align: right;
}

.mobile-nav-toggle i {
  color: #555;
}

.mobile-nav {
  position: fixed;
  top: 55px;
  right: 15px;
  bottom: 15px;
  left: 15px;
  z-index: 9999;
  overflow-y: auto;
  background: #fff;
  transition: ease-in-out 0.2s;
  opacity: 0;
  visibility: hidden;
  border-radius: 10px;
  padding: 10px 0;
}

.mobile-nav * {
  margin: 0;
  padding: 0;
  list-style: none;
}

.mobile-nav a {
  display: block;
  position: relative;
  color: #000;
  padding: 10px 20px;
  font-weight: 500;
  outline: none;
}

.mobile-nav a:hover, .mobile-nav .active > a, .mobile-nav li:hover > a {
  color: #a80d0d;
  text-decoration: none;
}

.mobile-nav .drop-down > a:after {
  content: "\ea99";
  font-family: IcoFont;
  padding-left: 10px;
  position: absolute;
  right: 15px;
}

.mobile-nav .active.drop-down > a:after {
  content: "\eaa1";
}

.mobile-nav .drop-down > a {
  padding-right: 35px;
}

.mobile-nav .drop-down ul {
  display: none;
  overflow: hidden;
}

.mobile-nav .drop-down li {
  padding-left: 20px;
}

.mobile-nav-overly {
  width: 100%;
  height: 100%;
  z-index: 9997;
  top: 0;
  left: 0;
  position: fixed;
  background: rgba(59, 59, 59, 0.6);
  overflow: hidden;
  display: none;
  transition: ease-in-out 0.2s;
}

.mobile-nav-active {
  overflow: hidden;
}

.mobile-nav-active .mobile-nav {
  opacity: 1;
  visibility: visible;
}

.mobile-nav-active .mobile-nav-toggle i {
  color: #fff;
}
</style>
<!-- ======= Top Bar ======= -->
<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex align-items-center justify-content-between">
      <div class="d-flex align-items-center"  style="font-size:16px;">
        <i class="fas fa-envelope-open-text fa-sm"></i><a href="persona_quality@yahoo.com">psyqonsul@gmail.com</a>
        <i class="fas fa-phone-alt fa-sm"></i><a>(0761) 848844</a>
        <i class="fab fa-whatsapp fa-lg"></i><a href="https://api.whatsapp.com/send?phone=6281268968909&text=Hallo,%20PsyQonsul...">+6281268968909</a>
      </div>
      <div class="social-links" style="font-size:15px;">
        <!-- <a href="#" class="twitter"><i class="icofont-twitter"></i></a> -->
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <!-- <a href="#" class="skype"><i class="icofont-skype"></i></a> -->
        <!-- <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> -->
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
      <a href="{{url('/')}}" class="logo mr-auto"><img src="{{asset('Medicio')}}/assets/img/logo.png" alt=""></a>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <h1 class="logo mr-auto"><a href="index.html">Medicio</a></h1> -->
      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{url('/')}}">Home</a></li>
          <li><a href="#contact">PsyQonsul</a></li>
          <li><a href="#contact">Info Jiwa Sehat</a></li>
          <li><a href="#contact">Energizer Game</a></li>
          <li><a href="#">Cendera Hati</a></li>
            <!-- <li><a href="#contact">Cari Psikolog</a></li> -->


            <!-- <li class="drop-down"><a href="">Klien</a>
              <ul>
                <li><a href="#">Info Jiwa Sehat</a></li> -->
                <!-- <li><a href="#">Cara Konsultasi</a></li> -->
                <!-- <li><a href="#">Energizer Game</a></li>
                <li><a href="#">Cendera Hati</a></li>
              </ul>
            </li> -->
            <li class="drop-down"><a href="">Psikolog</a>
              <ul>
                <li><a href="#">Kamus Psikologi</a></li>
                <li><a href="#">List Psikolog</a></li>
                <li><a href="{{url('register-psikolog')}}i">Daftar sebagai Psikolog</a></li>

              </ul>
            </li>
            <!-- <li><a href="#contact">Tanya Kami</a></li> -->
          @if(!empty(Auth::user()->name))
          <?php
            if(!empty(Auth::user()->biodata->img)){
              $image = Auth::user()->biodata->img;
            }else{
              if(!empty(Auth::user()->psikologi->foto)){
                $image = Auth::user()->psikologi->foto;
              }else{
                $image = '';
              }
              
            }
          ?>
            <li class="drop-down"><a href="#" class=""><img src="{{asset($image)}}" class="user-image icon-foto"> <p>{{ Auth::user()->name }}</p></a>
            
              <ul>
                <li><a href="{{url('/psikolog/profil')}}">Profil</a></li>
                <li><a href="{{url('/klien/pemesanan/all')}}">Pemesanan</a></li>
                <li><a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                
                      <i class="nav-icon fas fa-fw fa-sign-out-alt">

                      </i>
                      {{ trans('global.logout') }}
                  </a></li>
              </ul>
            </li>
          @endif
        </ul>
      </nav><!-- .nav-menu -->
      @if(empty(Auth::user()->id))
      <div class="register">
      <a onclick="loginBuka()" class="appointment-btn scrollto " data-toggle="modal" href="#myModal"><span class="d-none d-md-inline"></span> Login</a>
      </div>
      <div class="login" style="display:none">
      <a onclick="bukaRegister()"  id="registerHeader" class="appointment-btn scrollto " data-toggle="modal" href="#myModal"><span class="d-none d-md-inline"></span> Register</a>
      </div>
      @endif
    </div>
  </header>

  