<select required class="form-control sub_pendidikan" id="pilih_pendidikan_sub" name="pilih_pendidikan_sub">
    <option value="">Pilih Profesi</option>
    @foreach($sub_pendidikan as $sub_pendidikan)
        @if(!empty($id_sub_pendidikan))
            @if($sub_pendidikan->id_sub_pendidikan == $id_sub_pendidikan)
                <option selected=""  value="{{$sub_pendidikan->id_sub_pendidikan}}">{{$sub_pendidikan->nama}}</option>
            @else
                <option value="{{$sub_pendidikan->id_sub_pendidikan}}">{{$sub_pendidikan->nama}}</option>
            @endif
        @else
            <option  value="{{$sub_pendidikan->id_sub_pendidikan}}">{{$sub_pendidikan->nama}}</option>
        @endif
        
    @endforeach
</select>