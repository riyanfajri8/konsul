@extends('konsul.layouts.app2')

@section('content')

<section class="daftar-psikolog bg-basic pb-0">
  <div class="container">
    <div class="row justify-content-center pb-2">
      <div class="col-md-12 heading-section text-center">
        <span class="subheading">Pilih Psikolog kamu</span>
        <h2 class="mb-1">Langkah 2 : Cari Psikolog</h2>
      </div>
    </div>
  </div>


  <!-- step -->
  <div class="container">
    <section id="step">
      <div class="card2 card-timeline px-2 border-none">
        <div class="col-md-8 offset-md-2 justify-content-center">
          <ul class="bs4-order-tracking">
            <li class="step active">
              <div style="width: 40px; height: 40px;"><i style="
    padding-top: 9px;" class="fas fa-clipboard-list"></i></div> Cek Layanan Konsultasi
            </li>
            <li class="step active">
              <div style="width: 40px; height: 40px;"><i style="
    padding-top: 9px;" class="fas fa-user-md"></i></div> Cari Psikolog
            </li>
            <li class="step ">
              <div style="width: 40px; height: 40px;"><i style="
    padding-top: 9px;" class="far fa-calendar-check"></i></div> Buat Janji Konsultasi
            </li>
          </ul>
        </div>
      </div>
    </section>
  </div>

  <!-- form layanan konsul yang sudah diisi -->
  <section id="layanan">
    <div class="container">
      <form class="needs-validation" novalidate="">
        <div class="form-row">
          <!-- <div class="col-md-12"> -->
            <div class="col-md-3">
              <div class="label2">
                <label for="inputState">Kategori yang telah anda pilih</label>
              </div>
              <select onchange="kategori()" id="inputState" class="form-control">
                <option selected="">Cari Kategori</option>
                @foreach($kategori as $kategori1)
                @if(!empty($kategoripilih->id_kategori_konsul))
                @if($kategoripilih->id_kategori_konsul == $kategori1->id_kategori_konsul)
                <option selected value="{{$kategori1->id_kategori_konsul}}">{{$kategori1->nama_konsul}}</option>
                @else
                <option value="{{$kategori1->id_kategori_konsul}}">{{$kategori1->nama_konsul}}</option>
                @endif
                @endif
                @endforeach
              </select>
            </div>
            <div class="col-md-4" id="sub_kategori">
              <div class="label2">
                <label for="inputState">Sub Kategori yang telah anda pilih</label>
              </div>
              <select onchange="kategoriPilih()" class="form-control">
                <option selected="">Pilih Salah Satu</option>
              </select>
            </div>
            <div class="col-md-5">
              <div class="label2">
                <label class="label2" for="exampleFormControlTextarea1">Gejala gejala yang telah anda tulis</label>
              </div>
              <textarea id="masalah" class="form-control" id="exampleFormControlTextarea1"
                rows="3">{{$gejala}}</textarea>
            </div>
          <!-- </div> -->
          </div>
          <!-- <div class="form-group col-md-3">
            <div class="form-group">
              <div class="label2">
                <label class="label2" for="exampleFormControlTextarea1">Gejala gejala yang telah anda tulis</label>
              </div>
              <textarea id="masalah" class="form-control" id="exampleFormControlTextarea1"
                rows="3">{{$gejala}}</textarea>
            </div>
          </div> -->
        </div>
      </form>
    </div>
  </section>

  <!-- Langkah 2 :  Cari psikolog -->
  <section class="ftco-section bg-basic">
    <div class="container mb-4">
      <div class="row justify-content-center">
        <div class="col-md-6" style="border: #c6c4c4 1px solid;border-radius: 50px;padding: 0;">
          <div class="card-body row no-gutters align-items-center" style="padding: 0.15rem;">
            <!--end of col-->
            <div class="col">
              <input onkeyup="cariPsikolog()" id="nama" class="form-control form-control-lg form-control-borderless"
                type="search" placeholder="Ketikan nama psikolog yang akan anda cari">
            </div>
            <div class="col-auto">
                        <button class="btn btn-lg btn-success" type="submit">Cari Psikolog</button>
                    </div>
          </div>
        </div>
        <!--end of col-->
      </div>
    </div>
    
    <div class="container-xl">
      <div class="row gy-4">
        <div class="col-md-12">
          <div class="carousel-inner">
            <div class="item carousel-item active">
              <div class="row" id="pasikologi-cari">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

</section>
<!-- Modal -->
<div id="modal-profil" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Profil</h4>
      </div>
      <div class="modal-body">
        <div id="profilSingkat"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection

<script>
  function kategori() {
    var id_kategori = $('#inputState').val();
    var id = 0;
    $('#sub_kategori').load('{{url("/cari-psikologi")}}/' + id + "?id_kategori=" + id_kategori, function (e) {});
  }

  function cariPsikolog() {
    var nama = $('#nama').val();
    var id = $('.caripsiko').val();
    $('#pasikologi-cari').load('{{url("/cari-psikologi")}}/' + id + '/?nama=' + nama, function (e) {});
  }

  function kategoriPilih() {
    //var nama_kategori = $('#kategori_cari'+id).val();
    //$('#namaKategori').val(nama);
    var id = $('.caripsiko').val();

    $('#pasikologi-cari').load('{{url("/cari-psikologi")}}/' + id, function (e) {});
  }


  function pilihTanggal() {
    var tanggal = $('#tanggal').val();
    $.ajax({
      url: '{{url("pilih-tanggal-order")}}/' + tanggal,
      type: "GET",
      dataType: "html",
      success: function (data) {
        console.log(data);
        $('#psikologi-tampil').html(data);
      }
    });

  }

  function buatMasalah() {
    var masalah = $('#masalah').val();
    Cookies.set('masalah', masalah);
  }
</script>

@section('scripts')
@parent

@endsection