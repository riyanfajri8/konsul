<?php

use App\JadwalKonsul;

?>
<div class="container">
            <div class="main-body">
              <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                  <div class="card-detail-psikolog">
                    <div class="member">
                      <div class="member-img rounded-circle">
                        <img src="{{asset($psikologi->foto)}}" class="img-fluid" alt="">
                      </div>
                      <div class="member-info">
                        <h5>{{$psikologi->nama}}</h5>
                        <span>Psikolog</span>
                        <ul class="mt-3 text-left" style="line-height: normal; display:inherit;">
                          <li><i class="icofont-badge"></i> SIPP</li>
                          <p>{{$psikologi->sip}}</p>
                          <li><i class="icofont-bag"></i> Pengalaman</li>
                          <p>{{$psikologi->pengalaman}} Tahun</p>
                          <li><i class="icofont-graduate"></i> Pendidikan</li>
                          <p>{{$psikologi->universitas}}</p>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 mb-3">
                  <div class="card-detail-psikolog">
                    <div class="member">
                      <div class="member-info mb-4">
                        <div class="info">
                          <h4>Tentang Psikolog</h4>
                          <hr>
                        </div>
                        <p style="text-align: left;
                                padding-left: 0;
                                font-weight: 400;
                                margin-top: -10px;">
                          {{$psikologi->tentang_psikologi}}
                        </p>
                      </div>
                      <div class="member-info mb-4">
                        <div class="info">
                          <h4>Bidang Keahlian</h4>
                          <hr style="color: red">
                        </div>
                        <ul class="list-group list-group-flush">
                        @foreach($kategori_k as $kategori_ks)
                        <h5>{{$kategori_ks->nama_konsul}}</h5>
                        <?php
                            $kategori_psi =  DB::table('psikologi_kategori_konsul')
                                  ->select('psikologi.foto','psikologi.nama','psikologi.id_psikologi','nama_sub_kategori','kategori_konsul_sub_kategori.id_kategori_konsul')
                                  ->LeftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','psikologi_kategori_konsul.id_kategori_konsul')
                                  ->LeftJoin('psikologi','psikologi.id_psikologi','=','psikologi_kategori_konsul.id_psikologi')
                                  ->LeftJoin('kategori_konsul_sub_kategori','kategori_konsul_sub_kategori.id_sub_kategori','=','sub_kategori.id_sub_kategori')
                                  ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategori_ks->id_kategori_konsul)
                                  ->where('psikologi_kategori_konsul.id_psikologi',$id_psikologi)
                                  ->where('psikologi.status','aktif')
                                  ->groupBy('psikologi.foto','psikologi.nama','psikologi.id_psikologi','nama_sub_kategori','kategori_konsul_sub_kategori.id_kategori_konsul')
                                  ->get();
                        ?>
                            
                          @foreach($kategori_psi as $kategori_psis)
                            @if($kategori_ks->id_kategori_konsul == $kategori_psis->id_kategori_konsul)
                            <li class="list-group-item">{{$kategori_psis->nama_sub_kategori}}</li>                          
                            @endif
                          @endforeach
                        @endforeach
                        

                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <div class="card-detail-psikolog">
              <div class="member">
                <div class="row">
                  <div class="col-md-12">
                    <div class="member-info mb-4">
                      <div class="info">
                        <h4>Jadwal Konsul Available</h4>
                      </div>
                      <table class="table table-striped table-sm">
                      <?php  foreach($hari as $hari1){ ?>
                        <?php
                            $jadwal_konsul = JadwalKonsul::where('hari',$hari1->nama_hari)->where('id_psikologi',$id_psikologi)->get();
                        ?>
                        <tr>
                        <th class="row-header"><?= $hari1->nama_hari ?></th>
                        <?php foreach($jadwal_konsul as $jadwal1){ ?>
                            <td>{{$jadwal1->jadwal}} - {{$jadwal1->jadwal_akhir}}</td>
                        <?php } ?>
                        </tr>
                      <?php } ?>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <a href="{{url('kategori-konsul/order/perjanjian',[$psikologi->id_psikologi,0])}}"
                        class="btn btn-primary" style="float: right;"> Buat Janji </a>
          </div>