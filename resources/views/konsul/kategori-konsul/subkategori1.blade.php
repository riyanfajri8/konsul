<select onchange="kategoriPilih()" name="sub_kategori" id="inputState" class="sub_kategori form-control">
    <option value="">Pilih Salah Satu</option>
    @foreach($name_sub_kategori as $name_sub_kategori1)
        @if($name_sub_kategori1->id_sub_kategori == $id_sub)
            <option selected  value="{{$name_sub_kategori1->id_sub_kategori}}">{{$name_sub_kategori1->nama_sub_kategori}}</option>
        @else
            <option value="{{$name_sub_kategori1->id_sub_kategori}}">{{$name_sub_kategori1->nama_sub_kategori}}</option>
        @endif
    @endforeach
</select>