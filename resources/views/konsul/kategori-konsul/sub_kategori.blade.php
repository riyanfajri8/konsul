
<?php
    $sub_kategori = DB::table('kategori_konsul_sub_kategori')
    ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
    ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategori_head->id_kategori_konsul)
    ->get();
?>
<div class="modal-content">
    <div class="modal-header">
    <h3><?= $kategori_head->nama_konsul ?></h3>
    </div>
    <div class="modal-body">
        <section id="pricing" class="pricing">
            <div class="container aos-init aos-animate">

                <div class="row">
                    <div class="col-lg-12 aos-init aos-animate">
                        <div class="box" >
                            
                            <ul style="height: 200px;">
                            @foreach($sub_kategori as $sub_kategori)
                                <li><a href="{{url('/kategori-konsul/order',[$sub_kategori->id_sub_kategori])}}">
                                                                    {{$sub_kategori->nama_sub_kategori}}</a></li>
                            @endforeach
                                <li><a href="{{url('/kategori-konsul/order',28)}}">
                                                                    Lain-Lain</a></li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>
