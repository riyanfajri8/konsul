@extends('konsul.layouts.app3')

@section('content')
<style>
.container { max-width: 1400px !important; }
</style>

<style>
control {
    line-height: 34px;
}

body {
    background: #DCDCDC;
    margin-top: 20px;
}

.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.social-links li a {
    border-radius: 50%;
    color: rgba(121, 121, 121, .8);
    display: inline-block;
    height: 30px;
    line-height: 27px;
    border: 2px solid rgba(121, 121, 121, .5);
    text-align: center;
    width: 30px
}

.social-links li a:hover {
    color: #797979;
    border: 2px solid #797979
}

.thumb-lg {
    height: 88px;
    width: 88px;
}

.img-thumbnail {
    padding: .25rem;
    background-color: #fff;
    border: 1px solid #dee2e6;
    border-radius: .25rem;
    max-width: 100%;
    height: auto;
}

.text-pink {
    color: #ff679b !important;
}

.btn-rounded {
    border-radius: 2em;
}

.text-muted {
    color: #98a6ad !important;
}

h4 {
    line-height: 22px;
    font-size: 18px;
}
.masalah{
  resize:vertical;
  width: 459px;
  height:100px;
}
</style>

<section id="services" class="services">
    <div class="container"  data-aos="">

        <div class="section-title">
            <h3>Pilih <span>Psikologi </span></h3>
            <p></p>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3>Kategori</h3>
                    </div>
                    <div class="panel-body">

                        <?php

                        ?>
                        @foreach($kategori as $kategori1)
                        <?php
                            $sub_kategori = DB::table('kategori_konsul_sub_kategori')
                                ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                                ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategori1->id_kategori_konsul)
                                ->get();
                        ?>

                        <div class="col-md-12">
                        <div class="box box-success box-solid">
                            <div class="box-header with-border">
                            <h3 class="box-title"><?= $kategori1->nama_konsul ?></h3>

                            <div class="box-tools pull-right">
                               
                            </div>
                            <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                @foreach($sub_kategori as $sub_kategori)
                                        <h5>
                                            
                                            <a style="color: #060606;" href="#" id="kategori_cari<?= $sub_kategori->id_sub_kategori ?>" onclick="kategoriPilih('<?= $sub_kategori->id_sub_kategori ?>','<?= $sub_kategori->nama_sub_kategori ?>')"
                                                >{{$sub_kategori->nama_sub_kategori}}
                                            </a>
                                            
                                        </h5>
                                @endforeach
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        </div>

                        
                        @endforeach
                        
                        <!-- @foreach($kategori as $kategori)
                        @if($kategori->nama_konsul == $name_konsul)
                        <div class="list-group ">
                            <a href="{{url('kategori-konsul/order',['name_konsul' => $kategori->nama_konsul])}}"
                                class="list-group-item active">{{$kategori->nama_konsul}}</a>
                        </div>
                        @else
                        <div class="list-group">
                            <a href="{{url('kategori-konsul/order',['name_konsul' => $kategori->nama_konsul])}}"
                                class="list-group-item">{{$kategori->nama_konsul}}</a>
                        </div>
                        @endif
                        @endforeach -->
                    </div>
                </div>
            </div>

            <div class="col-xl-9 " >
                <div class="m-portlet__body" >
                    <div class="tab-content">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3><label id="namaKategori">{{$name_sub_kategori->nama_sub_kategori}}</label></h3>
                            </div>

                            <div class="col-xl-12 mt-12">
                                
                                <div class="panel-body">
                                    <div class="row" style="">
                                        <div class="col-xl-6" style=''>
                                            <label><h3>Silahkan tulisakan gejala-gejala yang anda rasakan dibawah ini <h3></label>
                                                <textarea onkeyup="buatMasalah()" id="masalah" name="masalah" class="form-control masalah" rows="4">{{$gejala}}</textarea>
                                        </div>
                                    </div>

                                    <h3><label id="namaKategori">Pilih Pisikolog bisa dengan cara menuliskan di kotak cari apabila klien sudah mengenal psikolog atauapapun cara mencari psikolog dengan cepat :</label></h3>
                                    <table >
                                        <tr>
                                            <td><input id="nama"  onkeyup="cariPsikolog()" placeholder="Cari Psikolog" type="text" class="form-control" name="cari"></td>
                                            <!-- <td><input type="submit" onclick="cariPsikolog()" class="form-control" value="Cari"></td> -->
                                        </tr>
                                    </table>
                                    <!-- <div class="row" style="">
                                        <div class="col-xl-6" style=''>
                                            <input id="nama" placeholder="Cari Psikologi" type="text" class="form-control" name="cari">
                                        </div>
                                        <div class="col-xl-1" style=';'>
                                            <input type="submit" onclick="cariPsikolog()" class="form-control" value="Cari">
                                        </div>
                                    <div> -->
                                    
                                </div>
                            </div>

                            <div class="col-xl-12 mt-12">
                                <div class="panel-body">
                                        
                                    <div class="row" id="pasikologi-cari">
                                    </div>
                                </div>
                            <div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- Modal -->
<div id="modal-profil" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profill</h4>
            </div>
            <div class="modal-body">
                <div id="profilSingkat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection

<script>

function cariPsikolog(){
    var nama = $('#nama').val();
    $('#pasikologi-cari').load('{{url("/cari-psikologi",[$name_konsul])}}/?nama='+nama, function(e) {});
}

function kategoriPilih(id,nama){
    //var nama_kategori = $('#kategori_cari'+id).val();
    //$('#namaKategori').val(nama);
    $('#namaKategori').text(nama);
    $('#pasikologi-cari').load('{{url("/cari-psikologi")}}/'+id, function(e) {});
}


function pilihTanggal() {
    var tanggal = $('#tanggal').val();
    $.ajax({
        url: '{{url("pilih-tanggal-order")}}/' + tanggal,
        type: "GET",
        dataType: "html",
        success: function(data) {
            console.log(data);
            $('#psikologi-tampil').html(data);
        }
    });

}
function buatMasalah(){
    var masalah = $('#masalah').val();
    Cookies.set('masalah', masalah);
}
</script>

@section('scripts')
@parent

@endsection