@foreach($psikologi as $psikologi1)
    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
        <div class="member">
            <div class="member-img rounded-circle">
            <img src="{{asset($psikologi1->foto)}}" class="img-fluid" alt="">
            </div>
            <div class="member-info">
            <h4>{{$psikologi1->nama}}</h4>
            <span>Psikolog</span>
                <ul class="mt-3 text-left" style="line-height: normal;">
                <li><i class="icofont-badge"></i> SIPP</li>
                    <p>{{$psikologi1->sip}}</p>
                <li><i class="icofont-bag"></i> Pengalaman</li>
                    <p>5 tahun</p>
                <li><i class="icofont-graduate"></i> Pendidikan</li>
                    <p>{{$psikologi1->universitas}}</p>
                </ul>
            </div>

            <label data-toggle="modal" data-target="#modal-profil" onclick="bukaProfil(<?= $psikologi1->id_psikologi ?>)" class="btn btn-success btn-block mt-4 mb-2"><i class="icofont-eye-alt"></i> Profil Lengkap</label>
            <button onclick="buatJanji('<?= $psikologi1->id_psikologi ?>')" class="btn btn-primarydetail btn-block"><i class="far fa-handshake"></i> Langkah 3 : Buat Janji</button>

        </div>
    </div>

    
@endforeach
@if(count($psikologi) == 0)
    <div class="col-lg-4">
        <div class="text-center mt-3" style="border: 1px solid #2b2eff;">
            <div class="member-card pt-2 pb-2">
                Psikologi yang anda cari tidak ada
            </div>
        </div>
    </div>
@endif
<script>
    function bukaProfil(id){
        $('#profilSingkat').load('{{url("/profil-psikologi")}}/'+id, function(e) {});
    }
    function buatJanji(id){
        var masalah = $('#masalah').val();
        var sub = $('.caripsiko').val();
        url = '{{url("/kategori-konsul/order/perjanjian")}}/'+id+'/'+sub+'?masalah='+masalah;       
        location.replace(url);
    }
</script>