@extends('konsul.layouts.app')

@section('content')
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important;
} */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}

label{
    color: #555;
}

.form-group m-form__group row {
    margin-bottom: 0;
    padding-bottom: 0;
}

.m-form .m-form__group {
	margin-bottom: 0;
	padding-top: 15px;
	padding-bottom: 0;
}

</style>
<div class="container mt-4">
    <div id="myCarousel" class="carousel slide">
        <div class="row" style="margin-top:150px;">

            @include('konsul.psikologi.menu')
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                role="tablist">
                            </ul>
                        </div>
                        <div class="m-portlet__head-tools">

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a class="m-portlet__nav-link btn btn-sm btn-brand m-btn m-btn--pill m-btn--air"
                                            href="/irhis_telegram/web/index.php?r=hrd%2Fuser-pegawai%2Fview-pegawai&amp;id=110171"><i
                                                class="fa fa-arrow-left"></i> Kembali</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <!-- start head tools portlet -->
                        <!-- end head tools portlet -->
                    </div>
                    <div class="tab-content">
                        <div class="container mt-4" id="laporancuti">

                            <div class="user-pegawai-update-pegawai">

                                <!--begin::Portlet-->
                                <div class="">
                                    <!-- <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <span class="m-portlet__head-icon ">
                                                    <i class="la la-user"></i>
                                                </span>
                                                <h3 class="m-portlet__head-text">
                                                    FORM IDENTITAS PSIKOLOGI
                                                </h3>
                                            </div>
                                        </div>
                                    </div> -->

                                    <!--begin::Form-->
                                    <!--<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">-->
                                    @if($errors->any())
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    @if(session('success'))
                                        <div class="alert alert-success">
                                            {!!  session('success')  !!}
                                        </div>
                                    @endif

                                    <div class="login-wrap2 p-4 mt-3">
                                        <form id="form-pendaftaran" class="m-form m-form--fit m-form--label-align-left  "
                                            action=""
                                            files="true"
                                            enctype="multipart/form-data"
                                            method="post">
                                            @csrf

                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">Nama Lengkap dan Gelar</label>
                                                                    <input type="text" id="personal-1-nama_lengkap" class="form-control" name="nama" value="{{ Auth::user()->name }}" placeholder="Isi Nama Lengkap">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">Email</label>
                                                                    <input readonly type="text" id="personal-1-email" class="form-control" name="email" value="{{ Auth::user()->email }}" placeholder="Isi Nama Lengkap">
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-lg-6">
                                                                <label>Nomor Pegawai:</label>

                                                                <div class="form-group field-pegawai-no_pegawai required">

                                                                    <input type="text" id="pegawai-no_pegawai"
                                                                        class="form-control" name="Pegawai[no_pegawai]"
                                                                        value="20.92.1.10636" disabled=""
                                                                        aria-required="true">

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div> -->
                                                        </div>

                                                        <div class="form-group m-form__group row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">No.Handphone / WA</label>
                                                                    <input type="text" id="kontakpersonal-1-kontak"
                                                                        class="form-control"
                                                                        name="no_hp" value="{{$psikolog->no_hp}}"
                                                                        placeholder="Isi No. HP">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div
                                                                    class="form-group">
                                                                    <label>Jenis Kelamin</label>
                                                                    <select id="personal-1-jenis_kelamin"
                                                                        class="form-control"
                                                                        name="jenis_kelamin">
                                                                        <option value="">---</option>
                                                                        <option value="Laki-laki" <?php if ($psikolog->jenis_kelamin == 'Laki-laki'){ echo ' selected=""'; }?>>Laki-laki
                                                                        </option>
                                                                        <option value="Perempuan" <?php if ($psikolog->jenis_kelamin == 'Perempuan'){ echo ' selected=""'; }?>>Perempuan</option>
                                                                    </select>

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-lg-6">
                                                                <label>Jenis Identitas:</label>

                                                                <div
                                                                    class="form-group field-identitaspersonal-id_jenis_identitas required">

                                                                    <select id="identitaspersonal-id_jenis_identitas"
                                                                        class="form-control"
                                                                        name="jenis_identitas"
                                                                        aria-required="true">
                                                                        <option value="" <?php if ($psikolog->jenis_identitas == ''){ echo ' selected=""'; }?>>Pilih</option>
                                                                        <option value="KTP"  <?php if ($psikolog->jenis_identitas == 'KTP'){ echo ' selected=""'; }?> >KTP</option>
                                                                        <option value="SIM" <?php if ($psikolog->jenis_identitas == 'SIM'){ echo ' selected=""'; }?>>SIM</option>
                                                                        <option value="Passport" <?php if ($psikolog->jenis_identitas == 'Passport'){ echo ' selected=""'; }?>>Passport</option>
                                                                    </select>

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="col-lg-6">
                                                                <label class="">No. Identitas:</label>
                                                                <div
                                                                    class="form-group field-identitaspersonal-no_identitas required">

                                                                    <input type="text" id="identitaspersonal-no_identitas"
                                                                        class="form-control "
                                                                        name="no_identitas" value="{{$psikolog->no_identitas}}"
                                                                        placeholder="no_identitas"
                                                                        aria-required="true">

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-md-6">
                                                                <div class="form-group field-personal-1-tempat_lahir required">
                                                                    <label>Tempat Lahir</label>
                                                                    <input type="text" id="personal-1-tempat_lahir"
                                                                        class="form-control "
                                                                        name="tempat_lahir" value="{{$psikolog->tempat_lahir}}"
                                                                        placeholder="Isi Tempat Lahir">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Tanggal Lahir</label>
                                                                        <input name="tanggal_lahir" id="datepicker" type="date" class="form-control " value="{{$psikolog->tanggal_lahir}}">
                                                                        <div class="input-group-addon">
                                                                            <span class="glyphicon glyphicon-th"></span>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-md-6">
                                                                <div class="form-group field-personal-1-status_perkawinan required">
                                                                    <label class="">Status Perkawinan</label>
                                                                    <select id="personal-1-status_perkawinan"
                                                                        class="form-control"
                                                                        name="status_perkawinan">
                                                                        <option value="" >---</option>
                                                                        <option value="Belum Menikah" <?php if ($psikolog->status_perkawinan == 'Belum
                                                                            Menikah'){ echo ' selected=""'; }?>>Belum
                                                                            Menikah</option>
                                                                        <option value="Menikah" <?php if ($psikolog->status_perkawinan == 'Menikah'){ echo ' selected=""'; }?>>Menikah</option>
                                                                    </select>

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group field-personal-1-id_pendidikan required">
                                                                    <label class="">Pekerjaan Sekarang</label>
                                                                    <select id="personal-1-id_pendidikan"
                                                                        class="form-control"
                                                                        name="pekerjaan">
                                                                        <option <?php if ($psikolog->pekerjaan == ''){ echo ' selected=""'; }?> value="">---</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Akademik/Pengajar'){ echo ' selected=""'; }?> value="Akademik/Pengajar">Akademik/Pengajar</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Ibu Rumah Tangga'){ echo ' selected=""'; }?> value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Karyawan BUMN'){ echo ' selected=""'; }?> value="Karyawan BUMN">Karyawan BUMN</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Karyawan Swasta'){ echo ' selected=""'; }?> value="Karyawan Swasta" data-select2-id="6"> 

                                                                            Karyawan Swasta</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Pegawai Negeri Sipil (PNS)'){ echo ' selected=""'; }?> value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)
                                                                        </option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Pelajar / Mahasiswa'){ echo ' selected=""'; }?> value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Pemerintahan / Militer'){ echo ' selected=""'; }?> value="Pemerintahan / Militer">Pemerintahan / Militer</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Pensiunan'){ echo ' selected=""'; }?> value="Pensiunan">Pensiunan</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Petani'){ echo ' selected=""'; }?>  value="Petani">Petani</option>
                                                                        <option <?php if ($psikolog->pekerjaan == 'Profesional ( Dokter, Pengacara,
                                                                            Dll )'){ echo ' selected=""'; }?> value="Profesional ( Dokter, Pengacara,
                                                                            Dll )">Profesional ( Dokter, Pengacara,
                                                                            Dll )</option>


                                                                        <option <?php if ($psikolog->pekerjaan == 'Wiraswasta'){ echo ' selected=""'; }?> value="Wiraswasta">Wiraswasta</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Tidak Bekerja'){ echo ' selected=""'; }?> value="Tidak Bekerja">Tidak Bekerja</option>

                                                                        <option <?php if ($psikolog->pekerjaan == 'Lain-lain'){ echo ' selected=""'; }?> value="Lain-lain">Lain-lain</option>


                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-md-6">
                                                                <label>Agama</label>
                                                                <div class="form-group field-personal-1-id_agama required">

                                                                    <select id="personal-1-id_agama" class="form-control"
                                                                        name="agama">
                                                                        <option  value="">Pilih</option>
                                                                        <option <?php if ($psikolog->agama == 'Islam'){ echo ' selected=""'; }?> value="Islam">Islam</option>
                                                                        <option <?php if ($psikolog->agama == 'Budha'){ echo ' selected=""'; }?> value="Budha">Budha</option>
                                                                        <option <?php if ($psikolog->agama == 'Hindu'){ echo ' selected=""'; }?> value="Hindu">Hindu</option>
                                                                        <option <?php if ($psikolog->agama == 'Kristen'){ echo ' selected=""'; }?> value="Kristen">Kristen</option>
                                                                        <option <?php if ($psikolog->agama == 'Katholik'){ echo ' selected=""'; }?> value="Katholik">Katholik</option>
                                                                        <option <?php if ($psikolog->agama == 'Konghucu'){ echo ' selected=""'; }?> value="Konghucu">Konghucu</option>
                                                                        <option <?php if ($psikolog->agama == 'Kepercayaan'){ echo ' selected=""'; }?>  value="Kepercayaan">Kepercayaan</option>
                                                                        <option <?php if ($psikolog->agama == 'Lain-lain'){ echo ' selected=""'; }?> value="Lain-lain">Lain-lain</option>
                                                                    </select>

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="">SIPP</label>
                                                                    <input type="text" id="kontakpersonal-1-kontak"
                                                                        class="form-control"
                                                                        name="sip" value="{{$psikolog->sip}}"
                                                                        placeholder="">
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-lg-6">
                                                                <label class="">Email:</label>
                                                                <div class="form-group field-kontakpersonal-4-kontak">
                                                                <input type="email" id="inputEmail" class="form-control" placeholder="Email"  value="{{ Auth::user()->email }}" required name="email">
                                                                

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div> -->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Pengalaman Psikolog selama (tahun)</label>
                                                                    <input type="number" id=""
                                                                        class="form-control" name="pengalaman" value="{{$psikolog->pengalaman}}" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                <?php 
                                                                
                                                                    $pendidikan = DB::table('pendidikan')->get();
                                                                ?>
                                                                    <label class="label" for="sipp">Pendidikan</label>
                                                                    <select onchange="pendidikanPilih()" id="pilih_pendidikan" required class="form-control" name="pilih_pendidikan">
                                                                    <option value="">---</option>
                                                                    @foreach($pendidikan as $pendidikan1)
                                                                        @if(!empty($psikolog->pendidikan->nama))
                                                                            @if($pendidikan1->nama == $psikolog->pendidikan->nama)
                                                                                <?php $pilih = 'selected'; ?>
                                                                                <option selected=""  value="{{$pendidikan1->id_pendidikan}}">{{$pendidikan1->nama}}</option>
                                                                            @else
                                                                                <option  value="{{$pendidikan1->id_pendidikan}}">{{$pendidikan1->nama}}</option>
                                                                            @endif
                                                                        @else
                                                                        <option  value="{{$pendidikan1->id_pendidikan}}">{{$pendidikan1->nama}}</option>
                                                                        @endif
                                                                        
                                                                    @endforeach
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group" id="pendidikan_sub">
                                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-md-6">
                                                                <div class="form-group field-personal-1-alamat_sementara required">
                                                                    <label>Alumni Universitas</label>
                                                                    <input type="text" id="personal-1-alamat_sementara"
                                                                        class="form-control"
                                                                        name="universitas"
                                                                        value="{{$psikolog->universitas}}"
                                                                        >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group field-personal-1-alamat_sementara required">
                                                                    <label>Alamat Lengkap</label>
                                                                    <textarea name="alamat" class="form-control" id="personal-1-alamat_sementara" rows="4">{{$psikolog->alamat}}</textarea>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 form-group">
                                                                {!! Form::label('img', 'Upload Foto Profil ( Aspek Ratio 1 : 1 atau persegi. Ukuran Maks 1 MB)', ['class' => 'control-label']) !!}
                                                                <br>
                                                            
                                                                <img id="uploadPreview" src="{{asset($psikolog->foto)}}"  style="width: 150px;" />
                                                                <input class="form-control" id="uploadImage" type="file" name="img" onchange="PreviewImage();" style="line-height: 2.5;" />
                                                                
                                                                <p class="help-block"></p>
                                                                @if($errors->has('img'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('img_name') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                            <!-- <div class="col-lg-6">
                                                                <label>Pengalaman</label>
                                                                <div
                                                                    class="form-group field-personal-1-alamat_sementara required">

                                                                    <textarea name="pengalaman" class="form-control" id="textAreaExample" rows="4">{{$psikolog->pengalaman}}</textarea>

                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div> -->
                                                            
                                                            <div class="col-md-6">
                                                                <div class="form-group field-personal-1-alamat_sementara required">
                                                                    <label>Tentang Psikolog:</label>
                                                                    <textarea name="tentang_psikologi" class="form-control" id="textAreaExample" rows="4">{{$psikolog->tentang_psikologi}}</textarea>
                                                                    <div class="help-block"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- INFORMASI MODEL DEPARTEMEN -->
                                                        <div class="m-form__heading"></div>
                                                    </div>

                                                        
                                                    </div>

                                                    
                                                </div>
                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-md text-center">
                                                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                                                Simpan Perubahan</button> 
                                                            <!-- <button type="reset" class="btn btn-secondary">Reset</button> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--end::Portlet-->
                                
                                <br>

                                <script>
                                $(document).ready(function() {
                                    var id_kab = '';
                                    var id_kec = '';
                                    $.post("index.php?r=pasien/list_kabupaten&id=" + $('#select-provinsi')
                                    .val(),
                                        function(data) {
                                            $("select#pasien-kabupaten").html(data);
                                            $('#pasien-kabupaten').val(id_kab);
                                            $.post("index.php?r=pasien/list_kecamatan&id=" + id_kab,
                                                function(data) {
                                                    $("select#pasien-kecamatan").html(data);
                                                    $('#pasien-kecamatan').val(id_kec);
                                                }
                                            );
                                        }
                                    );
                                });
                                </script>

                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    
    
    function pendidikanPilih(){
        var id_pendidikan =$('#pilih_pendidikan').val();
            $('#pendidikan_sub').load('{{url("/sub_pendidikan")}}?id_pendidikan='+id_pendidikan+"&id_sub_pendidikan={{$psikolog->id_sub_pendidikan}}", function(e) {});
    }

    function PreviewImage() {  
        var oFReader = new FileReader();  
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);  
        oFReader.onload = function (oFREvent) {  
            document.getElementById("uploadPreview").src = oFREvent.target.result;  
        };  
    };
    
    
$('#datepicker').datepicker({
    format: 'DD-MM-YYYY'
});
</script>
@endsection
@section('scripts')
@parent

@endsection