

<?php
use App\JadwalKonsul;
?>
@foreach($psikologi as $psikologi1)
    <!-- <label class="radio-inline"><input type="radio" name="jam" value="{{$psikologi1->jadwal}}">{{$psikologi1->jadwal}}</label> -->
    <?php
         $cek_transaksi = DB::table('transaksi')->where('jadwal',date("Y-m-d", strtotime($tanggal)))
         ->where('id_psikologi',$psikologi1->id_psikologi)
         ->where('id_jadwal_konsul',$id_jadwal_konsul)
         ->orderBy('id_transaksi','DESC')
         ->first();

         
        $jadwal_awal  = new \DateTime($tanggal.' '.$psikologi1->jadwal.':00'); //waktu awal
        $jadwal_akhir = new \DateTime($tanggal.' '.$psikologi1->jadwal_akhir.':00'); //waktu akhir
        $selisih_waktu = $jadwal_awal->diff($jadwal_akhir);

        if(empty($cek_transaksi)){ 
            if($selisih_waktu->h >= 2){
            ?>
            
                <?php 
                $waktu = date_create($tanggal.' '.$psikologi1->jadwal.':00');
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $psikologi1->jadwal.' - '.$waktu; ?>',1)"  id="durasi1" type="button" value="01:00" class="set_jam btn btn-danger mb-2">1 Jam</button>
                <?php 
                $waktu = date_create($tanggal.' '.$psikologi1->jadwal.':00');
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                $waktu = date_create($tanggal.' '.$waktu.':00');
                date_add($waktu, date_interval_create_from_date_string('30 minutes'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $psikologi1->jadwal.' - '.$waktu; ?>',130)"  id="durasi130" type="button" value="01:30" class=" set_jam btn btn-danger mb-2">1 Jam, 30 Menit</button>
                <?php 
                $waktu = date_create($tanggal.' '.$psikologi1->jadwal.':00');
                date_add($waktu, date_interval_create_from_date_string('2 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $psikologi1->jadwal.' - '.$waktu; ?>',2)"  id="durasi2" type="button" value="02:00" class=" set_jam btn btn-danger mb-2">2 Jam</button>
            <!-- 1jam 30 menit -->
            <?php }elseif($selisih_waktu->h == 1 && $selisih_waktu->i >= 30 ){ ?>
                <?php 
                $waktu = date_create($tanggal.' '.$psikologi1->jadwal.':00');
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $psikologi1->jadwal.' - '.$waktu; ?>',1)"  id="durasi1" type="button" value="01:00" class="set_jam btn btn-danger mb-2">1 Jam</button>
                <?php 
                $waktu = date_create($tanggal.' '.$psikologi1->jadwal.':00');
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                $waktu = date_create($tanggal.' '.$waktu.':00');
                date_add($waktu, date_interval_create_from_date_string('30 minutes'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $psikologi1->jadwal.' - '.$waktu; ?>',130)"  id="durasi130" type="button" value="01:30" class=" set_jam btn btn-danger mb-2">1 Jam, 30 Menit</button>

                <!-- 1jam  -->
            <?php }elseif($selisih_waktu->h == 1 ){ ?>
                <?php 
                $waktu = date_create($tanggal.' '.$psikologi1->jadwal.':00');
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $psikologi1->jadwal.' - '.$waktu; ?>',1)"  id="durasi1" type="button" value="01:00" class="set_jam btn btn-danger mb-2">1 Jam</button>
                
            <?php } ?>
        <?php }else{ ?>
            

    @if(!empty($cek_transaksi->jam))
        <?php $jam_tran = $cek_transaksi->jam; ?>
    @else
        <?php $jam_tran = ''; ?>
    @endif

    @if(!empty($cek_transaksi))
        <?php 

            $cek_expired = strtotime($cek_transaksi->jam); 
            $awal  = new \DateTime($cek_transaksi->created_at); //waktu awal

            $akhir = new \DateTime(date('Y-m-d H:i:s')); //waktu akhir

            $jumlah_jadwal = $awal->diff($akhir);

            //if($jumlah_jadwal->h > 1){ 
                $cek_sudah_pesan = DB::table('transaksi')->where('jadwal',$tanggal)
                            ->where('id_psikologi',$psikologi1->id_psikologi)
                            ->where('jam',$psikologi1->jadwal.':00')
                            ->where('status_bayar','settlement')
                            ->where('id_jadwal_konsul',$id_jadwal_konsul)
                            ->orderBy('id_transaksi','DESC')
                            ->first();
               if(empty($cek_sudah_pesan)){

            
                $ambil_terakhir = substr($cek_transaksi->jadwal_awal_akhir,8).":00";
            
                $jadwal_awal  = new \DateTime($tanggal.' '.$ambil_terakhir); //waktu awal
                $jadwal_akhir = new \DateTime($tanggal.' '.$psikologi1->jadwal_akhir.':00'); //waktu akhir
                $selisih_waktu = $jadwal_awal->diff($jadwal_akhir);

                $ambil_terakhir1 = substr($cek_transaksi->jadwal_awal_akhir,8);

            
        ?>
            <?php

                     if($selisih_waktu->h >= 2){
            ?>
            
                <?php 
                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $ambil_terakhir1.' - '.$waktu; ?>',1)"  id="durasi1" type="button" value="01:00" class="set_jam btn btn-danger mb-2">1 Jam</button>
                <?php 
                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('30 minutes'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?=$ambil_terakhir1.' - '.$waktu; ?>',130)"  id="durasi130" type="button" value="01:30" class=" set_jam btn btn-danger mb-2">1 Jam, 30 Menit</button>
                <?php 
                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('2 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $ambil_terakhir1.' - '.$waktu; ?>',2)"  id="durasi2" type="button" value="02:00" class=" set_jam btn btn-danger mb-2">2 Jam</button>
            <!-- 1jam 30 menit -->
            <?php }elseif($selisih_waktu->h == 1 && $selisih_waktu->i >= 30 ){ ?>
                <?php 
                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $ambil_terakhir1.' - '.$waktu; ?>',1)"  id="durasi1" type="button" value="01:00" class="set_jam btn btn-danger mb-2">1 Jam</button>
                <?php 
                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('30 minutes'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $ambil_terakhir1.' - '.$waktu; ?>',130)"  id="durasi130" type="button" value="01:30" class=" set_jam btn btn-danger mb-2">1 Jam, 30 Menit</button>

                <!-- 1jam  -->
            <?php }elseif($selisih_waktu->h == 1 ){ ?>
                <?php 
                $waktu = date_create($tanggal.' '.$ambil_terakhir);
                date_add($waktu, date_interval_create_from_date_string('1 hours'));
                $waktu = date_format($waktu, 'H:i');

                ?>
                <button onclick="setDurasi('<?= $ambil_terakhir1.' - '.$waktu; ?>',1)"  id="durasi1" type="button" value="01:00" class="set_jam btn btn-danger mb-2">1 Jam</button>
                
            <?php }else{
                echo "Mohon maaf, Jadwal psikolog sudah penuh, mohon pilih jadwal lain";
            } ?>
        <?php
               //}
            }
            
        ?>
    
    @else

        
    @endif
    <?php } ?>
@endforeach
<input id="jamDurasi" name="jamDurasi" style="line-height: 19px;"
         hidden    class="form-control">
<script>

    function setDurasi(id,nomor){
        $('#jamDurasi').val(id);
        if(nomor == 1){
            $('#durasi'+nomor).css('background','#28a745');
        }else{
            $('#durasi1').css('background-color','#e67e88');
        }

        if(nomor == 130){
            $('#durasi'+nomor).css('background','#28a745');
        }else{
            $('#durasi130').css('background-color','#e67e88');
        }

        if(nomor == 2){
            $('#durasi'+nomor).css('background','#28a745');
        }else{
            $('#durasi2').css('background-color','#e67e88');
        }
        

        
    }
</script>