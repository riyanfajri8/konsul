<?php
use App\Biodata;
?>
@extends('konsul.layouts.app')

@section('content')
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important;
} */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
<div class="container mt-4" >
    <div id="myCarousel" class="carousel slide" >
        <div class="row" style="margin-top : 150px;">

            @include('konsul.psikologi.menu')
            <div class="col-xl-9 col-lg-8">
            
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="tabel"
                        style="width:100%">
                        <div class="m-portlet__head">
                        <h4 class="text-left mt-4"><b>Daftar Klien Anda</b></h4>
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                    role="tablist">
                                </ul>
                            </div>
                            <!-- <div class="m-portlet__head-tools">

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="m-portlet__nav-link btn btn-sm btn-brand m-btn m-btn--pill m-btn--air"
                                                href="{{url('/psikolog/create-kategori')}}"><i
                                                    class="fa fa-arrow-left"></i> Create </a>
                                        </li>

                                    </ul>
                                </div>
                            </div> -->
                            <!-- start head tools portlet -->
                            <!-- end head tools portlet -->
                        </div>
                        <div class="tab-content">
                            <div class="container mt-4" id="laporancuti">
                            <div class="table-responsive">
                            <table class="table table-striped" padding="1" id="html_table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nama Klien</th>
                                        <th>No Transaksi</th>
                                        <th>Kategori Layanan</th>
                                        <th>tanggal Perjanjian</th>
                                        <th>Jam</th>
                                        <th>Media</th>
                                        <th>status</th>
                                        <th>status konsul</th>
                                        <th>Total Harga</th>
                                        <th>created_at</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($laporan_klien as $laporan_klien1)
                                    @if($laporan_klien1->status_bayar == 'settlement')
                                        <?php
                                            $cek_expired = strtotime($laporan_klien1->jam_transaksi); 
                                            $awal  = new \DateTime($laporan_klien1->created_at); //waktu awal

                                            $akhir = new \DateTime(date('Y-m-d H:i:s')); //waktu akhir

                                            $jumlah_jadwal = $awal->diff($akhir);
                                            $biodata = Biodata::where('id_user',$laporan_klien1->id_user)->first();

                                        ?>
                                        <tr>
                                            <td>{{$biodata->nama}}</td>
                                            <td>{{$laporan_klien1->no_transaksi}}</td>
                                            <td>{{$laporan_klien1->nama_sub_kategori}}</td>
                                            <td>{{date('d-M-Y', strtotime($laporan_klien1->jadwal))}}</td>
                                            <td>{{$laporan_klien1->jam}}</td>
                                            <td>{{$laporan_klien1->nama_media}}</td>
                                            @if($laporan_klien1->status_bayar == 'settlement')
                                                <td><label class="badge badge-primary">Sudah Dibayar</label></td>
                                            @else
                                                <td><span class="badge badge-warning">{{$laporan_klien1->status_bayar}}</span></td>
                                            @endif
                                            <td>{{$laporan_klien1->total_konsul}}</td>
                                            <td> 
                                                @if($laporan_klien1->status_konsul == 0)
                                                    <td><label class="badge badge-warning">Belum Konsul</label></td>
                                                @else
                                                    <td><label class="badge badge-primary">Sudah Konsul</label></td>
                                                @endif
                                            </td>
                                           
                                            
                                            <td>{{date('d-M-Y', strtotime($laporan_klien1->created_at)) }}</td>
                                            <td>
                                                @if($laporan_klien1->status_bayar == 'settlement')
                                                    @if($laporan_klien1->jadwal >= date('Y-m-d'))
                                                        
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if($laporan_klien1->status_konsul == 0)
                                                    <a href="{{url('psikolog/data_klien')}}?status_selesai=1&id_transaksi=<?=$laporan_klien1->id_transaksi  ?>" class="btn btn-primary">Selesi</a>
                                                @else
                                                @endif
                                               
                                            </td>
                                        
                                        </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table> 
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @endsection
@section('scripts')
@parent

@endsection