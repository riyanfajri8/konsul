@extends('konsul.layouts.app')

@section('content')
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important;
} */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}

.card2 {
	position: relative;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: column;
	min-width: 0;
	word-wrap: break-word;
	background-color: #fbfbfb;
	background-clip: border-box;
	border: 1px solid rgba(0, 0, 0, 0.125);
	border-radius: 15px;
}

.card-header2 {
	padding: .75rem 1.25rem;
	margin-bottom: 0;
	background-color: rgba(0, 0, 0, 0.03);
	border-bottom: 1px solid rgba(0, 0, 0, 0.125);
}

.card-body2 {
	-webkit-box-flex: 1;
	-ms-flex: 1 1 auto;
	flex: 1 1 auto;
	padding: 1rem;
    line-height: 2;
}

li{
    font-size: 13px;
}
</style>

<div class="container mt-4">
    <div id="myCarousel" class="carousel slide">
        <div class="row" style="margin-top:150px;">

            @include('konsul.psikologi.menu')
            <div class="col-md-9">
            
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="tabel"
                        style="width:100%">
                        <div class="m-portlet__head">
                        <h4 class="text-left mt-4"><b>Form Tambah Kategori Keahlian</b></h4>
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                    role="tablist">
                                </ul>
                            </div>
                            <div class="m-portlet__head-tools">

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="m-portlet__nav-link btn btn-sm btn-brand m-btn m-btn--pill m-btn--air"
                                                href="{{url('/psikolog/create-kategori')}}"><i
                                                    class="fa fa-arrow-left"></i> Kembali </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <!-- start head tools portlet -->
                            <!-- end head tools portlet -->
                        </div>
                        <div class="tab-content">
                           
                                @if($errors->any())
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    @if(session('success'))
                                        <div class="alert alert-success">
                                            {!!  session('success')  !!}
                                        </div>
                                    @endif

                                <form id="form-pendaftaran" class="m-form m-form--fit m-form--label-align-left  "
                                        action="{{url('psikolog/create-kategori')}}"
                                        method="post">
                                        @csrf
                                    <div class="row">
                                        @foreach($kategori as $kategori)
                                        
                                        <?php 
                                            $sub_kategori = DB::table('kategori_konsul_sub_kategori')
                                            ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                                            ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategori->id_kategori_konsul)
                                            ->get();
                                        ?>
                                                <div class="col-md-5" style="margin: 20px 0 20px 20px; padding-right: 0;">
                                                    <div class="card2">
                                                        <h5 class="card-header2"> <?= $kategori->nama_konsul ?></h5>
                                                    
                                                        <div class="card-body2">   
                                                            @foreach($sub_kategori as $sub_kategori1)
                                                                <li><input type="checkbox" id="sub_kategori" name="sub_kategori[]" value="{{$sub_kategori1->id_sub_kategori}}"><a style="color:#595959;">
                                                                {{$sub_kategori1->nama_sub_kategori}}</a></li>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach
                                           
                                    </div>
    
                                    <div class="row">
                                        <div class="col-md text-center mt-2 mb-5">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                                Simpan Kategori Keahlian
                                            </button>
                                        </div>
                                    </div>
                                    
                                </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    <script>
    $.fn.select2.defaults.set("theme", "bootstrap");
        $(".locationMultiple").select2({
            width: null
        })
    </script>
    
    @endsection
@section('scripts')
@parent

@endsection