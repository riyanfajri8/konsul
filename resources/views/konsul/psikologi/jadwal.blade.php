<style>
    .card{
        padding: 20px;
        margin: 20px;
        border-radius: 15px;
    }

    .m-list-timeline .m-list-timeline__items .m-list-timeline__item .m-list-timeline__time {
	color: #b20b0b;
}

</style>

<?php

use App\JadwalKonsul;

?>
@foreach($hari as $hari)
<?php
    $jadwal_konsul = JadwalKonsul::where('hari',$hari->nama_hari)->where('id_psikologi',$psikolog->id_psikologi)->get();
?>
<div class="col-md-12">
    <div class="card">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse"><?= $hari->nama_hari ?></a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse show">
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_widget4_tab1_content">
                            <div class="m-scrollable m-scroller ps ps--active-y"
                                data-scrollable="true">
                                <div
                                    class="m-list-timeline m-list-timeline--skin-light">
                                    @foreach($jadwal_konsul as $jadwal_konsul)
                                        <div class="m-list-timeline__items">

                                            <div class="m-list-timeline__item">
                                                <span
                                                    class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                                <span
                                                    class="m-list-timeline__text">{{$jadwal_konsul->jadwal}} - {{$jadwal_konsul->jadwal_akhir}} durasi {{$jadwal_konsul->durasi}}</span>
                                                <span onclick="delteJadwal('{{$jadwal_konsul->jadwal}}','{{$hari->nama_hari}}')"
                                                    class="m-list-timeline__time">delete</span>
                                            </div>

                                        </div>
                                        <br>
                                    @endforeach
                                </div>
                                <div class="ps__rail-x">
                                    <div class="ps__thumb-x" tabindex="0"></div>
                                </div>
                                <div class="ps__rail-y">
                                    <div class="ps__thumb-y" tabindex="0"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="m_widget4_tab2_content">
                        </div>
                        <div class="tab-pane" id="m_widget4_tab3_content">
                        </div>
                    </div>
                </div>
                <div class="form-group field-identitaspersonal-id_jenis_identitas required">
                </div>
                <div onclick="tambahJam('{{$hari->nama_hari}}')" data-toggle="modal"
                                    data-target="#myModal" class="btn btn-success btn-block"><center>Tambah Jam</center></div>
            </div>
        </div>
    </div>
    </div>
</div>


@endforeach