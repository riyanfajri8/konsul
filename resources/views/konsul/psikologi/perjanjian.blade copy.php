@extends('konsul.layouts.app2')

@section('content')

<style>
control {
    line-height: 34px;
}

body {
    background: #DCDCDC;
}

.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.social-links li a {
    border-radius: 50%;
    color: rgba(121, 121, 121, .8);
    display: inline-block;
    height: 30px;
    line-height: 27px;
    border: 2px solid rgba(121, 121, 121, .5);
    text-align: center;
    width: 30px
}

.social-links li a:hover {
    color: #797979;
    border: 2px solid #797979
}

.thumb-lg {
    height: 88px;
    width: 88px;
}

.img-thumbnail {
    padding: .25rem;
    background-color: #fff;
    border: 1px solid #dee2e6;
    border-radius: .25rem;
    max-width: 100%;
    height: auto;
}

.text-pink {
    color: #ff679b !important;
}

.btn-rounded {
    border-radius: 2em;
}

.text-muted {
    color: #98a6ad !important;
}

h4 {
    line-height: 22px;
    font-size: 18px;
}
body {
    font-family: 'Montserrat', sans-serif;
    color: #5b5757;
}
</style>
<style>
.fixed-top {
    position: inherit;
}
#header {
    background: #f4f4f4;
}
</style>

    <section class="ftco-section bg-light">
    
    <section id="step">
      <div class="card2 card-timeline px-2 border-none">
        <div class="col-md-8 offset-md-3 justify-content-center">
        <ul class="bs4-order-tracking">
          <li class="step active">
              <div style="width: 38px; height: 38px;"><i style="
    padding-top: 9px;" class="fas fa-clipboard-list"></i></div> Cek Layanan Konsultasi
          </li>
          <li class="step active">
              <div style="width: 38px; height: 38px;"><i style="
    padding-top: 9px;" class="fas fa-user-md"></i></div> Cari Psikolog
          </li>
          <li class="step active">
              <div style="width: 38px; height: 38px;"><i style="
    padding-top: 9px;" class="far fa-calendar-check"></i></div> Buat Janji Konsultasi
          </li>
        </ul>
        </div>
      </div>
    </section>

    <div class="container" style ="background: white;">


        <div class="section-title">
            <h3>Perjanjian <span>Konsul </span></h3>
            <p></p>
        </div>

        <div class="panel panel-primary">

            <div class="panel-body">
                <div class="row">
                    <div class="col-xl-4">
                            <div class="text-center card-box" style="border: 1px solid #2b2eff;">
                                <div class="member-card pt-2 pb-2">
                                    <div class="thumb-lg member-thumb mx-auto"><img
                                            src="{{asset($psikologi->foto)}}"
                                            class="rounded-circle img-thumbnail" alt="profile-image"></div>
                                    <div class="">
                                        <h4>{{$psikologi->nama}}</h4>
                                        <p class="text-muted">Psikolog</p>
                                    </div>
                                    <ul class="social-links list-inline">
                                        <li class="list-inline-item">
                                            <img src="{{asset('logo/like.png')}}"><p class="mb-0 text-muted">98.0%</p> 
                                        </li>
                                        <li class="list-inline-item"></li>
                                        <li class="list-inline-item">
                                            <img src="{{asset('logo/pengalaman.png')}}"><p class="mb-0 text-muted">3 Tahun</p>
                                        </li>
                                    </ul>
                                    <hr>
                                    <button type="button" data-toggle="modal" onclick="bukaProfil(<?= $psikologi->id_psikologi ?>)"
                                        data-target="#modal-profil" 
                                        class="btn btn-primary mt-3 btn-rounded waves-effect w-md waves-light">Lihat Profil</button>
                                    
                                    
                                </div>
                            </div>
                        
                        <!-- <img src="{{asset($psikologi->foto)}}" class="img-rounded" alt=""> -->
                    </div>
                    <div class="col-xl-8">
                        <div class="post-block_keterangan">
                            <form id="save-form" action="{{url('save-perjanjian',[$name])}}" method="GET"
                                class="form-horizontal post-search ">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td class="table-key"><i class="fas fa-chair"></i> Nama Psikologi</td>
                                            <td>
                                                {{$psikologi->nama}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-key"><i class="fas fa-chair"></i> Msalah</td>
                                            <td>
                                                <textarea id="masalah" name="masalah" class="form-control masalah" rows="4">{{$masalah}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-key"><i class="fas fa-chair"></i> Kategori</td>
                                            <td>
                                            <select id="sub_kategori"
                                                class="form-control"
                                                name="sub_kategori">
                                                        @if($name == 0)
                                                            <option selected value="">Pilih Layanan</option>
                                                        @endif
                                                    @foreach($kategori_sub as $kategori_sub )
                                                        @if($name == $kategori_sub->id_sub_kategori){
                                                            <option selected value="{{$name}}">{{$kategori_sub->nama_sub_kategori}}</option>
                                                        @else
                                                            <option value="{{$kategori_sub->id_sub_kategori}}">{{$kategori_sub->nama_sub_kategori}}</option>
                                                        @endif
                                                    @endforeach
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-key"><i class="fas fa-chair"></i> Pilih Tanggal</td>
                                            <td>
                                                <input id="datepicker" onchange="pilihTanggal()"
                                                    style="line-height: 19px;" name="date" class="form-control">

                                                <input id="tanggal" name="id_psikologi" style="line-height: 19px;"
                                                    hidden class="form-control" value="{{$id_psikologi}}">
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="table-key"><i class="fas fa-map-marker-alt"></i> Jam</td>
                                            <td>
                                                <div id="jam-konsul"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-key"> Media</td>
                                            <td>
                                                @foreach($media as $media)
                                                <label class="radio-inline">
                                                    <input id="media" class="mediar" type="radio" name="media"
                                                        value="{{$media->id_media}}">{{$media->nama_media}}
                                                </label>
                                                @endforeach
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <label onclick="buatJanji()" class="btn btn-danger" data-toggle="modal"
                                    data-target="#myModal" style="float: right;"> Buat Janji </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
            if(empty($biodata->jenis_kelamin) || empty($biodata->alamat) || empty($biodata->alamat) || empty($biodata->no_identitas) || empty($biodata->agama) || empty($biodata->pendidikan_terakhir) || empty($biodata->status_perkawinan || empty($biodata->pekerjaan) || empty($biodata->tempat_lahir))){
        ?>
    <form id="payment-form" method="get" action="Payment">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="result_type" id="result-type" value=""></div>
      <input type="hidden" name="result_data" id="result-data" value=""></div>
    </form>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Isi Data Diri Untuk Melanjutkan</h4>
            </div>
            <div class="modal-body">
                @include('konsul.psikologi.updateProfil')

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php
    }
?>

<!-- Modal -->
<div id="modal-profil" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profil</h4>
            </div>
            <div class="modal-body">
                <div id="profilSingkat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection
<script type="text/javascript"
	      src="https://app.sandbox.midtrans.com/snap/snap.js"
	      data-client-key="SB-Mid-client-gPB5wXO09A70jve3"></script>

<script>
function bukaProfil(id){
        $('#profilSingkat').load('{{url("/profil-psikologi")}}/'+id, function(e) {});
    }
function buatJanji() {
    '<?php
            if(empty($biodata->jenis_kelamin) || empty($biodata->alamat) || empty($biodata->alamat) || empty($biodata->no_identitas) || empty($biodata->agama) || empty($biodata->pendidikan_terakhir) || empty($biodata->status_perkawinan || empty($biodata->pekerjaan) || empty($biodata->tempat_lahir))){
        ?>'

    return false;
    '<?php
            }
        ?>'
    var masalah = $('#masalah').val();
    var tanggal =$('#tanggal').val();
    var jamJadwalDipilih = $('#jamJadwalDipilih').val();

    if(masalah.length <= 0){
        swal('Perhatian!', 'Ketik Masalah Anda', 'error');
            $("#masalah").css('outline', '1px solid red');
        return false;
    }else{
        $("#masalah").css('outline', '1px solid #ccc');
    }

    if(jamJadwalDipilih.length <= 0){
        swal('Perhatian!', 'Pilih Jam Masih Kosong', 'error');
            $("#jamJadwalDipilih").css('outline', '1px solid red');
        return false;
    }else{
        $("#jamJadwalDipilih").css('outline', '1px solid #ccc');
    }

    if(tanggal.length <= 0){
        swal('Perhatian!', 'Isi tanggal masih kosong', 'error');
            $("#tanggal").css('outline', '1px solid red');
        return false;
    }else{
        $("#tanggal").css('outline', '1px solid #ccc');
    }

    if($('input[name=media]:checked').length <= 0){
        swal('Perhatian!', 'Ceklis salah satu media', 'error');
            $(".mediar").css('outline', '1px solid red');
        return false;
    }else{
        $(".mediar").css('outline', '1px solid #ccc');
    }
    if($('#sub_kategori').val() == ''){
        swal('Perhatian!', 'Pilih Layanan', 'error');
            $("#sub_kategori").css('outline', '1px solid red');
        return false;
    }else{
        $("#sub_kategori").css('outline', '1px solid #ccc');
        var sub_kategori = $('#sub_kategori').val();
    }
    $.ajax({
        url: '{{url("save-perjanjian")}}/'+sub_kategori,
        type: "GET",
        data: $("#save-form").serialize(),
        dataType: "html",
        success: function(data) {
                swal("Sukses", "Berahsil Menyimpan", "success");
                location.href ="{{url('/sukses')}}/"+data+"/proses";
        }
    });
}

function pilihTanggal() {
    var tanggal = $('#datepicker').val();
    $.ajax({
        url: '{{url("pilih-tanggal-order")}}/' + tanggal + "/{{$psikologi->id_psikologi}}",
        type: "GET",
        dataType: "html",
        success: function(data) {
            console.log(data);
            $('#jam-konsul').html(data);
        }
    });

}
</script>
@section('scripts')
@parent

@endsection