<?php
use App\Lib;
?>
@extends('konsul.layouts.app2')

@section('content')

<style>
    control {
        line-height: 34px;
    }

    .badge-warning {
        color: #212529;
        background-color: #ffed4a;
    }

    .badge-primary {
        color: #fff;
        background-color: #3490dc;
    }
    }
</style>
<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="SB-Mid-client-gPB5wXO09A70jve3"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<section id="services" class="services">
    <div class="container">

        <section class="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading"><i class='fas fa-check-circle'></i> Selamat, Pemesanan Berhasil
                                Dibuat</h4>
                            <p>Kamu telah berhasil melakukan Pemesanan PsyQonsul, silahkan melanjutkan ke proses
                                pembayaran dengan klik tombol Bayar</p>
                            @if(!empty($transaksi->type_pembayaran))
                            <hr>
                            <p>Untuk info pembayaran & invoice bisa lihat halaman invoice berikut ini</p>
                            <a href="{{$transaksi->pdf_url}}" class='btn btn-primary'>Halaman Invoice</a>.</p>
                            @endif
                        </div>
                    </div>

                    <div class="container mt-3">
                        <h5>Detail Pemesanan</h5>
                    </div>

                    <form id="payment-form" method="get" action="Payment">
                        <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
                        <input type="hidden" name="result_type" id="result-type" value="">
                </div>
                <input type="hidden" name="result_data" id="result-data" value="">
            </div>
            </from>

            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-hover" id="html_table">
                                <thead>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <th>:</th>
                                        <th>{{Auth::user()->name}}</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Nomor Transaksi</th>
                                        <th>:</th>
                                        <th>{{$transaksi->no_transaksi}}</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Nama Psikolog</th>
                                        <th>:</th>
                                        <th>{{$laporan_klien->nama}}</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Tanggal Perjanjian</th>
                                        <th>:</th>
                                        <th>{{date('d-M-Y', strtotime($transaksi->jadwal))}}</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Jam</th>
                                        <th>:</th>
                                        <th>{{$transaksi->jadwal_awal_akhir}}</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Memakai Media</th>
                                        <th>:</th>
                                        <th>{{$transaksi->nama_media}}</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Total Harga</th>
                                        <th>:</th>
                                        <th>
                                            <?= Lib::rupiah($laporan_klien->total_konsul) ?>
                                        </th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>:</th>
                                        @if($transaksi->status_bayar == 'settlement')
                                        <th><label class="badge badge-primary">Sudah Dibayar</label></th>
                                        @else
                                        <th><span class="badge badge-warning">Belum Bayar</span></th>
                                        @endif
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        @if(empty($transaksi->type_pembayaran))
                                        <td>
                                            <label class="btn btn-primary" id="pay-button">Bayar</label>
                                        </td>
                                        @else
                                        @if(!empty($transaksi->type_pembayaran))
                                        <td>{{$transaksi->type_pembayaran}}</td>
                                        @endif
                                        @if(!empty($transaksi->bank))
                                        <td>{{$transaksi->bank}}</td>
                                        @endif
                                        @if(!empty($transaksi->va_number))
                                        <td>{{$transaksi->va_number}}</td>
                                        @endif
                                        @if(!empty($transaksi->biller_code))
                                        <td>{{$transaksi->biller_code}}</td>
                                        @endif
                                        @if(!empty($transaksi->bill_key))
                                        <td>{{$transaksi->bill_key}}</td>
                                        @endif
                                        @if(!empty($transaksi->store))
                                        <td>{{$transaksi->store}}</td>
                                        @endif
                                        @if(!empty($transaksi->payment_code))
                                        <td>{{$transaksi->payment_code}}</td>
                                        @endif
                                        @endif
                                    </tr>
                                </thead>

                                
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-12">
                <div class="table-responsive">
                    <table class="table table-striped" padding="1" id="html_table" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>No Transaksi</th>
                                <th>Nama Pesanan</th>
                                <th>Psikolog</th>
                                <th>tanggal</th>
                                <th>Jam</th>
                                <th>Media</th>
                                <th>status</th>
                                <th>Total Harga</th>

                                @if(empty($transaksi->type_pembayaran))
                                <th></th>
                                @else

                                @if(!empty($transaksi->type_pembayaran))
                                <th>type pembayaran</th>
                                @endif

                                @if(!empty($transaksi->bank))
                                <th>bank</th>
                                @endif

                                @if(!empty($transaksi->va_number))
                                <th>va number</th>
                                @endif

                                @if(!empty($transaksi->biller_code))
                                <th>biller code </th>
                                @endif

                                @if(!empty($transaksi->bill_key))
                                <th> bill key </th>
                                @endif

                                @if(!empty($transaksi->store))
                                <th>store</th>
                                @endif

                                @if(!empty($transaksi->payment_code))
                                <th>Payment Code </td>
                                    @endif
                                    @endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{Auth::user()->name}}</td>
                                <td>{{$transaksi->no_transaksi}}</td>
                                <td>{{$laporan_klien->nama_sub_kategori}}</td>
                                <td>{{$laporan_klien->nama}}</td>
                                <td>{{$transaksi->jadwal}}</td>
                                <td>{{$transaksi->jam}}</td>
                                <td>{{$transaksi->nama_media}}</td>
                                @if($transaksi->status_bayar == 'settlement')
                                <td><label class="badge badge-primary">Sudah Dibayar</label></td>
                                @else
                                <td><span class="badge badge-warning">Belum Bayar</span></td>
                                @endif
                                <td><?= Lib::rupiah($laporan_klien->total_konsul) ?></td>
                                @if(empty($transaksi->type_pembayaran))
                                <td>
                                    <label class="btn btn-primary" id="pay-button">Bayar</label>
                                </td>
                                @else
                                @if(!empty($transaksi->type_pembayaran))
                                <td>{{$transaksi->type_pembayaran}}</td>
                                @endif
                                @if(!empty($transaksi->bank))
                                <td>{{$transaksi->bank}}</td>
                                @endif
                                @if(!empty($transaksi->va_number))
                                <td>{{$transaksi->va_number}}</td>
                                @endif
                                @if(!empty($transaksi->biller_code))
                                <td>{{$transaksi->biller_code}}</td>
                                @endif
                                @if(!empty($transaksi->bill_key))
                                <td>{{$transaksi->bill_key}}</td>
                                @endif
                                @if(!empty($transaksi->store))
                                <td>{{$transaksi->store}}</td>
                                @endif
                                @if(!empty($transaksi->payment_code))
                                <td>{{$transaksi->payment_code}}</td>
                                @endif
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <div class="m-portlet m-portlet--tabs">

                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="m_tabs_6_1" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="m-section">
                                            <div class="m-section__content">
                                                <table class="table m-table m-table--head-separator-secondary">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">Nama
                                                                Lengkap</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">{{Auth::user()->name}}</th>
                                                        </tr>
                                                    </thead>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">Nomor
                                                                Transaksi</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">{{$transaksi->no_transaksi}}</th>
                                                        </tr>
                                                    </thead>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">Nama Psikolog</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">
                                                                {{$laporan_klien->nama}}</th>
                                                        </tr>
                                                    </thead>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">
                                                                Tanggal Perjanjian
                                                            </th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">
                                                                {{date('d-M-Y', strtotime($transaksi->jadwal))}}
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">
                                                                Jam</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">{{$transaksi->jadwal_awal_akhir}}</th>
                                                        </tr>
                                                    </thead>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">Memakai Media
                                                            </th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">{{$transaksi->nama_media}}
                                                            </th>
                                                        </tr>
                                                    </thead>



                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="m-section">
                                            <div class="m-section__content">
                                                <table class="table m-table m-table--head-separator-secondary">


                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">
                                                                Total Harga</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">
                                                                <?= Lib::rupiah($laporan_klien->total_konsul) ?>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">
                                                                Pekerjaan</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">
                                                                Karyawan Swasta</th>
                                                        </tr>
                                                    </thead>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane" id="m_tabs_6_3" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="m-section">
                                            <div class="m-section__content">
                                                <table class="table m-table m-table--head-separator-secondary">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-right">
                                                                Pekerjaan</th>
                                                            <th class="text-center">:
                                                            </th>
                                                            <th class="text-left">
                                                                Karyawan Swasta</th>
                                                        </tr>
                                                    </thead>


                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                
            </div>

    </div>
    </div>
</section>

</div>
</div>

<script>
    $('#pay-button').click(function (event) {
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type, data) {
            $("#result-type").val(type);
            $("#result-data").val(JSON.stringify(data));
            //resultType.innerHTML = type;
            //resultData.innerHTML = JSON.stringify(data);
        }
        snap.pay('<?=$snapToken?>', {

            onSuccess: function (result) {
                changeResult('success', result);
                console.log(result.status_message);
                console.log(result);
                $("#payment-form").submit();
            },
            onPending: function (result) {
                changeResult('pending', result);
                console.log(result.status_message);
                $("#payment-form").submit();
            },
            onError: function (result) {
                changeResult('error', result);
                console.log(result.status_message);
                $("#payment-form").submit();
            }
        });
    });
</script>

@endsection
@section('scripts')
@parent

@endsection