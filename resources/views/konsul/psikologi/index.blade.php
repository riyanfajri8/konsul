

<?php
?>
@foreach($psikologi as $psikologi1)
    <!-- <label class="radio-inline"><input type="radio" name="jam" value="{{$psikologi1->jadwal}}">{{$psikologi1->jadwal}}</label> -->
    <?php
         $cek_transaksi = DB::table('transaksi')->where('jadwal',$tanggal)
         ->where('id_psikologi',$psikologi1->id_psikologi)
         ->where('jam',$psikologi1->jadwal.':00')
         ->first();
    ?>

    @if(!empty($cek_transaksi->jam))
        <?php $jam_tran = $cek_transaksi->jam ?>
    @else
        <?php $jam_tran = '' ?>
    @endif

    @if(!empty($cek_transaksi))
        <?php 
            date_default_timezone_set('Asia/Jakarta');
            $cek_expired = strtotime($cek_transaksi->jam); 
            $awal  = new \DateTime($cek_transaksi->created_at); //waktu awal

            $akhir = new \DateTime(date('Y-m-d H:i:s')); //waktu akhir

            $jumlah_jadwal = $awal->diff($akhir);

            if($jumlah_jadwal->h > 1){ 
                $cek_sudah_pesan = DB::table('transaksi')->where('jadwal',$tanggal)
                            ->where('id_psikologi',$psikologi1->id_psikologi)
                            ->where('jam',$psikologi1->jadwal.':00')
                            ->where('status_bayar','settlement')
                            ->first();
               if(empty($cek_sudah_pesan)){
            
        ?>
        <button  id="setJadwal{{$psikologi1->id_jadwal_konsul}}" type="button" value="{{$psikologi1->jadwal}}" class=" set_jam btn btn-danger mb-2">{{$psikologi1->jadwal}} - {{$psikologi1->jadwal_akhir}} </button>
        <?php
               }
            }
            
        ?>

    @else
    <?php
    ?>
        <button id="setJadwal{{$psikologi1->id_jadwal_konsul}}" value="{{$psikologi1->jadwal}}" onclick="setJam(<?= $psikologi1->id_jadwal_konsul ?>,`<?= $psikologi1->jadwal ?>`)" type="button" class="set_jam btn btn-danger mb-2">{{$psikologi1->jadwal}} - {{$psikologi1->jadwal_akhir}}</button>
    @endif

@endforeach
<input id="jamJadwalDipilih" name="jadwalJam" style="line-height: 19px;"
            hidden class="form-control">
<script>

    function setJam(id,jam){
            if(jam == $('#setJadwal'+id).val()){
                $('#setJadwal'+id).css('background','#28a745');
                $('#jamJadwalDipilih').val(id);
            }

            
            '<?php foreach($psikologi as $psikologi2){ ?>'

                if(id != '<?= $psikologi2->id_jadwal_konsul ?>'){;
                    
                    $('#setJadwal<?= $psikologi2->id_jadwal_konsul ?>').css('background-color','#e67e88');
                }
            '<?php } ?>'
            var tanggal = $('#datepicker').val();
            $.ajax({
                url: '{{url("pilih-tanggal-order")}}/' + tanggal + "/{{$psikologi1->id_psikologi}}?durasi=1&id_jadwal_konsul="+id,
                type: "GET",
                dataType: "html",
                success: function(data) {
                    console.log(data);
                    $('#durasi').html(data);
                }
            });  
            
    }
</script>