<form action='{{url("psikolog/update-profil-user")}}?ajax=1' id="form-pendaftaran" files="true" enctype="multipart/form-data" class=""  method="post">
    @csrf

    <div class="row">
        <div class="col-md">
            <div class="login-wrap2 p-4 mt-3">
                
                        <div class="form-group">
                            <label class="label" for="name">Nama Lengkap</label>
                            <input type="text" id="personal-1-nama_lengkap" name="nama" value="{{ Auth::user()->name }}" class="form-control" required placeholder="">
                        </div>
                        <div class="form-group">
                            <label class="label" for="no_hp">No.Handphone / WA</label>
                            <input type="number" class="form-control" value="{{$biodata->no_hp}}" id="kontakpersonal-1-kontak" name="no_hp" required>
                        </div>
                        <div class="form-group">
                            <label class="label" for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" value="{{$biodata->tanggal_lahir}}"  required name="tanggal_lahir">
                        </div>
                        <div class="form-group">
                            <label class="label" for="jk">Jenis Kelamin</label>
                            <select id="personal-1-jenis_kelamin" class="form-control" name="jenis_kelamin">
                                <option value="">-</option>
                                <option value="Laki-laki"
                                    <?php if ($biodata->jenis_kelamin == 'Laki-laki'){ echo ' selected=""'; }?>>Laki-laki
                                </option>
                                <option value="Perempuan"
                                    <?php if ($biodata->jenis_kelamin == 'Perempuan'){ echo ' selected=""'; }?>>Perempuan
                                </option>
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="label" for="status">Status Pernikahan</label>
                            <select id="personal-1-status_perkawinan" class="form-control" name="status_perkawinan">
                                <option value="">-</option>
                                <option value="Belum Menikah" <?php if ($biodata->status_perkawinan == 'Belum Menikah')
                                { echo ' selected=""'; }?>>Belum Menikah</option>
                                <option value="Menikah"
                                    <?php if ($biodata->status_perkawinan == 'Menikah'){ echo ' selected=""'; }?>>Menikah
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="label" for="pendidikan">Pendidikan Terakhir</label>
                            <select id="personal-1-id_pendidikan" class="form-control" name="pendidikan_terakhir">
                                <option value="">-</option>
                                <option
                                    <?php if ($biodata->pendidikan_terakhir == 'Belum Sekolah'){ echo ' selected=""'; }?>
                                    value="Belum Sekolah">Belum Sekolah</option>
                                <option
                                    <?php if ($biodata->pendidikan_terakhir == 'Tidak Tamat SD'){ echo ' selected=""'; }?>
                                    value="Tidak Tamat SD">Tidak Tamat SD</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'TK'){ echo ' selected=""'; }?>
                                    value="TK">TK</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'SD'){ echo ' selected=""'; }?>
                                    value="SD">SD</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'SLTP'){ echo ' selected=""'; }?>
                                    value="SLTP">SLTP</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'SLTA'){ echo ' selected=""'; }?>
                                    value="SLTA">SLTA</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'D1'){ echo ' selected=""'; }?>
                                    value="D1">D1</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'D3'){ echo ' selected=""'; }?>
                                    value="D3">D3</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'D4'){ echo ' selected=""'; }?>
                                    value="D4">D4</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'S1'){ echo ' selected=""'; }?>
                                    value="S1">S1</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'S2'){ echo ' selected=""'; }?>
                                    value="S2">S2</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'S3'){ echo ' selected=""'; }?>
                                    value="S3">S3</option>
                                <option <?php if ($biodata->pendidikan_terakhir == 'dr'){ echo ' selected=""'; }?>
                                    value="dr">dr</option>
                                <option
                                    <?php if ($biodata->pendidikan_terakhir == 'Belum Tamat Sekolah'){ echo ' selected=""'; }?>
                                    value="Belum Tamat Sekolah">Belum Tamat Sekolah</option>
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="label" for="pekerjaan">Pekerjaan</label>
                            <select id="personal-1-pekerjaan" class="form-control" name="pekerjaan">
                                <option <?php if ($biodata->pekerjaan == ''){ echo ' selected=""'; }?> value="">-</option>

                                <option <?php if ($biodata->pekerjaan == 'Akademik/Pengajar'){ echo ' selected=""'; }?>
                                    value="Akademik/Pengajar">Akademik/Pengajar</option>

                                <option <?php if ($biodata->pekerjaan == 'Ibu Rumah Tangga'){ echo ' selected=""'; }?>
                                    value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>

                                <option <?php if ($biodata->pekerjaan == 'Karyawan BUMN'){ echo ' selected=""'; }?>
                                    value="Karyawan BUMN">Karyawan BUMN</option>

                                <option <?php if ($biodata->pekerjaan == 'Karyawan Swasta'){ echo ' selected=""'; }?>
                                    value="Karyawan Swasta" data-select2-id="6">

                                    Karyawan Swasta</option>
                                <option <?php if ($biodata->pekerjaan == 'Lain-lain'){ echo ' selected=""'; }?>
                                    value="Lain-lain">Lain-lain</option>

                                <option
                                    <?php if ($biodata->pekerjaan == 'Pegawai Negeri Sipil (PNS)'){ echo ' selected=""'; }?>
                                    value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)
                                </option>

                                <option <?php if ($biodata->pekerjaan == 'Pelajar / Mahasiswa'){ echo ' selected=""'; }?>
                                    value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>

                                <option
                                    <?php if ($biodata->pekerjaan == 'Pemerintahan / Militer'){ echo ' selected=""'; }?>
                                    value="Pemerintahan / Militer">Pemerintahan / Militer</option>

                                <option <?php if ($biodata->pekerjaan == 'Pensiunan'){ echo ' selected=""'; }?>
                                    value="Pensiunan">Pensiunan</option>

                                <option <?php if ($biodata->pekerjaan == 'Petani'){ echo ' selected=""'; }?>
                                    value="Petani">Petani</option>
                                <option <?php if ($biodata->pekerjaan == 'Profesional ( Dokter, Pengacara,
                                                                            Dll )'){ echo ' selected=""'; }?> value="Profesional ( Dokter, Pengacara,
                                                                            Dll )">Profesional ( Dokter, Pengacara,
                                    Dll )</option>

                                <option <?php if ($biodata->pekerjaan == 'Tidak Bekerja'){ echo ' selected=""'; }?>
                                    value="Tidak Bekerja">Tidak Bekerja</option>

                                <option <?php if ($biodata->pekerjaan == 'Wiraswasta'){ echo ' selected=""'; }?>
                                    value="Wiraswasta">Wiraswasta</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="label" for="email">Kota tempat tinggal saat ini</label>
                            <input type="text" id="personal-1-alamat_sementara" class="form-control" name="alamat"
                                value="{{$biodata->alamat}}">
                            <div class="help-block"></div>
                        </div>
               
            </div>




            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="">Nama Lengkap:</label>
                    <div class="form-group field-personal-1-nama_lengkap ">

                        <input type="text" id="personal-1-nama_lengkap" class="form-control" name="nama"
                            value="{{ Auth::user()->name }}" placeholder="Isi Nama Lengkap">

                        <div class="help-block"></div>
                    </div>
                </div>
            </div> -->

            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label>Jenis Identitas:</label>

                    <div class="form-group field-identitaspersonal-id_jenis_identitas required">

                        <select id="identitaspersonal-id_jenis_identitas" class="form-control" name="jenis_identitas"
                            aria-required="true">
                            <option value="" <?php if ($biodata->jenis_identitas == ''){ echo ' selected=""'; }?>>Pilih
                            </option>
                            <option value="KTP"
                                <?php if ($biodata->jenis_identitas == 'KTP'){ echo ' selected=""'; }?>>KTP</option>
                            <option value="SIM"
                                <?php if ($biodata->jenis_identitas == 'SIM'){ echo ' selected=""'; }?>>SIM</option>
                            <option value="Passport"
                                <?php if ($biodata->jenis_identitas == 'Passport'){ echo ' selected=""'; }?>>Passport
                            </option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="">No. Identitas:</label>
                    <div class="form-group field-identitaspersonal-no_identitas required">

                        <input type="text" id="identitaspersonal-no_identitas" class="form-control " name="no_identitas"
                            value="{{$biodata->no_identitas}}" placeholder="no_identitas" aria-required="true">

                        <div class="help-block"></div>
                    </div>
                </div>
            </div> -->

            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-4">
                    <label>Tempat Lahir:</label>
                    <div class="form-group field-personal-1-tempat_lahir required">

                        <input type="text" id="personal-1-tempat_lahir" class="form-control " name="tempat_lahir"
                            value="{{$biodata->tempat_lahir}}" placeholder="Isi Tempat Lahir">

                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label class="">Tanggal Lahir:</label>
                    <div class="form-group  ">
                        <div class="input-group date" data-provide="datepicker">
                            <input name="tanggal_lahir" id="datepicker" type="text" class="form-control "
                                value="{{$biodata->tanggal_lahir}}">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>

                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label>Jenis Kelamin:</label>

                    <div class="form-group field-personal-1-jenis_kelamin required">

                        <select id="personal-1-jenis_kelamin" class="form-control" name="jenis_kelamin">
                            <option value="">Pilih</option>
                            <option value="Laki-laki"
                                <?php if ($biodata->jenis_kelamin == 'Laki-laki'){ echo ' selected=""'; }?>>Laki-laki
                            </option>
                            <option value="Perempuan"
                                <?php if ($biodata->jenis_kelamin == 'Perempuan'){ echo ' selected=""'; }?>>Perempuan
                            </option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                </div>
            </div> -->

            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-4">
                    <label class="">Status Perkawinan:</label>

                    <div class="form-group field-personal-1-status_perkawinan required">

                        <select id="personal-1-status_perkawinan" class="form-control" name="status_perkawinan">
                            <option value="">Pilih</option>
                            <option value="Belum Menikah" <?php if ($biodata->status_perkawinan == 'Belum
                                                                        Menikah'){ echo ' selected=""'; }?>>Belum
                                Menikah</option>
                            <option value="Menikah"
                                <?php if ($biodata->status_perkawinan == 'Menikah'){ echo ' selected=""'; }?>>Menikah
                            </option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                </div> -->

                <!-- <div class="col-lg-4">
                    <label>Agama:</label>
                    <div class="form-group field-personal-1-id_agama required">

                        <select id="personal-1-id_agama" class="form-control" name="agama">
                            <option value="">Pilih</option>
                            <option <?php if ($biodata->agama == 'Islam'){ echo ' selected=""'; }?> value="Islam">Islam
                            </option>
                            <option <?php if ($biodata->agama == 'Budha'){ echo ' selected=""'; }?> value="Budha">Budha
                            </option>
                            <option <?php if ($biodata->agama == 'Hindu'){ echo ' selected=""'; }?> value="Hindu">Hindu
                            </option>
                            <option <?php if ($biodata->agama == 'Kristen'){ echo ' selected=""'; }?> value="Kristen">
                                Kristen</option>
                            <option <?php if ($biodata->agama == 'Katholik'){ echo ' selected=""'; }?>
                                value="Katholik">Katholik</option>
                            <option <?php if ($biodata->agama == 'Konghucu'){ echo ' selected=""'; }?>
                                value="Konghucu">Konghucu</option>
                            <option <?php if ($biodata->agama == 'Kepercayaan'){ echo ' selected=""'; }?>
                                value="Kepercayaan">Kepercayaan</option>
                            <option <?php if ($biodata->agama == 'Lain-lain'){ echo ' selected=""'; }?>
                                value="Lain-lain">Lain-lain</option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <label>Pendidikan Terakhir:</label>
                    <div class="form-group field-personal-1-id_pendidikan required">

                        <select id="personal-1-id_pendidikan" class="form-control" name="pendidikan_terakhir">
                            <option value="">Pilih</option>
                            <option
                                <?php if ($biodata->pendidikan_terakhir == 'Belum Sekolah'){ echo ' selected=""'; }?>
                                value="Belum Sekolah">Belum Sekolah</option>
                            <option
                                <?php if ($biodata->pendidikan_terakhir == 'Tidak Tamat SD'){ echo ' selected=""'; }?>
                                value="Tidak Tamat SD">Tidak Tamat SD</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'TK'){ echo ' selected=""'; }?>
                                value="TK">TK</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'SD'){ echo ' selected=""'; }?>
                                value="SD">SD</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'SLTP'){ echo ' selected=""'; }?>
                                value="SLTP">SLTP</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'SLTA'){ echo ' selected=""'; }?>
                                value="SLTA">SLTA</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'D1'){ echo ' selected=""'; }?>
                                value="D1">D1</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'D3'){ echo ' selected=""'; }?>
                                value="D3">D3</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'D4'){ echo ' selected=""'; }?>
                                value="D4">D4</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'S1'){ echo ' selected=""'; }?>
                                value="S1">S1</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'S2'){ echo ' selected=""'; }?>
                                value="S2">S2</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'S3'){ echo ' selected=""'; }?>
                                value="S3">S3</option>
                            <option <?php if ($biodata->pendidikan_terakhir == 'dr'){ echo ' selected=""'; }?>
                                value="dr">dr</option>
                            <option
                                <?php if ($biodata->pendidikan_terakhir == 'Belum Tamat Sekolah'){ echo ' selected=""'; }?>
                                value="Belum Tamat Sekolah">Belum Tamat Sekolah</option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                </div>
            </div> -->

            <!-- <div class="form-group m-form__group row">

                <div class="col-lg-6">
                    <label class="">No. HP:</label>
                    <div class="form-group field-kontakpersonal-1-kontak">

                        <input type="text" id="kontakpersonal-1-kontak" class="form-control" name="no_hp"
                            value="{{$biodata->no_hp}}" placeholder="Isi No. HP">

                        <div class="help-block"></div>
                    </div>
                </div>

                <div class="col-lg-6">
                    {!! Form::label('img', 'Upload File (KTP/SIM/Passport) max 500 kb', ['class' => 'control-label']) !!}
                    <img id="uploadPreview"  style="width: 150px;" />
                    <input class="form-control" id="img" type="file" name="img" onchange="PreviewImage();" />
                    
                    <p class="help-block"></p>
                    @if($errors->has('img'))
                        <p class="help-block">
                            {{ $errors->first('img_name') }}
                        </p>
                    @endif
                </div>
              
            </div> -->

            <!-- <div class="form-group m-form__group row">
                <div class="col-lg-6">
                    <label class="">Pekerjaan:</label>
                    <div class="form-group field-personal-1-pekerjaan required">
                        <select id="personal-1-pekerjaan" class="form-control" name="pekerjaan">
                            <option <?php if ($biodata->pekerjaan == ''){ echo ' selected=""'; }?> value="">Pilih
                                Pekerjaan</option>

                            <option <?php if ($biodata->pekerjaan == 'Akademik/Pengajar'){ echo ' selected=""'; }?>
                                value="Akademik/Pengajar">Akademik/Pengajar</option>

                            <option <?php if ($biodata->pekerjaan == 'Ibu Rumah Tangga'){ echo ' selected=""'; }?>
                                value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>

                            <option <?php if ($biodata->pekerjaan == 'Karyawan BUMN'){ echo ' selected=""'; }?>
                                value="Karyawan BUMN">Karyawan BUMN</option>

                            <option <?php if ($biodata->pekerjaan == 'Karyawan Swasta'){ echo ' selected=""'; }?>
                                value="Karyawan Swasta" data-select2-id="6">

                                Karyawan Swasta</option>
                            <option <?php if ($biodata->pekerjaan == 'Lain-lain'){ echo ' selected=""'; }?>
                                value="Lain-lain">Lain-lain</option>

                            <option
                                <?php if ($biodata->pekerjaan == 'Pegawai Negeri Sipil (PNS)'){ echo ' selected=""'; }?>
                                value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)
                            </option>

                            <option <?php if ($biodata->pekerjaan == 'Pelajar / Mahasiswa'){ echo ' selected=""'; }?>
                                value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>

                            <option
                                <?php if ($biodata->pekerjaan == 'Pemerintahan / Militer'){ echo ' selected=""'; }?>
                                value="Pemerintahan / Militer">Pemerintahan / Militer</option>

                            <option <?php if ($biodata->pekerjaan == 'Pensiunan'){ echo ' selected=""'; }?>
                                value="Pensiunan">Pensiunan</option>

                            <option <?php if ($biodata->pekerjaan == 'Petani'){ echo ' selected=""'; }?>
                                value="Petani">Petani</option>
                            <option <?php if ($biodata->pekerjaan == 'Profesional ( Dokter, Pengacara,
                                                                        Dll )'){ echo ' selected=""'; }?> value="Profesional ( Dokter, Pengacara,
                                                                        Dll )">Profesional ( Dokter, Pengacara,
                                Dll )</option>

                            <option <?php if ($biodata->pekerjaan == 'Tidak Bekerja'){ echo ' selected=""'; }?>
                                value="Tidak Bekerja">Tidak Bekerja</option>

                            <option <?php if ($biodata->pekerjaan == 'Wiraswasta'){ echo ' selected=""'; }?>
                                value="Wiraswasta">Wiraswasta</option>
                        </select>

                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label>Alamat tempat tinggal sekarang:</label>
                    <div class="form-group field-personal-1-alamat_sementara required">

                        <input type="text" id="personal-1-alamat_sementara" class="form-control" name="alamat"
                            value="{{$biodata->alamat}}">

                        <div class="help-block"></div>
                    </div>
                </div>
            </div> -->

            <!-- INFORMASI MODEL DEPARTEMEN -->
            <div class="m-form__heading"></div>
        </div>
    </div>


    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions--solid">
            <div class="row">
                <div class="col-md text-center">
                    <label onclick="nextPerjanjian()" class="btn btn-success"> Simpan Data</label>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    function nextPerjanjian(){
        $.ajax({
            url: '{{url("psikolog/update-profil-user")}}?ajax=1' ,
            type: "post",
            data: $('#form-pendaftaran').serialize(),
            dataType: "html",
            success: function(data) {
                console.log(data);
                if(data == 'berhasil'){
                    swal("Sukses", "Berahsil Menyimpan", "success");
                    location.reload();
                }
                if(data == 'tidak lengkap'){
                    swal("error!", "Ada form yang belum terisi", "error");
                }
                if(data == 'file_besar'){
                    swal("error!", "max ukuran foto 500 kb", "error");
                }
            }
        });
    }

    $("#form-pendaftaran").on('submit',(function(e) {
        var nama = $('#personal-1-nama_lengkap').val();
        var jenis_kelamin = $('#personal-1-jenis_kelamin').find(":selected").text();
        var jenis_identitas =  $('#identitaspersonal-id_jenis_identitas').find(":selected").val();
        var no_identitas = $('#identitaspersonal-no_identitas').val();
        var tempat_lahir = $('#personal-1-tempat_lahir').val();

        var agama =  $('#personal-1-id_agama').find(":selected").val();
        var tanggal = $('#datepicker').val()
        var status_perkawinan =  $('#personal-1-status_perkawinan').find(":selected").val();
        var pendidikan_terakhir =  $('#personal-1-id_pendidikan').find(":selected").val();
        var kontak = $('#kontakpersonal-1-kontak').val();
        var pekerjaan =  $('#personal-1-pekerjaan').find(":selected").val();
        var alamat = $('#personal-1-alamat_sementara').val();
        var img = $('#img').val();
        
        //alert(jenis_identitas);

        if(nama.length <= 0){
            swal('Perhatian!', 'Nama Lengkap Tidak boleh kosong', 'error');
                $("#personal-1-nama_lengkap").css('outline', '1px solid red');
            return false;
        }else{
            $("#personal-1-nama_lengkap").css('outline', '1px solid #ccc');
        }

        // if(img.length <= 0){
        //     swal('Perhatian!', 'Upload file identitas anda', 'error');
        //         $("#img").css('outline', '1px solid red');
        //     return false;
        // }else{
        //     $("#img").css('outline', '1px solid #ccc');
        // }
        
        if(no_identitas.length <= 0){
            swal('Perhatian!', 'NO Identitas Tidak Boleh Kosong', 'error');
                $("#identitaspersonal-no_identitas").css('outline', '1px solid red');
            return false;
        }else{
            $("#identitaspersonal-no_identitas").css('outline', '1px solid #ccc');
        }
        
        if(tempat_lahir.length <= 0){
            swal('Perhatian!', 'Tempat Lahir Tidak Boleh Kosong', 'error');
                $("#personal-1-tempat_lahir").css('outline', '1px solid red');
            return false;
        }else{
            $("#personal-1-tempat_lahir").css('outline', '1px solid #ccc');
        }

        if(tanggal.length <= 0){
            swal('Perhatian!', 'Tanggal Lahir Tidak Boleh Kosong', 'error');
                $("#datepicker").css('outline', '1px solid red');
            return false;
        }else{
            $("#datepicker").css('outline', '1px solid #ccc');
        }


        if( jenis_kelamin.length <= 5){
            swal('Perhatian!', 'Pilih salah satu Jenis Kelamin', 'error');
                $(".field-personal-1-jenis_kelamin").css('outline', '1px solid red');
            return false;
        }else{
            $(".field-personal-1-jenis_kelamin").css('outline', '1px solid #ccc');
        }

        if(jenis_identitas.length <= 0){
            swal('Perhatian!', 'Jenis Identitas Tidak Boleh Kosong', 'error');
                $(".field-identitaspersonal-id_jenis_identitas").css('outline', '1px solid red');
            return false;
        }else{
            $(".field-identitaspersonal-id_jenis_identitas").css('outline', '1px solid #ccc');
        }

        if(status_perkawinan.length <= 0){
            swal('Perhatian!', 'Status Perkawinan Tidak Boleh Kosong', 'error');
                $(".field-personal-1-status_perkawinan").css('outline', '1px solid red');
            return false;
        }else{
            $(".field-personal-1-status_perkawinan").css('outline', '1px solid #ccc');
        }

        if(agama.length <= 0){
            swal('Perhatian!', 'Agama Tidak Boleh Kosong', 'error');
                $(".field-personal-1-id_agama").css('outline', '1px solid red');
            return false;
        }else{
            $(".field-personal-1-id_agama").css('outline', '1px solid #ccc');
        }

        if(pendidikan_terakhir.length <= 0){
            swal('Perhatian!', 'Pendidikan Terakhir Tidak Boleh Kosong', 'error');
                $(".field-personal-1-id_pendidikan").css('outline', '1px solid red');
            return false;
        }else{
            $(".field-personal-1-id_pendidikan").css('outline', '1px solid #ccc');
        }

        if(kontak.length <= 0){
            swal('Perhatian!', 'NO. HP Terakhir Tidak Boleh Kosong', 'error');
                $("#kontakpersonal-1-kontak").css('outline', '1px solid red');
            return false;
        }else{
            $("#kontakpersonal-1-kontak").css('outline', '1px solid #ccc');
        }
        
        if(pekerjaan.length <= 0){
            swal('Perhatian!', 'Pendidikan Terakhir Tidak Boleh Kosong', 'error');
                $(".field-personal-1-pekerjaan").css('outline', '1px solid red');
            return false;
        }else{
            $(".field-personal-1-pekerjaan").css('outline', '1px solid #ccc');
        }

        if(alamat.length <= 0){
            swal('Perhatian!', 'Alamat Terakhir Tidak Boleh Kosong', 'error');
                $("#personal-1-alamat_sementara").css('outline', '1px solid red');
            return false;
        }else{
            $("#personal-1-alamat_sementara").css('outline', '1px solid #ccc');
        }
        e.preventDefault();
        $.ajax({
            url: '{{url("psikolog/update-profil-user")}}?ajax=1' ,
            type: "post",
            contentType: false,
            cache: false,
            processData:false,
            data: new FormData(this),
            dataType: "html",
            success: function(data) {
                console.log(data);
                if(data == 'berhasil'){
                    swal("Sukses", "Berahsil Menyimpan", "success");
                    location.reload();
                }
                if(data == 'tidak lengkap'){
                    swal("error!", "Ada form yang belum terisi", "error");
                }
                if(data == 'file_besar'){
                    swal("error!", "max ukuran foto 500 kb", "error");
                }
            }
        });
    }))
</script>