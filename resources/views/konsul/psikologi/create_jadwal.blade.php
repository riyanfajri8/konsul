@extends('konsul.layouts.app')

@section('content')
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

.container {
    max-width: 1400px !important;
}

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" />
<link rel="stylesheet" type="text/css" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">

<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>


<div class="container mt-4"  >
    <div id="myCarousel" class="carousel slide">
        <div calss="row">

            @include('konsul.psikologi.menu')
            <div class="col-xl-9 col-lg-8">
            
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="tabel"
                        style="width:100%">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                    role="tablist">
                                </ul>
                            </div>
                            <div class="m-portlet__head-tools">

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="m-portlet__nav-link btn btn-sm btn-brand m-btn m-btn--pill m-btn--air"
                                                href="{{url('/psikolog/create-kategori')}}"><i
                                                    class="fa fa-arrow-left"></i> Create </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <!-- start head tools portlet -->
                            <!-- end head tools portlet -->
                        </div>
                        <div class="tab-content">
                            <div class="container mt-4" id="laporancuti">
                                @if($errors->any())
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    @if(session('success'))
                                        <div class="alert alert-success">
                                            {!!  session('success')  !!}
                                        </div>
                                    @endif

                                <form id="form-jadwal" class="m-form m-form--fit m-form--label-align-left  "
                                        action="{{url('psikolog/create-kategori')}}"
                                        method="post">
                                        @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    

                                                    <div class="table-responsive">  
                                                        <table class="table table-bordered" id="dynamic_field">  
                                                                <tr>  
                                                                    <td><p>Input Hari</p></td>
                                                                    <td> : </td>
                                                                    <td>
                                                                        <div class="form-group field-identitaspersonal-id_jenis_identitas required">

                                                                            <select id="identitaspersonal-id_jenis_identitas" class="form-control" name="jenis_identitas" aria-required="true">
                                                                                <option value="">Pilih</option>
                                                                                <option value="KTP" selected="">KTP</option>
                                                                                <option value="SIM">SIM</option>
                                                                                <option value="Passport">Passport</option>
                                                                            </select>

                                                                            <div class="help-block"></div>
                                                                        </div>
                                                                    </td>  

                                                                    <td><p>Input Jam</p></td>
                                                                    <td> : </td>
                                                                    <td>
                                                                        <div class="form-group field-identitaspersonal-id_jenis_identitas required">

                                                                        <input type="time" id="appt" name="appt"
                                                                                 required>

                                                                            <div class="help-block"></div>
                                                                        </div>
                                                                    </td>  

                                                                    <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                                                                </tr>  
                                                        </table>  
                                                        <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
    

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
    $.fn.select2.defaults.set("theme", "bootstrap");
        $(".locationMultiple").select2({
            width: null
        })
    </script>
    <script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#form-jadwal').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>
   @endsection
@section('scripts')
@parent

@endsection