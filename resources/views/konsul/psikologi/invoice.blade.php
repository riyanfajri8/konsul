<!doctype html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Invoice - {{ env("APP_NAME") }}</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    .text-center {
        text-align:center;
    }
    .label {
        background-color: #888;
        padding: 2px 5px;
        border-radius: 3px;
        color: #fff;
    }
    .label-danger {
        background-color: #e74c3c;
    }
    .label-success {
        background-color: #27ae60;
    }
    .label-warning {
        background-color: #ff9f43;
    }
    .label-primary {
        background-color: #7367f0;
    }
    </style>
</head>

<body>
    <div id="invoice_box" class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <!-- <img src="{{ asset('images/logo.png') }}" style="width:100%; max-width:150px;"> -->
                            </td>
                            
                            <td>
                                <br>
                                Dibuat {{ Carbon\Carbon::parse($invoice->created_at)->format('d M Y') }}<br>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr class='text-center'>
                            <td>
                                Silahkan lakukan transfer Konsultasi ke <b>BCA</b> <br>
                                Sebesar <b>Rp {{ number_format($invoice->total_konsul,0,'.','.') }} </b> <br>
                                <a href="#">Upload Bukti Transfer</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Konsultasi.<br>
                                Jl. Alamat Perusahaan<br>
                                Tampan Kota Pekanbaru
                            </td>
                            
                            <td>
                                Customer,<br>
                                {{ $invoice->name }}<br>
                                {{ $invoice->email }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Metode Pembayaran
                </td>
                
                <td>
                    Nama
                </td>
            </tr>
            
            <tr class="details">
                <td>
                Transfer
                </td>
                    
                <td>
                    BCA
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Harga
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <b>Konsultasi</b>
                </td>
                
                <td>
                    Rp {{ number_format($invoice->total_konsul) }}
                </td>
            </tr>
            <tr class="total">
                <td></td>
                
                <td>
                   Total: Rp {{ number_format($invoice->total_konsul) }}
                </td>
            </tr>
        </table>

    </div>
    <script src="{{ asset('js/html2canvas.js') }}"></script>
    <script>
        // function download() {
        //     html2canvas(document.querySelector("#invoice_box")).then(canvas => {
        //         document.body.appendChild(canvas)
        //             var img    = canvas.toDataURL("image/png");
        //             document.write('<img src="'+img+'"/>');
        //         });
        // }
    </script>
</body>
</html>
