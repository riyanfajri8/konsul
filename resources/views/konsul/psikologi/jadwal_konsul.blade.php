@extends('konsul.layouts.app')

@section('content')
<?php

use App\JadwalKonsul;


?>
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important;
} */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}

label{
    color: #555;
}

</style>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Tambah Jadwal</h4>
        </div>
        <div class="modal-body">
            <div class="row" >
                <div class="col-lg-6">
                    <label>Waktu Mulai :</label>
                    <input type="time" onchange="jam1()" value="00:00" style="line-height: 18px;" class="form-control" id="jam1" name="appt"required>
                </div>
                <div class="col-lg-6">
                    <label>Waktu Habis :</label>
                    <input onchange="jam2()" type="time" value="00:00" style="line-height: 18px;" class="form-control" id="jam2" name="appt"required>
                </div>
                <input type="text" hidden readonly style="line-height: 18px;" class="form-control" id="hari" name="appt"required>
                
                <div class="col-lg-12 mt-4">
                <label>Total Durasi :</label>
                    <input onchange="durasi()" type="text" readonly style="line-height: 18px;" class="form-control" id="durasi" name="appt"required>
                    <input type="text" hidden readonly style="line-height: 18px;" class="form-control" id="harga" name="appt"required>
                </div>
                <div class="col-lg-6 mt-4">
                    <button onclick="simpanJadwal()" class="btn btn-primary">Simpan Jadwal</button>
                </div>
            </div>
                

        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    
    </div>
</div>
<div class="container mt-4" >
    <div id="myCarousel" class="carousel slide" >
        <div class="row" style="margin-top:150px;">

            @include('konsul.psikologi.menu')
            <div class="col-xl-9 col-lg-8">

                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="tabel"
                        style="width:100%">
                        <div class="m-portlet__head">
                        <h4 class="text-left mt-4"><b>Jadwal Konsultasi</b></h4>
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                    role="tablist">
                                </ul>
                            </div>
                            <div class="m-portlet__head-tools">

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <!-- <li class="m-portlet__nav-item">
                                            <a class="m-portlet__nav-link btn btn-sm btn-brand m-btn m-btn--pill m-btn--air"
                                                href="{{url('/psikolog/create-jadwal')}}"> Create </a>
                                        </li> -->

                                    </ul>
                                </div>
                            </div>
                            <!-- start head tools portlet -->
                            <!-- end head tools portlet -->
                        </div>
                        <div class="tab-content">
                            <div class="container mt-4" id="laporancuti">
                                <div id="jadwal"> </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>



        </div>
    </div>
    

<script>
        
    function jam1(){
        var jam1 = $('#jam1').val();
        var jam2 = $('#jam2').val();
        if(jam1 >= jam2){
            $('#jam2').val(jam1);
        }
        var waktuMulai = jam1,
            waktuSelesai = jam2,
       hours = waktuSelesai.split(':')[0] - waktuMulai.split(':')[0],
          minutes = waktuSelesai.split(':')[1] - waktuMulai.split(':')[1];

            if (jam1+':00' <= "12:00:00" && jam2+':00' >= "13:00:00"){
                a = 0;
            }else {
                a = 0;
            }

          minutes = minutes.toString().length<2?'0'+minutes:minutes;
          minutes = minutes.toString().length<2?'0'+minutes:minutes;
            if(minutes<0){ 
                hours--;
                minutes = 60 + minutes;        
            }
            hours = hours.toString().length<2?'0'+hours:hours;
            var total_hours = parseInt(hours) * parseInt(60000);
            var total_menit = parseInt(minutes) * parseInt(1000);
            $('#harga').val(total_hours + total_menit);
             
            $('#durasi').val(hours-a+ ' Jam ' + minutes+' Menit');
    }
    function jam2(){
        var jam1 = $('#jam1').val();
        var jam2 = $('#jam2').val();
        if(jam1 >= jam2){
            $('#jam2').val(jam1);
        }
        var waktuMulai = jam1,
            waktuSelesai = jam2,
       hours = waktuSelesai.split(':')[0] - waktuMulai.split(':')[0],
          minutes = waktuSelesai.split(':')[1] - waktuMulai.split(':')[1];

            if (jam1+':00' <= "12:00:00" && jam2+':00' >= "13:00:00"){
                a = 0;
            }else {
                a = 0;
            }

          minutes = minutes.toString().length<2?'0'+minutes:minutes;
          minutes = minutes.toString().length<2?'0'+minutes:minutes;
            if(minutes<0){ 
                hours--;
                minutes = 60 + minutes;        
            }
            hours = hours.toString().length<2?'0'+hours:hours;
            var total_hours = parseInt(hours) * parseInt(60000);
            var total_menit = parseInt(minutes) * parseInt(1000);
            $('#harga').val(total_hours + total_menit);
             
            $('#durasi').val(hours-a+ ' Jam ' + minutes+' Menit');
    }
    function tambahJam(id){
        $('#hari').val(id);
    }
    function simpanJadwal(){
        var jam1 = $('#jam1').val();
        var jam2 = $('#jam2').val();
        var hari = $('#hari').val();
        var durasi = $('#durasi').val();
        var harga = $('#harga').val();
        if($('#durasi').val() == '0'){
            alert('isi jam terlebih dengan benar');
            return false;
        }else{
            $.ajax({
                url: '{{url("/psikolog/create-jam")}}/' + jam1 +"/"+hari+"?jam2="+jam2+"&durasi="+durasi+"&harga="+harga,
                type: "GET",
                dataType: "html",
                success: function(data) {
                    // console.log(data);
                    if(data == 'berhasil'){
                        
                        $('#jadwal').load('{{url("/psikolog/ambil-jadwal")}}', function(e) {
                        });
                    }
                    // $('#psikologi-tampil').html(data);
                }
            });
        }
    }

    function delteJadwal(id,hari){
        $.ajax({
            url: '{{url("/psikolog/delete-jam")}}/' + id +"/"+hari,
            type: "GET",
            dataType: "html",
            success: function(data) {
                // console.log(data);
                if(data == 'berhasil'){
                    alert('berhasil di hapus');
                    $('#jadwal').load('{{url("/psikolog/ambil-jadwal")}}', function(e) {});
                }
                // $('#psikologi-tampil').html(data);
            }
        });
    }
    
</script>
@endsection
@section('scripts')
@parent

@endsection