<div class="col-xl-3 col-lg-4">
    <div class="m-portlet m-portlet--full-height  ">
        <div class="m-portlet__body">
            <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                    Your Profile
                </div>
                <div class="m-card-profile__pic">
                    <label onclick="gantifoto()"  data-toggle="modal" data-target="#myModal">
                        <div class="m-card-profile__pic-wrapper" style="border-radius: 0px;">
                            <div class="gambar">
                                <?php
                                    if(!empty($psikolog->foto)){
                                        echo '<img src="'.asset($psikolog->foto).'" style="border-radius: 100px; width:160px; height:160px" alt="" />';
                                    }else{
                                ?>

                                <?php } ?>
                                <div class="overlay">
                                    <div class="text">Ganti Foto</div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <div class="m-card-profile__details">
                    <span class="m-card-profile__name m--font-danger"></span>
                    <span class="m-card m--font-primary"
                        style="font-size:15px;">{{ Auth::user()->name }}</span>
                    <hr>
                </div>
            </div>

            <div id="m_ver_menu"
                class="menuprofil m-aside-menu m-aside-menu--skin-light m-aside-menu--submenu-skin-light m-scroller ps ps--active-y"
                data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow">
                    <li data-menuy="m-beranda" class="m-beranda m-menu__item" onclick="tes(this)">
                        <a href="{{url('/psikolog/profil')}}"
                            class="m-menu__link m-menu__toggle menu-dalam">
                            <i class="m-menu__link-icon fas fa-home"></i>
                            <span class="m-menu__link-text"><span>Beranda</span>
                            </span></a>
                    </li>

                    <li data-menux="m-profil" class="m-profil m-menu__item m-menu__item--submenu"
                        aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
                            class="m-menu__link m-menu__toggle menu-dalam"><i
                                class="m-menu__link-icon fas fa-id-card"></i>
                            <span class="m-menu__link-text">Profil</span><i
                                class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li data-menuy="m-profil-1" class="m-menu__item m-profil-1"
                                    onclick="tes(this)">
                                    <a href="{{url('/psikolog/data-profil')}}"
                                        class="m-menu__link">
                                        <span class="m-menu__link-text">
                                            <span>Data Profil</span></span>
                                    </a>
                                </li>
                                <li data-menuy="m-profil-2" class="m-menu__item m-profil-2"
                                    onclick="tes(this)">
                                    <a href="{{url('/psikolog/ubah-password')}}"
                                        class="m-menu__link">
                                        <span class="m-menu__link-text"><span>Rubah Password
                                                </span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li data-menuy="m-beranda" class="m-beranda m-menu__item" onclick="tes(this)">
                        <a href="{{url('/psikolog/kategori')}}"
                            class="m-menu__link m-menu__toggle menu-dalam">
                            <i class="m-menu__link-icon fa fa-list-alt"></i>
                            <span class="m-menu__link-text"><span>Pilih Kategori</span>
                            </span></a>
                    </li>

                    <li data-menuy="m-beranda" class="m-beranda m-menu__item" onclick="tes(this)">
                        <a href="{{url('/psikolog/jadwal-konsul')}}"
                            class="m-menu__link m-menu__toggle menu-dalam">
                            <i class="m-menu__link-icon fas fa-clock"></i>
                            <span class="m-menu__link-text"><span>Jadwal Konsul</span>
                            </span></a>
                    </li>

                    <li data-menuy="m-beranda" class="m-beranda m-menu__item" onclick="tes(this)">
                        <a href="{{url('/psikolog/data_klien')}}"
                            class="m-menu__link m-menu__toggle menu-dalam">
                            <i class="m-menu__link-icon fas fa-users"></i>
                            <span class="m-menu__link-text"><span>Data Klien</span>
                            </span></a>
                    </li>

                    <!-- fitur ini hanya utk koordinator, 5 adalah id_job_level koordinator -->
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Ganti Foto</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <form id="form-pendaftaran" class="m-form m-form--fit m-form--label-align-left  "
        action="{{url('psikolog/upload-img')}}"
        files="true"
        enctype="multipart/form-data"
        method="post">
        @csrf
        <div class="col-md-12 form-group">
            {!! Form::label('img', 'Upload Foto Profil ( Aspek Ratio 1 : 1 atau persegi. Ukuran Maks 1 MB)', ['class' => 'control-label']) !!}
            <br>
            <img id="uploadPreview1" src=""  style="width: 150px;" />
            <input class="form-control" id="uploadImage1" type="file" name="img" onchange="PreviewImage1();" style="line-height: 2.5;" />
            
            <p class="help-block"></p>
            @if($errors->has('img'))
                <p class="help-block">
                    {{ $errors->first('img_name') }}
                </p>
            @endif
        </div>

        <div class="row text-center">
                <div class="col-md-4 offset-md-4">
                    <button type="submit" class="btn btn-primary">Tukar Foto</button>
                </div>
            </div>
    </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<script>
    function PreviewImage1() {  
        var oFReader = new FileReader();  
        oFReader.readAsDataURL(document.getElementById("uploadImage1").files[0]);  
        oFReader.onload = function (oFREvent) {  
            document.getElementById("uploadPreview1").src = oFREvent.target.result;  
        };  
    };
</script>