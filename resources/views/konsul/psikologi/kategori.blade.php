@extends('konsul.layouts.app')

@section('content')
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important;
} */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
<div class="container" >
    <div id="myCarousel" class="carousel slide" >
        <div class="row" style="margin-top:150px;">

            @include('konsul.psikologi.menu')
            <div class="col-xl-9 col-lg-8">
            
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="tabel"
                        style="width:100%">
                        <div class="m-portlet__head">
                        <h4 class="text-left mt-4"><b>Daftar Kategori Keahlian</b></h4>
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                    role="tablist">
                                </ul>
                            </div>
                            <div class="m-portlet__head-tools">

                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <li class="m-portlet__nav-item">
                                            <a class="m-portlet__nav-link btn btn-sm btn-brand m-btn m-btn--pill m-btn--air"
                                                href="{{url('/psikolog/create-kategori')}}"><i
                                                    class="fa fa-plus"></i> Tambah Kategori Keahlian </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <!-- start head tools portlet -->
                            <!-- end head tools portlet -->
                        </div>
                        <div class="tab-content">
                            <div class="container mt-4" id="laporancuti">
                                <table class="table display nowrap" id="html_table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nama Kategori</th>
                                            <th>Sub Kategori</th>
                                            <!-- <th>Keterangan</th> -->
                                            <!-- <th>Harga</th> -->
                                            <th>Pilihan</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($kategori as $kategori1)
                                            <tr>
                                                <td>{{$kategori1->nama_konsul}}</td>
                                                <td>{{$kategori1->nama_sub_kategori}}</td>
                                                <!-- <td>{{$kategori1->keterangan}}</td> -->
                                                <!-- <td>{{$kategori1->harga}}</td> -->
                                                <td>
                                                <a href="{{url('psikolog/delete',[$kategori1->id_psikologi_kategori_konsul])}}" class="btn btn-outline-danger active">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @endsection
@section('scripts')
@parent

@endsection