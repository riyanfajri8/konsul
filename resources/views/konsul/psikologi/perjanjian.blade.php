@extends('konsul.layouts.app2')

<style>
.datepicker table {
    color: #26262b;
}
</style>
@section('content')
<section class="daftar-psikolog bg-basic">
   <div class="container">
      <div class="row justify-content-center pb-2">
          <div class="col-md-12 heading-section text-center">
            <span class="subheading">Tentukan jadwal dan media konsultasi kamu</span>
            <h2 class="mb-1">Langkah 3 : Buat Janji Konsultasi</h2>
          </div>
        </div>
      </div>
      <div class="container">
      <section id="step">
      <div class="card2 card-timeline px-2 border-none">
        <div class="col-md-8 offset-md-2 justify-content-center">
        <ul class="bs4-order-tracking">
          <li class="step active">
              <div style="width: 38px; height: 38px;"><i style="
    padding-top: 9px;" class="fas fa-clipboard-list"></i></div> Cek Layanan Konsultasi
          </li>
          <li class="step active">
              <div style="width: 38px; height: 38px;"><i style="
    padding-top: 9px;" class="fas fa-user-md"></i></div> Cari Psikolog
          </li>
          <li class="step active">
              <div style="width: 38px; height: 38px;"><i style="
    padding-top: 9px;" class="far fa-calendar-check"></i></div> Buat Janji Konsultasi
          </li>
        </ul>
        </div>
      </div>
    </section>
    </div>
<section class="ftco-section bg-basic" style="padding: 0;"> 
<form id="save-form" action="{{url('save-perjanjian',[$name])}}" method="GET"
                                class="form-horizontal post-search ">
      <div class="container-xl">
        <div class="row gy-4">
          <div class="col-md-12">
            <div class="carousel-inner">
              <div class="item carousel-item active">
                <div class="row">
                  <div class="col-lg-3">
                    <div class="member" style="margin-bottom: 0;">
                      <div class="member-img rounded-circle">
                        <img src="{{asset($psikologi->foto)}}" class="img-fluid" alt="">
                      </div>
                      <div class="member-info">
                        <h4>{{$psikologi->nama}}</h4>
                        <span>Psikolog</span>
                          <ul class="mt-3 text-left" style="line-height: normal;">
                            <li><i class="icofont-badge"></i> SIPP</li>
                              <p>{{$psikologi->sip}}</p>
                            <li><i class="icofont-bag"></i> Pengalaman</li>
                              <p>5 tahun</p>
                            <li><i class="icofont-graduate"></i> Pendidikan</li>
                              <p>{{$psikologi->universitas}}</p>
                          </ul>
                      </div>
                        <button onclick="bukaProfil(<?= $psikologi->id_psikologi ?>)" type="button" data-toggle="modal" data-target="#modal-profil"  class="btn btn-success btn-block mt-4 mb-2"><i class="icofont-eye-alt"></i> Profil Lengkap</button>
                      </div>
                    </div>
                    <div class="col-lg-9">
                      <div class="member">
                        <div class="member-info">
                          <div class="row">
                            <div class="col">
                              <table class="table" style="font-size: 14px;">
                                <tbody><tr>
                                    <td>Nama Psikolog</td>
                                    <td>:</td>
                                    <td>
                                        {{$psikologi->nama}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kategori</td>
                                    <td>:</td>
                                    <td>
                                    
                                      <select onchange="kategori()" id="inputState" class="form-control">
                                        <option selected="">pilih salah satu</option>
                                        @foreach($kategori as $kategori)
                                            @if(!empty($kategoripilih))
                                                @if($kategoripilih->id_kategori_konsul == $kategori->id_kategori_konsul)
                                                    <option selected value="{{$kategori->id_kategori_konsul}}">{{$kategori->nama_konsul}}</option>
                                                @else
                                                  <option value="{{$kategori->id_kategori_konsul}}">{{$kategori->nama_konsul}}</option>
                                                @endif
                                            @else
                                                <option value="{{$kategori->id_kategori_konsul}}">{{$kategori->nama_konsul}}</option>
                                            @endif
                                        @endforeach
                                      </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sub Kategori</td>
                                    <td>:</td>
                                    <td id="sub_kategori1">
                                      
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gejala - gejala</td>
                                    <td>:</td>
                                    <td>
                                        <textarea id="masalah" name="masalah" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$masalah}}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tanggal Konsultasi</td>
                                    <td>:</td>
                                    <td>
                                        <input id="datepicker" onchange="pilihTanggal()"
                                                    style="line-height: 19px;" name="date" class="form-control">

                                          <input id="tanggal" name="id_psikologi" style="line-height: 19px;"
                                                    hidden class="form-control" value="{{$id_psikologi}}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jam Konsultasi</td>
                                    <td>:</td>
                                    <td>    
                                      <div id="jam-konsul"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Durasi</td>
                                    <td>:</td>
                                    <td>    
                                      <div id="durasi"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Media Konsultasi</td>
                                    <td>:</td>
                                    <td>
                                      <div class="row">
                                      @foreach($media as $media)
                                      <div class="col-md-6">
                                          <div class="funkyradio">
                                            <div class="funkyradio-danger">
                                                <input type="radio" value="{{$media->id_media}}" name="media" id="radio{{$media->id_media}}">
                                                <label class="labelradio" for="radio{{$media->id_media}}">{{$media->nama_media}}</label>
                                            </div>
                                          </div>
                                      </div>
                                      @endforeach
                                        
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                  <td colspan="3" text-center>
                                  <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="inline2" name="sop">
                                    <label class="label2 custom-control-label" for="inline2" style="line-height:1.5; font-size:12px;">
                                      Dengan ini saya menyetujui semua peraturan yang ada pada PsyQosul. Bahwa benar jadwal yang telah saya input sesuai dengan keinginan saya dan apabila saya tidak bisa mengikuti konsultasi sesuai jadwal yang sudah dipilih maka itu bukan tanggung jawab PsyQonsul.
                                    </label>
                                  </div>
                                  </td>
                                </tr>
                                <hr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- button order konsul -->
          <div class="container mb-5 mt-4">
            <div class="row justify-content-center pb-2">
                <div class="col-md-3 heading-section text-center">
                <label onclick="buatJanji()" class="btn btn-danger" data-toggle="modal"
                                    data-target="#myModal"  class="btn btn-lg btn-primary btn-block" style="font-size: 16px;">Order PsyQonsul 
                   <i class="fas fa-hands-helping fa-lg"></i></label> 
                </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    </section>
      <?php
            if(empty($biodata->jenis_kelamin) || empty($biodata->status_perkawinan || empty($biodata->pekerjaan))){
        ?>
    <form id="payment-form" method="get" action="Payment">
      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      <input type="hidden" name="result_type" id="result-type" value=""></div>
      <input type="hidden" name="result_data" id="result-data" value=""></div>
    </form>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Isi Data Diri Untuk Melanjutkan</h4>
            </div>
            <div class="modal-body">
                @include('konsul.psikologi.updateProfil')

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<?php
    }
?>

<!-- Modal -->
<div id="modal-profil" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profil Psikolog</h4>
            </div>
            <div class="modal-body">
                <div id="profilSingkat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection
<script type="text/javascript"
	      src="https://app.sandbox.midtrans.com/snap/snap.js"
	      data-client-key="SB-Mid-client-gPB5wXO09A70jve3"></script>

<script>

function kategori(){
  var id_kategori = $('#inputState').val();
  var id = 0;
  $('#sub_kategori1').load('{{url("/cari-psikologi")}}/'+id+"?id_kategori="+id_kategori+"&perjanjian=1", function(e) {});
}

function bukaProfil(id){
        $('#profilSingkat').load('{{url("/profil-psikologi")}}/'+id, function(e) {});
    }
function buatJanji() {
    '<?php
            if(empty($biodata->jenis_kelamin) || empty($biodata->status_perkawinan) || empty($biodata->pekerjaan) ){
        ?>'

    return false;
    '<?php
            }
        ?>'
    var masalah = $('#masalah').val();
    var tanggal = $('#datepicker').val()
    var jamJadwalDipilih = $('#jamJadwalDipilih').val();
    var jamDurasi = $('#jamDurasi').val();

    if($('input[name=sop]:checked').val() == 'on'){
      $("#inline2").css('outline', '1px solid #ccc');
    }else{
        swal('Perhatian!', 'ceklis dulu', 'error');
        $("#inline2").css('outline', '1px solid red');
        return false;
    }
            
    if(masalah.length <= 0){
      $("#masalah").css('outline', '1px solid red');
        swal('Perhatian!', 'Ketik Masalah Anda', 'error');
        return false;
    }else{
        $("#masalah").css('outline', '1px solid #ccc');
    }

    if(jamJadwalDipilih.length <= 0){
        swal('Perhatian!', 'Pilih Jam Masih Kosong', 'error');
            $("#jamJadwalDipilih").css('outline', '1px solid red');
        return false;
    }else{
        $("#jamJadwalDipilih").css('outline', '1px solid #ccc');
    }
    
    if(jamDurasi.length <= 0){
        swal('Perhatian!', 'Pilih durasi Masih Kosong', 'error');
            $("#jamDurasi").css('outline', '1px solid red');
        return false;
    }else{
        $("#jamDurasi").css('outline', '1px solid #ccc');
    }
    if(tanggal.length <= 0){
        swal('Perhatian!', 'Isi tanggal masih kosong', 'error');
            $("#datepicker").css('outline', '1px solid red');
        return false;
    }else{
        $("#datepicker").css('outline', '1px solid #ccc');
    }

    if($('input[name=media]:checked').length <= 0){
        swal('Perhatian!', 'Ceklis salah satu media', 'error');
            $(".mediar").css('outline', '1px solid red');
        return false;
    }else{
        $(".mediar").css('outline', '1px solid #ccc');
    }
    if($('.sub_kategori').val() == ''){
        swal('Perhatian!', 'Pilih Layanan', 'error');
            $(".sub_kategori").css('outline', '1px solid red');
        return false;
    }else{
        $(".sub_kategori").css('outline', '1px solid #ccc');
        var sub_kategori = $('.sub_kategori').val();
    }
    
    $.ajax({
        url: '{{url("save-perjanjian")}}/'+sub_kategori,
        type: "GET",
        data: $("#save-form").serialize(),
        dataType: "html",
        success: function(data) {
                swal("Sukses", "Berhasil Menyimpan", "success");
                location.href ="{{url('/sukses')}}/"+data+"/proses";
        }
    });
}

function pilihTanggal() {
    var tanggal = $('#datepicker').val();
    $.ajax({
        url: '{{url("pilih-tanggal-order")}}/' + tanggal + "/{{$psikologi->id_psikologi}}",
        type: "GET",
        dataType: "html",
        success: function(data) {
            console.log(data);
            $('#jam-konsul').html(data);
            $('#durasi').html('pilih jam terlebih dahulu');
        }
    });
}
</script>
@section('scripts')
@parent

@endsection