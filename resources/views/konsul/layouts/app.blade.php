<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PsyQonsul - Online Konseling Psikologi</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('Medicio')}}/assets/img/favicon.png" rel="icon">
  <link href="{{url('Medicio')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/vendor/animate.css/animate.css">
  <link href="{{url('Medicio')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">

  <!-- hotqonsul bootstrap5 -->
  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

  <!-- Template Main CSS File -->
  <link href="{{url('Medicio')}}/assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/css/style2.css">
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/css/custom.css">
  <link rel="stylesheet" href="{{url('konsul')}}/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="{{url('konsul')}}/css/jquery.timepicker.css">
  <link href="{{url('Medicio')}}/assets/css/style.bundle.css" rel="stylesheet">
  <!-- <link href="{{url('metronic')}}/assets/demo/demo9/base/style.bundle.css" rel="stylesheet"> -->
  
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body class="" style="color: #fff;">
  <button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>
      @include('konsul.partials.menu')
      <div class="tab-content">
        <div class="">
          @yield("content")
          </div>
      </div>
    
    <!-- Vendor JS Files -->
    
    <!-- <script src="{{url('js')}}/select2.full.min.js"></script> -->
    <script src="{{url('metronic')}}/assets/vendors/base/vendors.bundle.js"></script>
    <!-- <script src="{{url('default')}}/base/scripts.bundle.js" type="text/javascript"></script> -->
    <!-- <script src="{{url('BizLand')}}/assets/vendor/jquery/jquery.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/php-email-form/validate.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/counterup/counterup.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/venobox/venobox.min.js"></script>
    <script src="{{url('BizLand')}}/assets/vendor/aos/aos.js"></script> -->
    <script src="{{url('metronic')}}/assets/demo/demo9/base/scripts.bundle.js"></script>
    
    <script>
        $('#jadwal').load('{{url("/psikolog/ambil-jadwal")}}', function(e) {});
        $('#masalah').val(Cookies.get('masalah'));
        $('#kategori').load('{{url("/kategori")}}', function(e) {});
        <?php if(!empty($name_konsul)){ ?>
            $('#pasikologi-cari').load('{{url("/cari-psikologi",[$name_konsul])}}', function(e) {});
        <?php } ?>
        $(document).ready(function() {
            pilihTanggal();

            $( '.uang' ).mask('000.000.000', {reverse: true});
        });
        pendidikanPilih();
        
    </script>
    

    <!-- Template Main JS File -->
    <!-- <script src="{{url('BizLand')}}/assets/js/main.js"></script> -->
    @yield('scripts')
</body>

</html>