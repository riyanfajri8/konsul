<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PsyQonsul - Online Konseling Psikologi</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('Medicio')}}/assets/img/favicon.png" rel="icon">
  <link href="{{url('Medicio')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/vendor/animate.css/animate.css">

  <link href="{{url('Medicio')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">


  <!-- Template Main CSS File -->
  <link href="{{url('Medicio')}}/assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/css/style2.css">
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/css/custom.css">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css'>

  <script>
$(document).ready(function(){
  $(".wish-icon i").click(function(){
    $(this).toggleClass("fa-heart fa-heart-o");
  });
}); 
</script>
</head>

<body>
    <button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>
    @include('konsul.partials.menu')
    <div class="tab-content">
		  <div class="">
        @yield("content")
        </div>
    </div>
    
 <!-- footer  -->
<footer class="ftco-footer ftco-no-pt">
  <div class="container">
  <div class="row mb-5">
    <div class="col-md pt-5">
      <div class="ftco-footer-widget pt-md-5 mb-4">
        <h2 class="ftco-heading-2">Tentang PsyQonsul</h2>
        <p>PsyQonsul adalah Layanan Konsultasi Online yang merupakan bagian dari Persona Quality</p>
        <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
          <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
          <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
          <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
      </ul>
  </div>
  </div>
  <div class="col-md pt-5">
  <div class="ftco-footer-widget pt-md-5 mb-4 ml-md-5">
    <h2 class="ftco-heading-2">Psikologi</h2>
    <ul class="list-unstyled">
      <li><a href="#" class="py-2 d-block">Info Jiwa Sehat</a></li>
      <li><a href="#" class="py-2 d-block">Kamus Psikolog</a></li>
      <li><a href="#" class="py-2 d-block">Energizer Game</a></li>
      <li><a href="#" class="py-2 d-block">Cendera Hati</a></li>
  </ul>
  </div>
  </div>
  <div class="col-md pt-5">
  <div class="ftco-footer-widget pt-md-5 mb-4">
    <h2 class="ftco-heading-2">Layanan</h2>
    <ul class="list-unstyled">
      <li><a href="#" class="py-2 d-block">Cek Layanan Konsultasi</a></li>
      <li><a href="#" class="py-2 d-block">Daftar Psikolog</a></li>
      <li><a href="#" class="py-2 d-block">Buat Janji</a></li>
  </ul>
  </div>
  </div>
  <div class="col-md pt-5">
  <div class="ftco-footer-widget pt-md-5 mb-4">
     <h2 class="ftco-heading-2">Informasi Kontak</h2>
     <div class="block-23 mb-3">
       <ul>
         <li><span class="icon fa fa-map-marker"></span><span class="text">Jl. Parit Indah No.09 Perkantoran Grand sudirman Blok B-7 Pekanbaru</span></li>
         <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">(0761) 848844, 37421, 0851.000.17500</span></a></li>
         <li><a href="#"><span class="icon fa fa-paper-plane"></span><span class="text">persona_quality@yahoo.com, admin@personaquality.com</span></a></li>
     </ul>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-12 text-center">
  <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <a href="https://colorlib.com" target="_blank">Persona Quality</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
  </div>
  </div>
  </div>
</footer>
    <!-- Vendor JS Files -->
    <!-- Vendor JS Files -->
  <script src="{{url('Medicio')}}/assets/vendor/jquery/jquery.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/php-email-form/validate.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/counterup/counterup.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/venobox/venobox.min.js"></script>
  <script src="{{url('Medicio')}}/assets/vendor/aos/aos.js"></script>
  <script src="{{url('Medicio')}}/assets/js/main.js"></script>

<script type="text/javascript">
      $(function() {
        $( "#datepicker" ).datepicker({
          format: 'dd-mm-yyyy'
        });
      });
    </script>
    <script>
   
        $('#jadwal').load('{{url("/psikolog/ambil-jadwal")}}', function(e) {});
        $('#kategori').load('{{url("/kategori")}}', function(e) {});
        $('#kategori1').load('{{url("/kategori")}}?k=1', function(e) {});
        
        <?php if(!empty($name_konsul)){ ?>
            $('#pasikologi-cari').load('{{url("/cari-psikologi",[$name_konsul])}}', function(e) {});
        <?php } ?>
        
        
        $(document).ready(function() {
            pilihTanggal();

            $( '.uang' ).mask('000.000.000', {reverse: true});

        });
        $('#masalah').val(Cookies.get('masalah'));
    </script>

    <!-- Template Main JS File -->
  <!-- Template Main JS File -->
    @yield('scripts')
</body>

</html>