<?php
use App\Media;
use App\KonsulLansung;
//use DB;
use Illuminate\Http\Request;
use App\Biodata;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PsyQonsul - Online Konseling Psikologi</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('Medicio')}}/assets/img/favicon.png" rel="icon">
  <link href="{{url('Medicio')}}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/vendor/animate.css/animate.css">
  <link href="{{url('Medicio')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="{{url('Medicio')}}/assets/vendor/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">
  <script src="{{url('socket_io')}}/socket.io.js"></script>

  <!-- animated icon fontawesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.0.8/font-awesome-animation.min.css"/>

  <!-- hotqonsul bootstrap5 -->
  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"> -->
  <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Template Main CSS File -->
  <link href="{{url('Medicio')}}/assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/css/style2.css">
  <link rel="stylesheet" href="{{url('Medicio')}}/assets/css/custom.css">
  <link rel="stylesheet" href="{{url('konsul')}}/css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="{{url('konsul')}}/css/jquery.timepicker.css">

  <script type="text/javascript"
	      src="https://app.midtrans.com/snap/snap.js"
	      data-client-key="Mid-client-jZV6MFoFsI0MQ_lJ"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css'> -->

  <script>
$(document).ready(function(){
  $(".wish-icon i").click(function(){
    $(this).toggleClass("fa-heart fa-heart-o");
  });
}); 
</script>
</head>

<body>
    <!-- <button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button> -->
    @include('konsul.partials.menu')
    <div class="tab-content">
		  <div class="">
        @yield("content")
        </div>
    </div>


    <!-- hotqonsul -->
    <!-- <section id="#hotqonsul">
      <div class="row">
        <div class="col-md-6">
        <input type="checkbox" id="check"> 
        <label class="chat-btn" for="check"><p>HotQonsul  </p><i class="icofont-telephone"></i><i class="icofont-close"></i>
        </label>
        <div class="wrapper" style="width:30%;">
          <div class="header">
            <h6>Let's Chat - Online</h6>
          </div>
          <div class="text-center p-2"> <span>Please fill out the form to start chat!</span> </div>
          <div class="chat-form"> <input type="text" class="form-control" placeholder="Name"> <input type="text"
              class="form-control" placeholder="Email"> <textarea class="form-control"
              placeholder="Your Text Message"></textarea> <button class="btn btn-success btn-block">Submit</button> </div>
        </div>
        </div>
      </div>
    </section> -->

@if(empty(Auth::user()->id))
<form id="payment-form" method="GET" action="{{url('save-perjanjian/konsul_lansung')}}">
    <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
    <input type="hidden" name="result_type" id="result-type" value=""></div>
    <input type="hidden" name="result_data" id="result-data" value=""></div>
</from>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Register HotQonsul</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        
      <div class="login-wrap2 p-4 mt-3">
        <form id="dataForm">
          <?php $media = Media::where('status','Aktif')->get(); ?>
            {{ csrf_field() }}
              <div class="form-group">
                  <label class="label" for="name">Nama Lengkap</label>
                  <input type="text" id="inputName" name="name" class="form-control" required placeholder="">
              </div>
              <div class="form-group">
                  <label class="label" for="email">No.Handphone / WA</label>
                  <input type="number"  class="form-control"  required name="no_telp">
              </div>
              <div class="form-group">
                <label class="label" for="email">Email</label>
                  <input name="email" type="text"
                      class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                      autofocus placeholder="" value="{{ old('email', null) }}">
                      @if($errors->has('email'))
                      <div class="invalid-feedback">
                          {{ $errors->first('email') }}
                      </div>
                      @endif
              </div>
              <div class="form-group">
                    <label class="">Tanggal Lahir:</label>
                    <input name="tanggal_lahir" type="date" class="form-control ">
              </div>
              <div class="form-group">
                  <label>Jenis Kelamin:</label>
                      <select id="personal-1-jenis_kelamin" class="form-control" name="jenis_kelamin">
                          <option value="">Pilih</option>
                          <option value="Laki-laki">Laki-laki
                          </option>
                          <option value="Perempuan">Perempuan
                          </option>
                      </select>
              </div>
              <div class="form-group">
                    <label class="">Status Perkawinan:</label>
                        <select id="personal-1-status_perkawinan" class="form-control" name="status_perkawinan">
                            <option value="">Pilih</option>
                            <option value="Belum Menikah">Belum Menikah</option>
                            <option value="Menikah" >Menikah </option>
                        </select>
              </div>
              <div class="form-group">
                    <label>Pendidikan Terakhir:</label>
                        <select id="personal-1-id_pendidikan" class="form-control" name="pendidikan_terakhir">
                            <option value="">Pilih</option>
                            <option value="Belum Sekolah">Belum Sekolah</option>
                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                            <option value="TK">TK</option>
                            <option value="SD">SD</option>
                            <option value="SLTP">SLTP</option>
                            <option value="SLTA">SLTA</option>
                            <option value="D1">D1</option>
                            <option value="D3">D3</option>
                            <option value="D4">D4</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                            <option value="dr">dr</option>
                            <option value="Belum Tamat Sekolah">Belum Tamat Sekolah</option>
                        </select>
              </div>
              <div class="form-group">
                    <label class="">Pekerjaan:</label>
                        <select id="personal-1-pekerjaan" class="form-control" name="pekerjaan">
                            <option value="">Pilih Pekerjaan</option>
                            <option value="Akademik/Pengajar">Akademik/Pengajar</option>
                            <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                            <option value="Karyawan BUMN">Karyawan BUMN</option>
                            <option value="Karyawan Swasta" data-select2-id="6">Karyawan Swasta</option>
                            <option value="Lain-lain">Lain-lain</option>
                            <optionvalue="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS) </option>
                            <option value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>
                            <option value="Pemerintahan / Militer">Pemerintahan / Militer</option>
                            <option value="Pensiunan">Pensiunan</option>
                            <option value="Petani">Petani</option>
                            <option value="Profesional ( Dokter, Pengacara, Dll )">Profesional ( Dokter, Pengacara, Dll )</option>
                            <option value="Tidak Bekerja">Tidak Bekerja</option>
                            <option value="Wiraswasta">Wiraswasta</option>
                        </select>
              </div>
              <div class="form-group">
                  <label class="label" for="email">Kota tinggal saat ini</label>
                  <input type="text"  class="form-control"  required name="alamat">
              </div>
              <div class="form-group mt-4">
                <label class="" style="margin-left: -5px;">Pilih Media</label>
                
              </div>
              @foreach($media as $media)
                    <div class="funkyradio">
                      <div class="funkyradio-danger">
                          <input type="radio" value="{{$media->id_media}}" name="media" id="radio{{$media->id_media}}">
                          <label class="labelradio" style="color: #5e5858;" for="radio{{$media->id_media}}">{{$media->nama_media}}</label>
                      </div>
                    </div>
              @endforeach

              <div class="form-group mt-4">
                <label class="" style="margin-left: -5px;">Pilih Durasi</label>
              </div>
              <div class="funkyradio">
                <div class="funkyradio-danger">
                    <input type="radio" value="1" name="durasi" id="radio1jam">
                    <label class="labelradio" style="color: #5e5858;" for="radio1jam">1 Jam</label>
                </div>
              </div>
              
              <div class="funkyradio">
                <div class="funkyradio-danger">
                    <input type="radio" value="2" name="durasi" id="radio2jam">
                    <label class="labelradio" style="color: #5e5858;" for="radio2jam">1.5 Jam</label>
                </div>
              </div>

              <div class="funkyradio">
                <div class="funkyradio-danger">
                    <input type="radio" value="2.5" name="durasi" id="radio2.5jam">
                    <label class="labelradio" style="color: #5e5858;" for="radio2.5jam">2 Jam</label>
                </div>
              </div>
              
              <div class="modal-footer d-flex justify-content-center">
                <label onclick="submitLansung()" class="btn btn-primary">Buat Pesanan HotQonsul</label>
              </div>
        </form>    
      </div>
    </div>
      
    </div>
  </div>
</div>
<script>
function submitLansung(){
  
  var media = $('input[name=media]:checked').val();
  var d = $("form").serialize();
    $.ajax({
        url: "{{url('/save-perjanjian/konsul_lansung')}}?akun=1&media="+media,
        type: "GET",
        data: d,
        dataType: "html",
        success: function(data) {
          var resultType = document.getElementById('result-type');
          var resultData = document.getElementById('result-data');
          function changeResult(type,data){
              $("#result-type").val(type);
              $("#result-data").val(JSON.stringify(data));
              //resultType.innerHTML = type;
              //resultData.innerHTML = JSON.stringify(data);
          }
          snap.pay(data, {
              
              onSuccess: function(result){
              changeResult('success', result);
              console.log(result.status_message);
              console.log(result);
              $("#payment-form").submit();
              },
              onPending: function(result){
              changeResult('pending', result);
              console.log(result.status_message);
              $("#payment-form").submit();
              },
              onError: function(result){
              changeResult('error', result);
              console.log(result.status_message);
              $("#payment-form").submit();
              }
          });
            // console.log(data);
            // $('#durasi').html(data);
        }
    });
    return false;
    
}
</script>
@else
<form id="payment-form" method="GET" action="{{url('save-perjanjian/konsul_lansung')}}">
    <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
    <input type="hidden" name="result_type" id="result-type" value=""></div>
    <input type="hidden" name="result_data" id="result-data" value=""></div>
</from>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Register HotQonsul</h4>
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
      
      <div class="login-wrap2 p-4 mt-3">
        <form id="dataKonsulLansung" action="{{url('/save-perjanjian/konsul_lansung')}}" method="GET" files="true" enctype="multipart/form-data">
        <?php $media = Media::where('status','Aktif')->get(); ?>
            {{ csrf_field() }}
            <h5>Pilih Media</h5>
            <input hidden name="lansung" value=1>
              @foreach($media as $media)
                    <div class="funkyradio">
                      <div class="funkyradio-danger">
                          <input type="radio" checked value="{{$media->id_media}}" name="media" id="radio{{$media->id_media}}">
                          <label class="labelradio" style="color: #5e5858;" for="radio{{$media->id_media}}">{{$media->nama_media}}</label>
                      </div>
                    </div>
              @endforeach
              
              <div class="form-group mt-4">
                <label class="" style="margin-left: -5px;">Pilih Durasi</label>
              </div>
              <div class="funkyradio">
                <div class="funkyradio-danger">
                    <input type="radio" value="1" name="durasi" id="radio1jam">
                    <label class="labelradio" style="color: #5e5858;" for="radio1jam">1 Jam</label>
                </div>
              </div>
              <div class="funkyradio">
                <div class="funkyradio-danger">
                    <input type="radio" value="2" name="durasi" id="radio2jam">
                    <label class="labelradio" style="color: #5e5858;" for="radio2jam">1.5 Jam</label>
                </div>
              </div>
              <div class="funkyradio">
                <div class="funkyradio-danger">
                    <input type="radio" value="2.5" name="durasi" id="radio2.5jam">
                    <label class="labelradio" style="color: #5e5858;" for="radio2.5jam">2 Jam</label>
                </div>
              </div>
              
              <div class="modal-footer d-flex justify-content-center">
                <label onclick="submitLansung()" class="btn btn-primary">Buat Pesanan HotQonsul</label>
              </div>
        </form>    
      </div>
    </div>
      
    </div>
  </div>
</div>
<?php
 
  
?>
<script>

function submitLansung(){
  
  var media = $('input[name=media]:checked').val();
  var durasi = $('input[name=durasi]:checked').val();
    $.ajax({
        url: "{{url('/save-perjanjian/konsul_lansung')}}?media="+media+"&durasi="+durasi,
        type: "GET",
        data: $("#dataKonsulLansung").serialize(),
        dataType: "html",
        success: function(data) {
          var resultType = document.getElementById('result-type');
          var resultData = document.getElementById('result-data');
          function changeResult(type,data){
              $("#result-type").val(type);
              $("#result-data").val(JSON.stringify(data));
              //resultType.innerHTML = type;
              //resultData.innerHTML = JSON.stringify(data);
              
          }
          snap.pay(data, {
                  onSuccess: function(result){
                  changeResult('success', result);
                  console.log(result.status_message);
                  console.log(result);
                  $("#payment-form").submit();
                  },
                  onPending: function(result){
                  changeResult('pending', result);
                  console.log(result.status_message);
                  $("#payment-form").submit();
                  },
                  onError: function(result){
                  changeResult('error', result);
                  console.log(result.status_message);
                  $("#payment-form").submit();
                  }
              });
          
            // console.log(data);
            // $('#durasi').html(data);
        }
    });
    return false;
    
}

</script>
@endif

<div class="text-center">
  <a href="" class="chat-btn" data-toggle="modal" data-target="#modalLoginForm">
    <p>HotQonsul  </p><i class="icofont-telephone faa-tada animated"></i>
  </a>
</div>
  
    
 <!-- footer  -->
<footer class="ftco-footer ftco-no-pt">
  <div class="container">
  <div class="row mb-5">
    <div class="col-md pt-5" style="position:unset;">
      <div class="ftco-footer-widget pt-md-5 mb-4">
        <h2 class="ftco-heading-2">Tentang PsyQonsul</h2>
        <p>PsyQonsul adalah Layanan Konsultasi Online yang merupakan bagian dari Persona Quality</p>
        <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
          <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
          <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
          <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
      </ul>
  </div>
  </div>
  <div class="col-md pt-5" style="position:unset;">
  <div class="ftco-footer-widget pt-md-5 mb-4 ml-md-5">
    <h2 class="ftco-heading-2">Psikologi</h2>
    <ul class="list-unstyled">
      <li><a href="#" class="py-2 d-block">Info Jiwa Sehat</a></li>
      <li><a href="#" class="py-2 d-block">Kamus Psikolog</a></li>
      <li><a href="#" class="py-2 d-block">Energizer Game</a></li>
      <li><a href="#" class="py-2 d-block">Cendera Hati</a></li>
  </ul>
  </div>
  </div>
  <div class="col-md pt-5" style="position:unset;">
  <div class="ftco-footer-widget pt-md-5 mb-4">
    <h2 class="ftco-heading-2">Layanan</h2>
    <ul class="list-unstyled">
      <li><a href="#ftco-section" class="py-2 d-block">Cek Layanan Konsultasi</a></li>
      <li><a href="#" class="py-2 d-block">Daftar Psikolog</a></li>
      <li><a href="#" class="py-2 d-block">Buat Janji</a></li>
  </ul>
  </div>
  </div>
  <div class="col-md pt-5" style="position:unset;">
  <div class="ftco-footer-widget pt-md-5 mb-4">
     <h2 class="ftco-heading-2">Informasi Kontak</h2>
     <div class="block-23 mb-3">
       <ul>
         <li><span class="icon fa fa-map-marker"></span><span class="text">Jl. Parit Indah No.09 Perkantoran Grand sudirman Blok B-7 Pekanbaru</span></li>
         <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">(0761) 848844, 37421, 0851.000.17500</span></a></li>
         <li><a href="#"><span class="icon fa fa-paper-plane"></span><span class="text">persona_quality@yahoo.com, admin@personaquality.com</span></a></li>
     </ul>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-12 text-center" style="position:unset;">
  <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <a href="https://colorlib.com" target="_blank">Persona Quality</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
  </div>
  </div>
  </div>
</footer>
    <!-- Vendor JS Files -->
    
    <script src="{{url('Medicio')}}/assets/vendor/jquery/jquery.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/php-email-form/validate.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/counterup/counterup.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/venobox/venobox.min.js"></script>
    <script src="{{url('Medicio')}}/assets/vendor/aos/aos.js"></script>
    <script src="{{url('konsul')}}/js/bootstrap-datepicker.js"></script>
    <!-- Template Main JS File -->
    <script src="{{url('Medicio')}}/assets/js/main.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
var socket = io.connect('https://www.psyqonsul.com:4000');
<?php if(!empty(Auth::user()->id)){ ?>
  socket.on('new data', function (data) {
    if(data['sendNotif'] == 1 && data['user'] == '<?= Auth::user()->id ?>'){
        swal({
            title: "Lunas",
            text: "Terimakasih Telah Melakukan Pembayaran",
            type: "success"
        }, function() {
            window.location = "{{url('/klien/pemesanan/all')}}";
        });
    }
    if(data['nomor'] == '081215660154'){
      //alert();
    }
  });
  <?php } ?>
</script>
<script type="text/javascript">

      $(function() {
        $( "#datepicker" ).datepicker({
          format: 'dd-mm-yyyy'
        });
      });
</script>
    <script>
        var ukuran_lebar = $(window).width(); 
        if(ukuran_lebar <= 957 ){
          $(".button_profil").show();
        }else{
          $(".button_profil").hide();
        }
        $('#jadwal').load('{{url("/psikolog/ambil-jadwal")}}', function(e) {});
        $('#kategori').load('{{url("/kategori")}}', function(e) {});
        $('#kategori1').load('{{url("/kategori")}}?k=1', function(e) {});

        var id_kategori = $('#inputState').val();
        var id = 0;
        '<?php
          if(!empty($name_konsul)){
              $name_konsul = $name_konsul;
          }else{
              $name_konsul ='';
          }
        ?>'
        $('#sub_kategori').load('{{url("/cari-psikologi")}}/'+id+"?id_kategori="+id_kategori+"&id_sub={{$name_konsul}}", function(e) {});

        
        '<?php
          if(!empty($name)){
              $name = $name;
          }else{
              $name ='';
          }
        ?>'

        $('#sub_kategori1').load('{{url("/cari-psikologi")}}/'+id+"?id_kategori="+id_kategori+"&perjanjian=1&id_sub={{$name}}", function(e) {});
        
        <?php if(!empty($name_konsul)){ ?>
            $('#pasikologi-cari').load('{{url("/cari-psikologi",[$name_konsul])}}', function(e) {});
        <?php } ?>
        
        
        $(document).ready(function() {
            pilihTanggal();

            $( '.uang' ).mask('000.000.000', {reverse: true});

        });
        $('#masalah').val(Cookies.get('masalah'));

        
        
    </script>

    <!-- Template Main JS File -->
  <!-- Template Main JS File -->
    @yield('scripts')
</body>

</html>