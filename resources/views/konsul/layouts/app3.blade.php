<!DOCTYPE html>
<html lang="en">
<head>
    <title>PsyQonsul - Online Konseling Psikologi</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
<!--     <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
 -->
  <link rel="stylesheet"
        href="{{ url('adminlte/css') }}/select2.min.css"/>
  <link href="{{ url('adminlte/css/AdminLTE.min.css') }}" rel="stylesheet">
  <link href="{{ url('adminlte/css/skins/skin-blue.min.css') }}" rel="stylesheet">
  
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{url('metronic')}}/assets/vendors/base/vendors.bundle.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url('konsul')}}/css/animate.css">
    
    <link rel="stylesheet" href="{{url('konsul')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('konsul')}}/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{url('konsul')}}/css/magnific-popup.css">

    <link rel="stylesheet" href="{{url('konsul')}}/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="{{url('konsul')}}/css/jquery.timepicker.css">

    <!-- icon -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{url('konsul')}}/assets/icofont/icofont.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css')}}/bootstrap.min.css">
    <script src="{{url('js/js')}}/jquery.min.js"></script>
    <script src="{{url('js')}}/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{url('konsul')}}/css/flaticon.css">
    <link rel="stylesheet" href="{{url('konsul')}}/css/style.css">
    <link rel="stylesheet" href="{{url('konsul')}}/css/custom.css">
    <script src="{{url('metronic')}}/assets/demo/demo9/base/scripts.bundle.js"></script>
</head>

<body class="" style="margin-top: 0px;">
    @include('konsul.partials.menu')
    <div class="tab-content">
		<div class="">
        @yield("content")
        </div>
    </div>
    
<footer class="ftco-footer ftco-no-pt">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md pt-5">
        <div class="ftco-footer-widget pt-md-5 mb-4">
          <h2 class="ftco-heading-2">About</h2>
          <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
            <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
        </ul>
    </div>
</div>
<div class="col-md pt-5">
    <div class="ftco-footer-widget pt-md-5 mb-4 ml-md-5">
      <h2 class="ftco-heading-2">Help Desk</h2>
      <ul class="list-unstyled">
        <li><a href="#" class="py-2 d-block">Customer Care</a></li>
        <li><a href="#" class="py-2 d-block">Legal Help</a></li>
        <li><a href="#" class="py-2 d-block">Services</a></li>
        <li><a href="#" class="py-2 d-block">Privacy and Policy</a></li>
        <li><a href="#" class="py-2 d-block">Refund Policy</a></li>
        <li><a href="#" class="py-2 d-block">Call Us</a></li>
    </ul>
</div>
</div>
<div class="col-md pt-5">
   <div class="ftco-footer-widget pt-md-5 mb-4">
      <h2 class="ftco-heading-2">Recent Courses</h2>
      <ul class="list-unstyled">
          <li><a href="#" class="py-2 d-block">Computer Engineering</a></li>
          <li><a href="#" class="py-2 d-block">Web Design</a></li>
          <li><a href="#" class="py-2 d-block">Business Studies</a></li>
          <li><a href="#" class="py-2 d-block">Civil Engineering</a></li>
          <li><a href="#" class="py-2 d-block">Computer Technician</a></li>
          <li><a href="#" class="py-2 d-block">Web Developer</a></li>
      </ul>
  </div>
</div>
<div class="col-md pt-5">
    <div class="ftco-footer-widget pt-md-5 mb-4">
       <h2 class="ftco-heading-2">Have a Questions?</h2>
       <div class="block-23 mb-3">
         <ul>
           <li><span class="icon fa fa-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
           <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">+2 392 3929 210</span></a></li>
           <li><a href="#"><span class="icon fa fa-paper-plane"></span><span class="text">info@yourdomain.com</span></a></li>
       </ul>
   </div>
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12 text-center">

    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      Copyright ©<script>document.write(new Date().getFullYear());</script> <i class="fa fa-heart" aria-hidden="true"></i> by <a href="{{url('/')}}" target="_blank">PsyQonsul</a>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
  </div>
</div>
</div>
</footer>
    <!-- Vendor JS Files -->
    
    <script src="{{url('konsul')}}/js/jquery.min.js"></script>
    <script src="{{url('konsul')}}/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="{{url('konsul')}}/js/popper.min.js"></script>
    <script src="{{url('konsul')}}/js/bootstrap.min.js"></script>
    <script src="{{url('konsul')}}/js/jquery.easing.1.3.js"></script>
    <script src="{{url('konsul')}}/js/jquery.waypoints.min.js"></script>
    <script src="{{url('konsul')}}/js/jquery.stellar.min.js"></script>
    <script src="{{url('konsul')}}/js/owl.carousel.min.js"></script>
    <script src="{{url('konsul')}}/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('konsul')}}/js/jquery.animateNumber.min.js"></script>
    <script src="{{url('konsul')}}/js/bootstrap-datepicker.js"></script>
    <script src="{{url('konsul')}}/js/scrollax.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="{{url('konsul')}}/js/google-map.js"></script>
    <script src="{{url('konsul')}}/js/main.js"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
      $(function() {
        $( "#datepicker" ).datepicker({
          format: 'dd-mm-yyyy'
        });
      });
    </script>
    <script>
   
        $('#jadwal').load('{{url("/psikolog/ambil-jadwal")}}', function(e) {});
        $('#kategori').load('{{url("/kategori")}}', function(e) {});
        $('#kategori1').load('{{url("/kategori")}}?k=1', function(e) {});
        <?php if(!empty($name_konsul)){ ?>
            $('#pasikologi-cari').load('{{url("/cari-psikologi",[$name_konsul])}}', function(e) {});
        <?php } ?>
        
        
        $(document).ready(function() {
            pilihTanggal();

            $( '.uang' ).mask('000.000.000', {reverse: true});

        });
        $('#masalah').val(Cookies.get('masalah'));
    </script>

    <!-- Template Main JS File -->
    <script src="{{url('BizLand')}}/assets/js/main.js"></script>
    @yield('scripts')
</body>

</html>