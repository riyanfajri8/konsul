
<section id="step">
    <div class="card2 card-timeline px-2 border-none">
        <div class="col-md-8 offset-md-2 justify-content-center">
        <ul class="bs4-order-tracking">
        <li class="step active">
            <div style="width: 40px; height: 40px;"><i style="color: #fff; padding: 0;" 
            class="fas fa-clipboard-list"></i></div> Cek Layanan Konsultasi
        </li>
        <li class="step">
            <div style="width: 40px; height: 40px;"><i style="color: #fff; padding: 0;" 
            class="fas fa-user-md"></i></div> Cari Psikolog
        </li>
        <li class="step ">
            <div style="width: 40px; height: 40px;"><i style="color: #fff; padding: 0;" 
            class="far fa-calendar-check"></i></div> Buat Janji Konsultasi
        </li>
        </ul>
        </div>
    </div>
</section>
<form action="{{url('cari-psikolog')}}"  method="GET">
      <div class="row aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-3 mb-5 mb-lg-0">
          <label for="form24" class="petunjuk">
            <i class="fas fa-caret-square-down prefix" style="color: #218838"></i> Pilih Kategori Konsultasi
          </label>
            <ul class="nav nav-tabs flex-column">
            <?php $no = 0; ?>
            @foreach($kategori as $kategori2)
              @if($no == 0)
              <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#tab-{{$kategori2->id_kategori_konsul}}">
                  <h4>{{$kategori2->nama_konsul}}</h4>
                </a>
              </li>
              @else
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-{{$kategori2->id_kategori_konsul}}">
                  <h4>{{$kategori2->nama_konsul}}</h4>
                </a>
              </li>
              @endif
              <?php $no++ ?>
            @endforeach
            </ul>
          </div>
          <?php $no = 0; ?>
          <div class="col-lg-9 " style="color: #9EA1A2">
            <div class="tab-content">
              @foreach($kategori as $kategori2)
                <?php
                    $sub_kategori = DB::table('kategori_konsul_sub_kategori')
                    ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                    ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategori2->id_kategori_konsul)
                    ->get();
                ?>
              @if($no == 0)
              <div class="tab-pane active show" id="tab-{{$kategori2->id_kategori_konsul}}">
                <h3>{{$kategori2->nama_konsul}}</h3>
                  
                    <label for="form24" class="petunjuk">
                      <i class="fas fa-check-square prefix" style="color: #218838"></i> Silahkan pilih detail permasalahan yang akan anda konsultasikan
                    </label>                <div class="row">
                  <div class="col-md-12">
                    <div class="funkyradio">
                      @foreach($sub_kategori as $sub_kategori)
                        <div class="funkyradio-danger">
                            <input type="radio" value="{{$sub_kategori->id_sub_kategori}}" name="sub_kategori" id="radio-r-{{$sub_kategori->id_sub_kategori}}">
                            <label class="labelradio" for="radio-r-{{$sub_kategori->id_sub_kategori}}">{{$sub_kategori->nama_sub_kategori}}</label>
                        </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
              @else
              <div class="tab-pane" id="tab-{{$kategori2->id_kategori_konsul}}">
                <h3>{{$kategori2->nama_konsul}}</h3>
                  
                    <label for="form24" class="petunjuk">
                      <i class="fas fa-check-square prefix" style="color: #218838"></i> Silahkan pilih detail permasalahan yang akan anda konsultasikan
                    </label>                <div class="row">
                  <div class="col-md-12">
                    <div class="funkyradio">
                      @foreach($sub_kategori as $sub_kategori)
                        <div class="funkyradio-danger">
                            <input type="radio" value="{{$sub_kategori->id_sub_kategori}}" name="sub_kategori" id="radio-r-{{$sub_kategori->id_sub_kategori}}">
                            <label class="labelradio" for="radio-r-{{$sub_kategori->id_sub_kategori}}">{{$sub_kategori->nama_sub_kategori}}</label>
                        </div>
                      @endforeach
                    </div>
                  </div>
                  
                </div>
              </div>
              @endif
              <?php $no++ ?>
              @endforeach
            </div>
                <div class="row mt-4">
                  <div class="col-md">
                    <!--Textarea with icon prefix-->
                    <div class="md-form amber-textarea active-amber-textarea-2">
                      <label for="form24" class="petunjuk">
                        <i class="fas fa-pencil-alt prefix" style="color: #218838"></i> Tambahkan detail gejala-gejala / permasalahan yang anda rasakan (Optional)
                      </label>
                      <textarea id="gejala" class="md-textarea form-control" rows="3"></textarea>
                    </div>
                  </div>
                </div>
                  <div class="row mt-4">
                    <div class="col-md">
                      <label onclick="langkahSelanjutnya()" class="btn btn-primary btn-block">Lanjutkan Langkah 2 : Cari Psikolog</label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- {{url('/kategori-konsul/order',[$sub_kategori->id_sub_kategori])}} -->

<script>
    function langkahSelanjutnya(){
        var sub_kategori = $('input[name=sub_kategori]:checked').val();
        var gejala = $('#gejala').val();
        location.href ="{{url('/kategori-konsul/order')}}/"+sub_kategori+"?gejala="+gejala;
    }
    function bukaSubaKategori(id){
        $('#sub-kategori').load('{{url("/sub-kategori")}}/'+id, function(e) {});
    }
</script>
