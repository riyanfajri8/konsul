@extends('konsul.layouts.app2')

@section('content')
<style>
.fixed-top {
    position: inherit;
}
#header {
    background: #f4f4f4;
}
</style>
<section class="ftco-section bg-light">
    <div class="container">
      <div class="row justify-content-center pb-2">
        <div class="col-md-12 heading-section text-center">
          <span class="subheading">Temukan PSikolog kamu</span>
          <h2 class="mb-1">Daftar Psikolog PsyQonsul</h2>
        </div>
      </div>
    </div>
  
        <div class="container" data-aos="fade-up">
        <div class="container mb-4">
            <div class="row justify-content-center">
            <div class="col-md-6">
                <form class="card card-sm"  action="{{url('cari-psikolog')}}"  method="GET">
                    <div class="card-body row no-gutters align-items-center" style="padding: 0.15rem;">
                        <!--end of col-->
                        <div class="col">
                            <input name="nama_psikolog" class="form-control form-control-lg form-control-borderless" type="search" placeholder="Ketikan nama psikolog yang akan anda cari">
                        </div>
                        <!--end of col-->
                        <div class="col-auto">
                            <button class="btn btn-lg btn-success" type="submit">Cari Psikolog</button>
                        </div>
                        <!--end of col-->
                    </div>
                </form>
            </div>
            <!--end of col-->
            </div>
        </div>
        <div class="row gy-4">
            @foreach($psikologi as $psikologi1)
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                <div class="member">
                <div class="member-img rounded-circle">
                    <img src="{{asset($psikologi1->foto)}}" class="img-fluid" alt="">
                </div>
                <div class="member-info">
                    <h4>{{$psikologi1->nama}}</h4>
                    <span>Psikolog</span>
                    <ul class="mt-3 text-left" style="line-height: normal;">
                        <li><i class="icofont-badge"></i> SIPP</li>
                        <p>{{$psikologi1->sip}}</p>
                        <li><i class="icofont-bag"></i> Pengalaman</li>
                        <p>5 tahun</p>
                        <li><i class="icofont-graduate"></i> Pendidikan</li> 
                        <p>{{$psikologi1->universitas}}</p> 
                    </ul>
                </div>
                    <label data-toggle="modal" data-target="#modal-profil" onclick="bukaProfil(<?= $psikologi1->id_psikologi ?>)" class="btn btn-success btn-block mt-4 mb-2"><i class="icofont-eye-alt"></i> Profil Lengkap</label>
                    <a href="{{url('kategori-konsul/order/perjanjian',[$psikologi1->id_psikologi,0])}}" class="btn btn-primarydetail btn-block"><i class="far fa-handshake"></i> Buat Janji </a>                     
                </div>
            </div>
            @endforeach
            @if(empty(Auth::user()->id))
            <div class="container mb-5 mt-4">
                <div class="row justify-content-center pb-2">
                    <div class="col-md-3 heading-section text-center ftco-animate">
                        <p class="daftarpsikolog">Anda seorang Psikolog ?</p>
                        <a href="{{url('register-psikolog')}}i" class="btn btn-lg btn-success" type="submit" style="font-size: 16px;">Daftar sebagai Psikolog</a>
                    </div>
                </div>
            </div>
            @endif

      </div>

</section>
<!-- Modal -->
<div id="modal-profil" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profil</h4>
            </div>
            <div class="modal-body">
                <div id="profilSingkat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection

<script>
    function bukaProfil(id){
        $('#profilSingkat').load('{{url("/profil-psikologi")}}/'+id, function(e) {});
    }
    function buatJanji(id){
        var masalah = $('#masalah').val();
	    Cookies.set('masalah', masalah);
        url = '{{url("/kategori-konsul/order/perjanjian")}}/'+id;
        location.href = url;
    }
</script>

@section('scripts')
@parent

@endsection