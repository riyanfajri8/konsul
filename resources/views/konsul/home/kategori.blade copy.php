<?php

?>
@foreach($kategori as $kategori)
<?php
                    $sub_kategori = DB::table('kategori_konsul_sub_kategori')
                    ->leftJoin('sub_kategori','sub_kategori.id_sub_kategori','=','kategori_konsul_sub_kategori.id_sub_kategori')
                    ->where('kategori_konsul_sub_kategori.id_kategori_konsul',$kategori->id_kategori_konsul)
                    ->get();
                ?>

        <div class="col-lg-4 aos-init aos-animate  mt-4" >
            <div class="box" >
                <h3><?= $kategori->nama_konsul ?></h3>
                <ul style="height: 200px;">
                @foreach($sub_kategori as $sub_kategori)
                    <li><a href="{{url('/kategori-konsul/order',[$sub_kategori->id_sub_kategori])}}">
                                                        {{$sub_kategori->nama_sub_kategori}}</a></li>
                @endforeach
                </ul>
                @if($kategori->id_kategori_konsul != 6)
                    <div class="btn-wrap">
                        <label data-toggle="modal" onclick="bukaSubaKategori(<?= $kategori->id_kategori_konsul ?>)"
                    data-target="#modal-sub-kategori" class="btn-buy">Open All</label>
                    </div>
                @endif
            </div>
        </div>

@endforeach

<script>
    function bukaSubaKategori(id){
        $('#sub-kategori').load('{{url("/sub-kategori")}}/'+id, function(e) {});
    }
</script>
