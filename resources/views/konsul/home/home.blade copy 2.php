@extends('konsul.layouts.app2')

@section('content')
<style>
.login-wrap .form-group {
    position: relative;
    margin-bottom: 23px;
}
</style>
<div class="hero-wrap js-fullheight" style="background-image: url(&quot;{{url('konsul')}}/images/bg_new.jpg&quot;); height: 413px;">
    <div class="container">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            @if (session('danger'))
            <div class="alert alert-danger">
                {{ session('danger') }}
            </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <div class="row no-gutters slider-text js-fullheight align-items-center" data-scrollax-parent="true" style="height: 413px;">
        
            <div class="col-md-7 ftco-animate fadeInUp ftco-animated">
                <span class="subheading">Selamat Datang di PsyQonsul</span>
                <h1 class="mb-3">Ayo Cerita... Kamu tidak perlu Depresi lagi!</h1>
                <p class="caps">PsyQonsul adalah Layanan Konsultasi Online sebagai asisten curhatan pribadi kamu. Kami sudah berpengalaman sejak 2010 yang dibentuk oleh psikolog-psikolog profesional <a href="{{url('register-psikologi')}}">Register Jadi Psikolog</a></p>

                <p class="mb-0">
                    <a href="#" class="btn btn-primary">Selengkapnya</a>
                    <a href="{{url('cari-psikolog')}}" class="btn btn-primary">Cari Psikolog</a>
                </p>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-no-pb ftco-no-pt">
    <div class="container">
        <div class="row">
        @if(empty(Auth::user()->id))
            <div class="col-md-7"></div>
            <div class="col-md-5 order-md-last login" style="display:none">
                <div class="login-wrap p-4 p-md-5">
                
                        <!-- <h3 class="mb-4">Daftar sekarang</h3> -->
                            <form method="POST" action="{{ url('login-user') }}">
                                {{ csrf_field() }}
                                <h1>Konsultasi</h1>
                                <p class="text-muted">Login</p>

                                <div class="input-group mb-3">
                                    <input name="email" type="text"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                                        autofocus placeholder="Email" value="{{ old('email', null) }}">
                                    @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="input-group mb-3">
                                    <input name="password" type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required
                                        placeholder="Password">
                                    @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="input-group mb-4" style="padding-left: 11px;">
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" name="remember" type="checkbox" id="remember"
                                            style="vertical-align: middle;" />
                                        <label class="form-check-label" for="remember" style="vertical-align: middle; color: #6c757d !important;">
                                            Remember me
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group d-flex justify-content-end mt-4">
                                    <button type="submit" class="btn btn-primary submit"><span class="fa fa-paper-plane">Login</span></button>
                                </div>

                                <div class="row">
                                    
                                        <a class="" href="{{ route('password.request') }}">
                                            Lupa password?
                                        </a>
                                </div>
                                
                            </form>
                    
                    <p class="text-center" style="#6c757d !important;">Belum Punya Akun Silahkan ? <a  onclick="bukaRegister()" href="#signin">Register</a></p>
                </div>
            </div>

            <div class="col-md-5 order-md-last register">
                <div class="login-wrap p-4 p-md-5">
                    <form action="{{ url('register-user') }}" method="POST" files="true" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    
                        <div class="form-group">
                            <label class="label" for="name">Nama Lengkap</label>
                            <input type="text" id="inputName" name="name" class="form-control" required placeholder="">
                        </div>
                        <div class="form-group">
                            <label class="label" for="email">No. Whatapp</label>
                            <input type="number"  class="form-control"  required name="no_telp">
                        </div>
                        <div class="form-group">
                            <label class="label" for="email">Email</label>
                            <input type="email" id="inputEmail" class="form-control" required name="email">
                        </div>
                        <div class="form-group">
                            <label class="label" for="password">Password</label>
                            <input type="password"  class="form-control" required name="password">
                        </div>
                        <div class="form-group">
                            <label class="label" for="password">Ulangi Password</label>
                            <input type="password" id="inputConfPassword" class="form-control"  required name="password_confirmation">
                        </div>
                        
                        <div class="form-group d-flex justify-content-end mt-4">
                            <button type="submit" class="btn btn-primary submit"><span class="fa fa-paper-plane">Daftar Sekarang</span></button>
                        </div>

                        <div class="row">
                            <!-- <div class="col-6 text-right">
                                <button type="submit" class="btn btn-primary px-4">
                                Submit
                                </button>
                            </div> -->
                            <!-- <div class="col-6">
                                <label  class="btn btn-primary px-4">
                                    Login
                                </label>
                            </div> -->
                        </div>
                    </form>
                    <p class="text-center" style="#6c757d !important;">Sudah memiliki akun? <a onclick="loginBuka()" href="#">Login</a></p>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>

<section class="ftco-section bg-light">
   <div class="container">
        <div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate fadeInUp ftco-animated">
            <span class="subheading">Bagaimana Cara Konsultasi PsyQonsul ?</span>
            <h2 class="mb-4">3 Langkah Mudah Untuk Solusi Masalah Kamu</h2>
        </div>
    </div>
    <div class="row">
       <div class="col-md-4 ftco-animate fadeInUp ftco-animated">
          <div class="project-wrap">
                <a href="{{url('layanan-konsultasi')}}" class="img" style="background-image: url({{url('konsul')}}/images/work-2.jpg);">
                </a>
                <div class="text p-4">
                    <a href="{{url('layanan-konsultasi')}}" class="btn btn-primary btn-block">1. Cek Layanan Konsultasi</a>
            </div>
       </div>
   </div>
   <div class="col-md-4 ftco-animate fadeInUp ftco-animated">
        <div class="project-wrap">
                <a href="{{url('cari-psikolog')}}" class="img" style="background-image: url({{url('konsul')}}/images/work-2.jpg);">
                </a>
                <div class="text p-4">
                    <a href="{{url('cari-psikolog')}}" class="btn btn-primary btn-block">2. Cari Psikolog</a>
            </div>
        </div>
    </div>
    <div class="col-md-4 ftco-animate fadeInUp ftco-animated">
        <div class="project-wrap">
            <a href="#" class="img" style="background-image: url({{url('konsul')}}/images/cb_3.jpg);">
                </a>
                <div class="text p-4">
                    <button class="btn btn-primary btn-block">3. Buat Janji Konsultasi</button>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center pb-4">
            <div class="col-md-12 heading-section text-center ftco-animate fadeInUp ftco-animated">
                <span class="subheading">Permasalahan apa saja yang bisa saya konsultasikan ?</span>
                <h2 class="mb-4">Cek Layanan Konsultasi</h2>
            </div>
        </div>
    
        <div class="row" id="kategori">
        </div>
    <div>

</section>
<!-- Modal -->
<div id="modal-sub-kategori" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div id="sub-kategori"></div>
    </div>
</div>

<section class="ftco-section bg-light">
   <div class="container">
      <div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center ftco-animate fadeInUp ftco-animated">
          	<span class="subheading">Bagaimana cara mencari Psikolog ?</span>
            <h2 class="mb-4">Cari Psikolog</h2>
          </div>
      </div>
    </div>
  
        <div class="container" data-aos="fade-up">

        <div class="row gy-4">

         @foreach($psikologi as $psikologi1)
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                    <div class="member">
                    <div class="member-img">
                        <img src="{{asset($psikologi1->foto)}}" class="img-fluid" alt="">
                    </div>
                    <div class="member-info" style="height: 255px;">
                        <h4>{{$psikologi1->nama}}</h4>
                        <span>Psikolog</span>
                        <ul class="mt-3 text-left" style="line-height: normal;">
                            <li><i class="icofont-badge"></i> SIPP</li>
                            <p>{{$psikologi1->sip}}</p>
                            <li><i class="icofont-bag"></i> Pengalaman</li>
                            <p>5 tahun</p>
                            <li><i class="icofont-graduate"></i> Pendidikan</li> 
                            <p>{{$psikologi1->universitas}}</p> 
                        </ul>
                    </div>
                        <button onclick="bukaProfil(<?= $psikologi1->id_psikologi ?>)" data-toggle="modal" data-target="#modal-profil" class="btn btn-primarydetail btn-block mb-3 mt-4"><i class="icofont-eye-alt"></i> Detail Psikolog</button>
                    </div>
                </div>
            @endforeach
      </div>
</section>

<!-- Modal -->
<div id="modal-profil" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profill</h4>
            </div>
            <div class="modal-body">
                <div id="profilSingkat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script>
    function loginBuka(){
        $(".login").show();
        $(".register").hide();
    }
    function bukaRegister(){
        $(".login").hide();
        $(".register").show();
    }

    function bukaProfil(id){
        $('#profilSingkat').load('{{url("/profil-psikologi")}}/'+id, function(e) {});
    }
    function buatJanji(id){
        var masalah = $('#masalah').val();
	    Cookies.set('masalah', masalah);
        url = '{{url("/kategori-konsul/order/perjanjian")}}/'+id;
        location.href = url;
    }
</script>
@parent

@endsection