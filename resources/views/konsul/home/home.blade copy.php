@extends('konsul.layouts.app')

@section('content')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" >
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div calss="row">
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                @if(!empty(Auth::user()->id))
                <div class="section-title">
                    <h3>SOLUSI MASALAH TEPAT Dan DIPERCAYA</h3>
                    <p>3 LANGKAH MUDAH MENDAPATKAN KONSULTASI DARI PARA AHLINYA</p>

                    <div class="icon-box">
                        <img calss="img-fluid" src="{{asset('foto/langkah_muda.png')}}" style="width: 533px;">
                    </div>
                </div>
                @else
                <div class="col-lg-8">
                    <div class="section-title">
                        <h3>SOLUSI MASALAH TEPAT Dan DIPERCAYA</h3>
                        <p>3 LANGKAH MUDAH MENDAPATKAN KONSULTASI DARI PARA AHLINYA</p>

                        <div class="icon-box">
                            <img calss="img-fluid" src="{{asset('foto/langkah_muda.png')}}" style="width: 533px;">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="card-body">
                            @if (session('danger'))
                            <div class="alert alert-danger">
                                {{ session('danger') }}
                            </div>
                            @endif
                            <form method="POST" action="{{ url('login-user') }}">
                                {{ csrf_field() }}
                                <h1>Konsultasi</h1>
                                <p class="text-muted">Login</p>

                                <div class="input-group mb-3">
                                    <input name="email" type="text"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                                        autofocus placeholder="Email" value="{{ old('email', null) }}">
                                    @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="input-group mb-3">
                                    <input name="password" type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required
                                        placeholder="Password">
                                    @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="input-group mb-4" style="padding-left: 11px;">
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" name="remember" type="checkbox" id="remember"
                                            style="vertical-align: middle;" />
                                        <label class="form-check-label" for="remember" style="vertical-align: middle;">
                                            Remember me
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-primary px-4">
                                            Login
                                        </button>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a class="btn btn-primary px-4" href="{{url('/register')}}">
                                            Register
                                        </a>
                                        <a class="btn btn-link px-0" href="{{ route('password.request') }}">
                                            Forgot your password?
                                        </a>



                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section><!-- End Hero -->

<section id="service" class="pricing">
      <div class="container aos-init aos-animate">

        <div class="section-title">
          <h3>CEK LAYANAN <span>KONSULTASI</span></h3>
          
        </div>

        <div class="row">

          
          <div id="kategori"></div>
          

        </div>

      </div>
</section>

<!-- Modal -->
<div id="modal-sub-kategori" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div id="sub-kategori"></div>
    </div>
</div>

<!-- ======= Footer ======= -->
<footer id="footer">

    <!-- <div class="footer-newsletter">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h4>Ketik Masalah Kamu Disini</h4>
                    <form action="" method="post">
                        <input type="email" name="masalah"><input type="submit" value="Subscribe">
                    </form>
                </div>
            </div>
        </div>
    </div> -->

    <div class="footer-top" style="background: #ddd9d9;">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>PSYQONSUL<span>.</span></h3>
                    <p>
                        A108 Adam Street <br>
                        New York, NY 535022<br>
                        United States <br><br>
                        <strong>Phone:</strong> +1 5589 55488 55<br>
                        <strong>Email:</strong> info@example.com<br>
                    </p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                   
                </div>
                @if(empty(Auth::user()->name))
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Daftar Psikologi</h4>
                    <div class="mt-3">
                    <p>
                    
                    <strong>
                        <a href="{{url('register-psikologi')}}">Registrasi Psikologi</a>
                    </strong>
                    
                    </p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="container py-4">
        <div class="copyright">
            &copy; Copyright <strong><span>PSYQONSUL</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bizland-bootstrap-business-template/ -->
            Designed by <a href="https://bootstrapmade.com/">PSYQONSUL</a>
        </div>
    </div>
</footer><!-- End Footer -->
@endsection
@section('scripts')
@parent

@endsection