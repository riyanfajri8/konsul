@extends('konsul.layouts.app2')

@section('content')
<?php
use App\Transaksi;
?>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style>
.login-wrap .form-group {
    position: relative;
    margin-bottom: 23px;
}
.rating{
	position: absolute;
	top:50%;
	left: 50%;
	transform: translate(-50%, -50%) rotateY(180deg);
	display: flex;
}
.rating input{
	display: none;
}
.rating label{
	display: block;
	cursor: pointer;
	width: 50px;
	/*background: #ccc;*/
}
.rating label:before{
	content:'\f005';
	font-family: fontAwesome;
	position: relative;
	display: block;
	font-size: 50px;
	color: #101010;
}
.rating label:after{
	content:'\f005';
	font-family: fontAwesome;
	position: absolute;
	display: block;
	font-size: 50px;
	color: #fffa00;
	top:0;
	opacity: 0;
	transition: .5s;
	text-shadow: 0 2px 5px rgba(0,0,0,.5);
}
.rating label:hover:after,
.rating label:hover ~ label:after,
.rating input:checked ~ label:after
{
	opacity: 1;
}
.sweet-alert .sa-confirm-button-container {
    display: inline-block;
    position: relative;
    padding-top: 39px;
}

</style>

<!-- jumbotron -->
<div class="hero-wrap js-fullheight" style="background-image: url('{{url('Medicio')}}/assets/img/bg_new.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-center" data-scrollax-parent="true">
        <div class="col-md-7 ftco-animate">
          <span class="subheading">Selamat Datang di PsyQonsul</span>
          <h1 class="mb-1">Ayo Curhat... Kamu gak perlu Galau lagi!</h1>
          <p class="caps">PsyQonsul adalah Layanan Konsultasi Online, yang diasuh oleh Psikolog - Psikolog Profesional.
              Kami sudah berpengalaman sejak 2010.</p>
          <p class="mb-0">
            <a href="#" class="btn btn-primary">Selengkapnya</a>
          </p>
        </div>
      </div>
    </div>
  </div>


  <!-- register di jumbotron -->
  <section class="ftco-section ftco-no-pb ftco-no-pt">
            <!-- @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif -->
            @if (session('danger'))
            <div class="alert alert-danger">
                {{ session('danger') }}
            </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
    <div class="container">
       
      <div class="row">
        <div class="col-md-7"></div>
        @if(empty(Auth::user()->id))
            <div class="col-md-5 order-md-last login" style="display:none">
                <div class="login-wrap p-4 p-md-5">
                
                        <!-- <h3 class="mb-4">Daftar sekarang</h3> -->
                            <form method="POST" action="{{ url('login-user') }}">
                                {{ csrf_field() }}
                                <!-- <div class="input-group mb-3">
                                    <input name="email" type="text"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                                        autofocus placeholder="Email" value="{{ old('email', null) }}">
                                    @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="input-group mb-3">
                                    <input name="password" type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required
                                        placeholder="Password">
                                    @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div> -->

                                <div class="form-group">
                                    <label class="label" for="name">Email</label>
                                    <input name="email" type="text"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                                        autofocus placeholder="" value="{{ old('email', null) }}">
                                    @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="label" for="name">Password</label>
                                    <input name="password" type="password"
                                        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required
                                        placeholder="">
                                    @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-md">
                                        <div class="input-group mb-4" style="padding-left: 11px;">
                                            <div class="form-check checkbox">
                                                <input class="form-check-input" name="remember" type="checkbox" id="remember"
                                                    style="vertical-align: middle;" />
                                                <label class="form-check-label" for="remember" style="vertical-align: middle; color: #6c757d !important; font-size: 14px;">
                                                    Ingat Saya
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                    <a style="color: #bf0c0c; float:right; padding:0;"  href="{{ route('password.request') }}">
                                        Lupa password?
                                    </a>
                                    </div>
                                </div>
                        
                                <div class="form-group d-flex justify-content-end mt-4" style="display;">
                                    <button type="submit" class="btn btn-primary submit"><span>Login</span></button>
                                </div>
                            </form>
                    
                    <p class="text-center" style="color: #555; display: inherit;">Belum Punya Akun ? <a style="color: #bf0c0c" onclick="bukaRegister()" href="#signin">Daftar Sekarang</a></p>
                </div>
            </div>

            <div class="col-md-5 order-md-last register" >
                <div class="login-wrap p-4 p-md-5">
                    <form action="{{ url('register-user') }}" method="POST" files="true" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                            <div class="form-group">
                                <label class="label" for="name">Nama Lengkap</label>
                                <input type="text" id="inputName" name="name" class="form-control" required placeholder="">
                            </div>
                            <div class="form-group">
                                <label class="label" for="email">No.Handphone / WA</label>
                                <input type="number"  class="form-control"  required name="no_telp">
                            </div>
                            <div class="form-group">
                                <label class="label" for="email">Email</label>
                                <input type="email" id="inputEmail" class="form-control" required name="email">
                            </div>
                            <div class="form-group">
                                <label class="label" for="password">Password</label>
                                <input type="password"  class="form-control" required name="password">
                            </div>
                            <div class="form-group">
                                <label class="label" for="password">Ulangi Password</label>
                                <input type="password" id="inputConfPassword" class="form-control"  required name="password_confirmation">
                            </div>
                            
                            <div class="form-group d-flex justify-content-end mt-4">
                                <button type="submit" class="btn btn-primary submit"><span>Daftar Sekarang</span></button>
                            </div>

                            <div class="row">
                                <!-- <div class="col-6 text-right">
                                    <button type="submit" class="btn btn-primary px-4">
                                    Submit
                                    </button>
                                </div> -->
                                <!-- <div class="col-6">
                                    <label  class="btn btn-primary px-4">
                                        Login
                                    </label>
                                </div> -->
                            </div>
                    </form>
                    <p class="text-center" style="color: #555; display: inherit;">Sudah memiliki akun?
                    <a type="button" onclick="loginBuka()" data-toggle="modal" data-target="#exampleModalLong" style="color: #bf0c0c">Login</a></p>        
                </div>
            </div>
        </div>
        @endif
      </div>
    </div>
</section>



<section id="3_langkah" class="ftco-section bg-light">
   <div class="container">
        <div class="row justify-content-center pb-4">
          <div class="col-md-12 heading-section text-center">
            <span class="subheading">Bagaimana Cara Konsultasi PsyQonsul ?</span>
            <h2>3 Langkah Mudah Untuk Solusi Masalah Kamu</h2>
        </div>
    </div>
    <div class="row">
       <div class="col-md-4">
          <div class="project-wrap">
                <a href="#layanan1" class="img" style="background-image: url({{url('Medicio')}}/assets/img/cara-konsul/1.png);">
                </a>
                <div class="text p-4">
                    <!-- <a href="{{url('layanan-konsultasi')}}" class="btn btn-primary btn-block">1. Cek Layanan Konsultasi</a> -->
                    <a href="#layanan1" class="btn btn-primary btn-block">1. Cek Layanan Konsultasi</a>
            </div>
       </div>
   </div>
   <div class="col-md-4">
        <div class="project-wrap">
                <a href="{{url('cari-psikolog')}}" class="img" style="background-image: url({{url('Medicio')}}/assets/img/cara-konsul/2.jpg);">
                </a>
                <div class="text p-4">
                    <a href="{{url('cari-psikolog')}}" class="btn btn-primary btn-block">2. Cari Psikolog</a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="project-wrap">
            <a href="#" class="img" style="background-image: url({{url('Medicio')}}/assets/img/cara-konsul/3.jpg);">
                </a>
                <div class="text p-4">
                    <button class="btn btn-primary btn-block">3. Buat Janji Konsultasi</button>
            </div>
        </div>
    </div>
</section>


<section id="layanan1" class="ftco-section bg-grey" style="background-color: #f9f9f9">
   <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-12 heading-section text-center">
            <span class="subheading">Permasalahan apa saja yang bisa saya konsultasikan ?</span>
            <h2>Langkah 1 : Cek Layanan Konsultasi</h2>
        </div>
      </div>
    
        <div id="kategori">
        </div>
    <div>

</section>


<!-- Modal -->
<div id="modal-sub-kategori" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div id="sub-kategori"></div>
    </div>
</div>

<section class="ftco-section bg-light">
     <div class="container" style="max-width: 1000px">
        <div class="row justify-content-center pb-2">
            <div class="col-md-12 heading-section text-center">
              <span class="subheading">Psikolog Profesional</span>
              <h2 class="mb-4">Daftar Psikolog PsyQonsul</h2>
            </div>
        </div>
      </div>
        
     <div class="container mb-4">
      <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{url('cari-psikolog')}}"  method="GET" class="card card-sm">
                <div class="card-body row no-gutters align-items-center" style="padding: 0.15rem;">
                    <!--end of col-->
                    <div class="col">
                        <input name="nama_psikolog" class="form-control form-control-lg form-control-borderless" type="search" placeholder="Ketikan nama psikolog yang akan anda cari">
                    </div>
                    <!--end of col-->
                    <div class="col-auto">
                        <button class="btn btn-lg btn-success" type="submit">Cari Psikolog</button>
                    </div>
                    <!--end of col-->
                </div>
            </form>
        </div>
        <!--end of col-->
      </div>
    </div>
        

      <div class="container-xl aos-init aos-animate" data-aos="fade-up" style="max-width: 1190px" ;="">
        <div class="row row-table gy-4">

          <div class="col-md-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>   
            <!-- Wrapper for carousel items -->

            <div class="carousel-inner">
            <?php $no=0; ?>
            @foreach($psikologi as $psikologi1)
            @if($no == 0)
              <div class="item carousel-item active">
                <div class="row">
                <?php $no1=0; ?>
                @foreach($psikologi as $psikologi2)
                    @if($no1 <= 3)
                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <div class="member">
                            <div class="member-img rounded-circle">
                                <img src="{{asset($psikologi2->foto)}}" class="img-fluid" alt="">
                            </div>
                            <div class="member-info">
                                <h4>{{$psikologi2->nama}}</h4>
                                <span>Psikolog</span>
                                <ul class="mt-3 text-left" style="line-height: normal;">
                                    <li><i class="icofont-badge"></i> SIPP</li>
                                    <p>{{$psikologi2->sip}}</p>
                                    <li><i class="icofont-bag"></i> Pengalaman</li>
                                    <p>{{$psikologi2->pengalaman}} tahun</p>
                                    <li><i class="icofont-graduate"></i> Pendidikan</li> 
                                    <p>{{$psikologi2->universitas}}</p> 
                                </ul>
                            </div>
                                <label data-toggle="modal" data-target="#modal-profil" onclick="bukaProfil(<?= $psikologi2->id_psikologi ?>)" class="btn3 btn-success btn-block mt-4 mb-2"><i class="icofont-eye-alt"></i> Profil Lengkap</label>
                                <!-- <a href="{{url('kategori-konsul/order/perjanjian',[$psikologi2->id_psikologi,0])}}" class="btn btn-primarydetail btn-block"><i class="far fa-handshake"></i> Buat Janji </a>                         -->
                            </div>
                        </div>
                    @endif
                    <?php $no1++ ?>
                @endforeach
                </div>
              </div>
            @elseif($no == 4)
            <div class="item carousel-item ">
                <div class="row">
                <?php $no2 = 0 ?>
                @foreach($psikologi as $psikologi3)
                    @if($no2 > 3 && 3 < $no2)
                        @if($no2 <= 7)
                        <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                            <div class="member">
                            <div class="member-img rounded-circle">
                                <img src="{{asset($psikologi3->foto)}}" class="img-fluid" alt="">
                            </div>
                            <div class="member-info">
                                <h4>{{$psikologi3->nama}}</h4>
                                <span>Psikolog</span>
                                <ul class="mt-3 text-left" style="line-height: normal;">
                                    <li><i class="icofont-badge"></i> SIPP</li>
                                    <p>{{$psikologi3->sip}}</p>
                                    <li><i class="icofont-bag"></i> Pengalaman</li>
                                    <p>{{$psikologi3->pengalaman}} tahun</p>
                                    <li><i class="icofont-graduate"></i> Pendidikan</li> 
                                    <p>{{$psikologi3->universitas}}</p> 
                                </ul>
                            </div>
                                <label data-toggle="modal" data-target="#modal-profil" onclick="bukaProfil(<?= $psikologi3->id_psikologi ?>)" class="btn3 btn-success btn-block mt-4 mb-2">
                                    <i class="icofont-eye-alt"></i> 
                                    Profil Lengkap
                                </label>
                                <!-- <a href="{{url('kategori-konsul/order/perjanjian',[$psikologi3->id_psikologi,0])}}" class="btn btn-primarydetail btn-block"><i class="far fa-handshake"></i> Buat Janji </a>                          -->
                            </div>
                        </div>
                        @endif
                    @endif
                    <?php $no2++ ?>
                @endforeach
                </div>
              </div>
            @elseif($no == 8)
            <div class="item carousel-item ">
                <div class="row">
                    <?php $no3 = 0 ?>
                    @foreach($psikologi as $psikologi4)
                        @if($no3 > 7)
                            @if($no3 <= 11)
                            <div class="col-lg-3 col-md-6 d-flex align-items-stretch aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                                <div class="member">
                                <div class="member-img rounded-circle">
                                    <img src="{{asset($psikologi4->foto)}}" class="img-fluid" alt="">
                                </div>
                                <div class="member-info">
                                    <h4>{{$psikologi4->nama}}</h4>
                                    <span>Psikolog</span>
                                    <ul class="mt-3 text-left" style="line-height: normal;">
                                        <li><i class="icofont-badge"></i> SIPP</li>
                                        <p>{{$psikologi4->sip}}</p>
                                        <li><i class="icofont-bag"></i> Pengalaman</li>
                                        <p>{{$psikologi4->pengalaman}} tahun</p>
                                        <li><i class="icofont-graduate"></i> Pendidikan</li> 
                                        <p>{{$psikologi4->universitas}}</p> 
                                    </ul>
                                </div>
                                    <label data-toggle="modal" data-target="#modal-profil" onclick="bukaProfil(<?= $psikologi4->id_psikologi ?>)" class="btn3 btn-success btn-block mt-4 mb-2"><i class="icofont-eye-alt"></i> Profil Lengkap</label>
                                    <!-- <a href="{{url('kategori-konsul/order/perjanjian',[$psikologi4->id_psikologi,0])}}" class="btn btn-primarydetail btn-block"><i class="far fa-handshake"></i> Buat Janji </a>                      -->
                                </div>
                            </div>
                            @endif
                        @endif
                        <?php $no3++ ?>
                    @endforeach
                    </div>
                </div>
            @endif
              <?php $no++; ?>
            @endforeach 
            </div>
            
            <!-- Carousel controls -->
            <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
              <i class="icofont-caret-left"></i>
            </a>
            <a class="carousel-control-next" href="#myCarousel" data-slide="next">
              <i class="icofont-caret-right"></i>
            </a>
        </div>
      </div>
</div></div></section>

  <!-- register psikolog menu -->
  <div class="container mb-5 mt-4">
    <div class="row justify-content-center pb-2">
      <div class="col-md-3 heading-section text-center">
        <p class="daftarpsikolog"><b>Anda Seorang Psikolog ?</b></p>
        <a href="{{url('register-psikolog')}}i" class="appointment-btn scrollto" type="submit" style="font-size: 16px;">Daftar sebagai Psikolog</a>
      </div>
    </div>
  </div>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modal-profil" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Profil Psikolog</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>

            <div class="modal-body">
                <div id="profilSingkat"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<script>
<?php
    if(!empty(Transaksi::kasih_reting())){
?>
var boton = "button";
swal({   
    title: "<small>Kasih Reting Psikolog <br> <?= Transaksi::kasih_reting()->psikologi->nama; ?></small>",  
    text: ' <div class="rating"><input type="radio" name="star" id="star1" value="1"><label for="star1"></label><input type="radio" name="star" id="star2" value="2"><label for="star2"></label><input type="radio" name="star" id="star3" value="3"><label for="star3" ></label><input type="radio" name="star" id="star4" value="4"><label for="star4"></label><input type="radio"> </div>',
    html: true 
});

$( ".confirm" ).click(function() {
    var reting = $('input[name=star]:checked', '.rating').val();
    if(reting == 'undefined'){
        reting = 0;
    }
    $.ajax({
        url: '{{url("/")}}?reting='+reting,
        type: "GET",
        dataType: "html",
        success: function(data) {

        }
    });
});
<?php      
    }
?>
<?php if (session('danger')) {?>
$(".login").show();
$(".register").hide();
swal({
  title: "{{ session('danger') }}",
  icon: "warning"
});
<?php } ?>
<?php if (session('success')){ ?>
    swal({
    title: "{{ session('success') }}",
    icon: "success"
});
$(".login").show();
$(".register").hide();
history.pushState('', 'New Page Title', '#3_langkah');
<?php } ?>
    function loginBuka(){
        $(".login").show();
        $(".register").hide();
    }
    function bukaRegister(){
        $(".login").hide();
        $(".register").show();
    }

    function bukaProfil(id){
        $('#profilSingkat').load('{{url("/profil-psikologi")}}/'+id, function(e) {});
    }
    function buatJanji(id){
        var masalah = $('#masalah').val();
	    Cookies.set('masalah', masalah);
        url = '{{url("/kategori-konsul/order/perjanjian")}}/'+id;
        location.href = url;
    }
</script>
@parent

@endsection