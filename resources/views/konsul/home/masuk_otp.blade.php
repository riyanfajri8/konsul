@extends('konsul.layouts.app2')

@section('content')

<!-- ======= Footer ======= -->
<section class="otp">
    <div class="container">
    @if (session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
    @endif
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
        <div class="row">
            <div class="col-md-4 offset-md-4 text-center">
            <img src="{{asset('Medicio')}}/assets/otp.jpg" width="200">
            </div>
            <div class="col-md-4 offset-md-4 text-center">
            <h3>Kode Verifikasi <span>00:<span id="waktu">46</span></span></h3>
            <p>Mohon verifikasi identitas dengan memasukkan kode OTP yang telah dikirimkan ke nomor Whatsapp anda.</p>
            </div>
            <div class="col-md-6 offset-md-3 text-center">
            <div class="otp-form">
                <form action="" method="post">
                @csrf
                    <div class="form-group">
                        <input name="id_user" hidden type="text" value="{{ $_GET['id']}}">
                        <input name="otp" type="text" class="form-control" placeholder="Masukkan Kode Verifikasi">
                        <button type="submit" class="btn btn-success">Verifikasi</button>
                    </div>
                </form>
            </div>
            </div>
            <div class="col-md-4 offset-md-4 text-center">
            <p>Belum terima Kode Verifikasi ?<a href="#" onclick="kirimOtpUlang()"> Kirim ulang </a></p>
            </div>
        </div>
    </div>
  </section>

<div id="preloader"></div>
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
@endsection
@section('scripts')
<script type="text/javascript">
    <?php if (session('danger')) {?>
        swal({
            title: "{{ session('danger') }}",
            icon: "warning"
        });
    <?php } ?>
    <?php if (session('success')){ ?>
        
    <?php } ?>
    function kirimOtpUlang(){
        $.ajax({
            url: '{{url("/masuk-otp")}}?id= <?= $_GET['id'] ?>&kirim_ulang=1',
            type: "GET",
            dataType: "html",
            success: function(data) {
                waktuOtp();
            }
        });
    }
  function waktuOtp(){
    var waktu = 60;
    setInterval(function() {
    waktu--;
    if(waktu < 0) {
        return false;
    }else{
    document.getElementById("waktu").innerHTML = waktu;
    }
    }, 1000);
  }
  waktuOtp();
  
</script>
@parent

@endsection