@extends('konsul.layouts.app')

@section('content')

<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}
/* .container { max-width: 1400px !important; } */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
<script type="text/javascript"
	      src="https://app.sandbox.midtrans.com/snap/snap.js"
	      data-client-key="SB-Mid-client-gPB5wXO09A70jve3"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<div class="container mt-4">
    <div id="myCarousel" class="carousel slide">
        <div class="row" style="margin-top:150px;">
            <!-- <form id="payment-form" method="post" action="Payment"> -->
                <!-- <input type="hidden" name="_token" value="{!! csrf_token() !!}"> -->
                <!-- <input type="hidden" name="result_type" id="result-type" value=""></div>
                <input type="hidden" name="result_data" id="result-data" value=""></div> -->
            </from>
            @include('konsul.user.menu')
            <div class="col-md-9 col-md-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs">
                    <div class="tab-content">
                    <div class="table-responsive">
                            <table class="table table-striped" padding="1" id="html_table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No Transaksi</th>
                                        <th>Nama Pesanan</th>
                                        <th>Psikolog</th>
                                        <th>Tanggal Perjanjian</th>
                                        <th>Jam</th>
                                        <th>Media</th>
                                        <th>Total Harga</th>
                                        <th>Pembayaran</th>
                                        <th>Status Konsultasi</th>
                                        <!-- <th>type pembayaran</th>
                                        <th>bank</th>
                                        <th>va number</th>
                                        <th>bank</th>
                                        <th>biller_code</th>
                                        <th>bill_key</th> -->
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($laporan_klien as $laporan_klien1)
                                        <?php
                                            $cek_expired = strtotime($laporan_klien1->jam_transaksi); 
                                            $awal  = new \DateTime($laporan_klien1->created_at); //waktu awal

                                            $akhir = new \DateTime(date('Y-m-d H:i:s')); //waktu akhir

                                            $jumlah_jadwal = $awal->diff($akhir);

                                        ?>
                                        <tr>
                                            <td>{{$laporan_klien1->no_transaksi}}</td>
                                            <td>{{$laporan_klien1->nama_sub_kategori}}</td>
                                            <td>{{$laporan_klien1->nama_psikolog}}</td>
                                            <td>{{date('d-M-Y', strtotime($laporan_klien1->jadwal))}}</td>
                                            <td>{{$laporan_klien1->jam}}</td>
                                            <td>{{$laporan_klien1->nama_media}}</td>
                                            @if($laporan_klien1->status_bayar == 'settlement')
                                                <td><label class="badge badge-primary">Sudah Dibayar</label></td>
                                            @elseif($laporan_klien1->status_bayar == 'expire')
                                                <td><span class="badge badge-danger">Kadar luasa</span></td>
                                            @endif
                                            <td>{{$laporan_klien1->total_konsul}}</td>
                                            <!-- <td>{{$laporan_klien1->type_pembayaran}}</td>
                                            <td>{{$laporan_klien1->bank}}</td>
                                            <td>{{$laporan_klien1->va_number}}</td>
                                            <td>{{$laporan_klien1->bank}}</td>
                                            <td>{{$laporan_klien1->biller_code}}</td>
                                            <td>{{$laporan_klien1->bill_key}}</td> -->
                                            @if(empty($laporan_klien1->type_pembayaran))
                                                @if($laporan_klien1->status_bayar != 'expire')
                                                    <td>
                                                        <label onclick="bayarLansung('<?= $laporan_klien1->no_transaksi ?>')" class="btn btn-primary" id="pay-button">Bayar</label>
                                                    </td>
                                                @endif
                                            @else
                                            <td>
                                                
                                            </td>
                                            @endif
                                                @if($laporan_klien1->status_bayar != 'expire')
                                                    @if($laporan_klien1->status_konsul == 0)
                                                    <td>
                                                        <label>Akan Berlansung</label>
                                                    </td>
                                                    @else
                                                    <td>
                                                        <label>Selesai</label>
                                                    </td>
                                                    @endif
                                                @endif
                                                @if($laporan_klien1->status_bayar == 'expire')
                                                    <td>
                                                        <span class="badge badge-danger" id="pay-button">Batal</span>
                                                    </td>
                                                @endif
                                            
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
function bayarLansung(id){
    var resultType = document.getElementById('result-type');
    var resultData = document.getElementById('result-data');
    function changeResult(type,data){
        $("#result-type").val(type);
        $("#result-data").val(JSON.stringify(data));
        //resultType.innerHTML = type;
        //resultData.innerHTML = JSON.stringify(data);
    }
    $.ajax({
            url: '{{url("klien/bayar")}}?no_transaksi='+id ,
            type: "GET",
            data: $("#form-pendaftaran").serialize(),
            dataType: "html",
            success: function(data) {
                snap.pay(data, {
        
                    onSuccess: function(result){
                    changeResult('success', result);
                    console.log(result.status_message);
                    console.log(result);
                    $("#payment-form").submit();
                    },
                    onPending: function(result){
                    changeResult('pending', result);
                    console.log(result.status_message);
                    $("#payment-form").submit();
                    },
                    onError: function(result){
                    changeResult('error', result);
                    console.log(result.status_message);
                    $("#payment-form").submit();
                    }
                });
            }
        });
    
};
</script>

@endsection
@section('scripts')
@parent

@endsection