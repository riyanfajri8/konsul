<div class="col-md-3">
    <div class="m-portlet m-portlet--full-height  ">
        <div class="m-portlet__body">
            <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                    Your Profile
                </div>
                <div class="m-card-profile__pic">
                    <label onclick="gantifoto()">
                        <div class="m-card-profile__pic-wrapper" style="border-radius: 100px;">
                            <div class="gambar">
                                <?php
                                    if(!empty($biodata->img)){
                                        echo '<img src="'.asset($biodata->img).'" style="border-radius: 100px; width:160px; height:160px;" alt="" />';
                                    }else{
                                ?>

                                <?php } ?>
                                <div class="overlay">
                                    <div class="text">Ganti Foto</div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <div class="m-card-profile__details">
                    <span class="m-card-profile__name m--font-danger"></span>
                    <span class="m-card m--font-primary"
                        style="font-size:15px;">{{ Auth::user()->name }}</span>
                    <hr>
                </div>
            </div>

            <div id="m_ver_menu"
                class="menuprofil m-aside-menu m-aside-menu--skin-light m-aside-menu--submenu-skin-light m-scroller ps ps--active-y"
                data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow">
                    <li data-menuy="m-beranda" class="m-beranda m-menu__item" onclick="tes(this)">
                        <a href="{{url('/psikolog/profil')}}"
                            class="m-menu__link m-menu__toggle menu-dalam">
                            <i class="m-menu__link-icon fas fa-home"></i>
                            <span class="m-menu__link-text"><span>Beranda</span>
                            </span></a>
                    </li>

                    <li data-menux="m-profil" class="m-profil m-menu__item m-menu__item--submenu"
                        aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;"
                            class="m-menu__link m-menu__toggle menu-dalam"><i
                                class="m-menu__link-icon fas fa-user"></i>
                            <span class="m-menu__link-text">Profil</span><i
                                class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li data-menuy="m-profil-1" class="m-menu__item m-profil-1"
                                    onclick="tes(this)">
                                    <a href="{{url('/klien/data-profil')}}"
                                        class="m-menu__link">
                                        <span class="m-menu__link-text">
                                            <span>Data Profil</span></span>
                                    </a>
                                </li>
                                <li data-menuy="m-profil-2" class="m-menu__item m-profil-2"
                                    onclick="tes(this)">
                                    <a href="{{url('/klien/ubah-password')}}"
                                        class="m-menu__link">
                                        <span class="m-menu__link-text"><span>Rubah Password
                                                </span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li data-menuy="m-beranda" class="m-beranda m-menu__item" onclick="tes(this)">
                        <a href="{{url('/klien/pemesanan/all')}}"
                            class="m-menu__link m-menu__toggle menu-dalam">
                            <i class="m-menu__link-icon fas fa-receipt"></i>
                            <span class="m-menu__link-text"><span>Riwayat Pemesanan</span>
                            </span></a>
                    </li>

                    <!-- fitur ini hanya utk koordinator, 5 adalah id_job_level koordinator -->
                </ul>
            </div>
        </div>
    </div>
</div>