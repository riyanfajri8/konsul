@extends('konsul.layouts.app')

@section('content')
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important;
} */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
<div class="container mt-4">
    <div id="myCarousel" class="carousel slide">
        <div class="row" style="margin-top:150px;">

            @include('konsul.user.menu')
            <div class="col-md-9">
                <div class="container" id="laporancuti">

                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon ">
                                        <i class="la la-user"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Edit Data Profil
                                    </h3>
                                </div>
                            </div>
                            <div class="text-center mt-4">
                                <a class="btn btn-primary"
                                    href="{{url('/klien/data-profil')}}"><i class="fa fa-arrow-left"></i> 
                                    Kembali
                                </a>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <!--<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">-->
                        @if($errors->any())
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @if(session('success'))
                            <div class="alert alert-success">
                                {!!  session('success')  !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-md">
                                <div class="login-wrap2 p-4 mt-3">
                                    <form action="" method="POST" files="true" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="form-group">
                                                <label class="label" for="name">Nama Lengkap</label>
                                                <input type="text" id="personal-1-nama_lengkap" name="nama" value="{{ Auth::user()->name }}" class="form-control" required placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label class="label" for="no_hp">No.Handphone / WA</label>
                                                <input type="number" class="form-control" value="{{$biodata->no_hp}}" id="kontakpersonal-1-kontak" name="no_hp" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="label" for="usia">usia</label>
                                                <input type="number" class="form-control"  required name="usia">
                                            </div>
                                            <div class="form-group">
                                                <label class="label" for="jk">Jenis Kelamin</label>
                                                <select id="personal-1-jenis_kelamin" class="form-control" name="jenis_kelamin">
                                                    <option value="">-</option>
                                                    <option value="Laki-laki"
                                                        <?php if ($biodata->jenis_kelamin == 'Laki-laki'){ echo ' selected=""'; }?>>Laki-laki
                                                    </option>
                                                    <option value="Perempuan"
                                                        <?php if ($biodata->jenis_kelamin == 'Perempuan'){ echo ' selected=""'; }?>>Perempuan
                                                    </option>
                                                </select>
                                                <div class="help-block"></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="label" for="status">Status Pernikahan</label>
                                                <select id="personal-1-status_perkawinan" class="form-control" name="status_perkawinan">
                                                    <option value="">-</option>
                                                    <option value="Belum Menikah" <?php if ($biodata->status_perkawinan == 'Belum Menikah')
                                                    { echo ' selected=""'; }?>>Belum Menikah</option>
                                                    <option value="Menikah"
                                                        <?php if ($biodata->status_perkawinan == 'Menikah'){ echo ' selected=""'; }?>>Menikah
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group field-personal-1-id_pendidikan required">
                                                <label class="label" for="pendidikan">Pendidikan Terakhir</label>
                                                <select id="personal-1-id_pendidikan"
                                                        class="form-control"
                                                        name="pendidikan_terakhir">
                                                        <option value="">Pilih</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'Belum Sekolah'){ echo ' selected=""'; }?> value="Belum Sekolah">Belum Sekolah</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'Tidak Tamat SD'){ echo ' selected=""'; }?> value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'TK'){ echo ' selected=""'; }?> value="TK">TK</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'SD'){ echo ' selected=""'; }?> value="SD">SD</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'SLTP'){ echo ' selected=""'; }?> value="SLTP">SLTP</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'SLTA'){ echo ' selected=""'; }?> value="SLTA">SLTA</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'D1'){ echo ' selected=""'; }?> value="D1">D1</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'D3'){ echo ' selected=""'; }?> value="D3">D3</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'D4'){ echo ' selected=""'; }?> value="D4">D4</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'S1'){ echo ' selected=""'; }?>  value="S1">S1</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'S2'){ echo ' selected=""'; }?> value="S2">S2</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'S3'){ echo ' selected=""'; }?> value="S3">S3</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'dr'){ echo ' selected=""'; }?> value="dr">dr</option>
                                                        <option <?php if ($biodata->pendidikan_terakhir == 'Belum Tamat Sekolah'){ echo ' selected=""'; }?> value="Belum Tamat Sekolah">Belum Tamat Sekolah</option>
                                                    </select>
                                                <div class="help-block"></div>
                                            </div>
                                            <div class="form-group field-personal-1-id_pendidikan required">
                                                <label class="label" for="pekerjaan">Pekerjaan</label>
                                                <select id="personal-1-pekerjaan" class="form-control" name="pekerjaan">
                                                    
                                                    <option <?php if ($biodata->pekerjaan == ''){ echo ' selected=""'; }?> value="">-</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Akademik/Pengajar'){ echo ' selected=""'; }?>
                                                        value="Akademik/Pengajar">Akademik/Pengajar</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Ibu Rumah Tangga'){ echo ' selected=""'; }?>
                                                        value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Karyawan BUMN'){ echo ' selected=""'; }?>
                                                        value="Karyawan BUMN">Karyawan BUMN</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Karyawan Swasta'){ echo ' selected=""'; }?>
                                                        value="Karyawan Swasta" data-select2-id="6">

                                                        Karyawan Swasta</option>
                                                    <option <?php if ($biodata->pekerjaan == 'Lain-lain'){ echo ' selected=""'; }?>
                                                        value="Lain-lain">Lain-lain</option>

                                                    <option
                                                        <?php if ($biodata->pekerjaan == 'Pegawai Negeri Sipil (PNS)'){ echo ' selected=""'; }?>
                                                        value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)
                                                    </option>

                                                    <option <?php if ($biodata->pekerjaan == 'Pelajar / Mahasiswa'){ echo ' selected=""'; }?>
                                                        value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>

                                                    <option
                                                        <?php if ($biodata->pekerjaan == 'Pemerintahan / Militer'){ echo ' selected=""'; }?>
                                                        value="Pemerintahan / Militer">Pemerintahan / Militer</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Pensiunan'){ echo ' selected=""'; }?>
                                                        value="Pensiunan">Pensiunan</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Petani'){ echo ' selected=""'; }?>
                                                        value="Petani">Petani</option>
                                                    <option <?php if ($biodata->pekerjaan == 'Profesional ( Dokter, Pengacara,
                                                                                                Dll )'){ echo ' selected=""'; }?> value="Profesional ( Dokter, Pengacara,
                                                                                                Dll )">Profesional ( Dokter, Pengacara,
                                                        Dll )</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Tidak Bekerja'){ echo ' selected=""'; }?>
                                                        value="Tidak Bekerja">Tidak Bekerja</option>

                                                    <option <?php if ($biodata->pekerjaan == 'Wiraswasta'){ echo ' selected=""'; }?>
                                                        value="Wiraswasta">Wiraswasta</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="label" for="email">Kota tempat tinggal saat ini</label>
                                                <input type="text" id="personal-1-alamat_sementara" class="form-control" name="alamat"
                                                    value="{{$biodata->alamat}}">
                                                <div class="help-block"></div>
                                            </div>

                                            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                                <div class="m-form__actions m-form__actions--solid">
                                                    <div class="row">
                                                        <div class="col-lg-9"></div>
                                                        <div class="col-lg-3">
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>
                                                        Submit Data</button> <button type="reset" class="btn btn-default">Reset</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>    
                                </div>




                                <!-- <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label class="">Nama Lengkap:</label>
                                        <div class="form-group field-personal-1-nama_lengkap ">

                                            <input type="text" id="personal-1-nama_lengkap" class="form-control" name="nama"
                                                value="{{ Auth::user()->name }}" placeholder="Isi Nama Lengkap">

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label>Jenis Identitas:</label>

                                        <div class="form-group field-identitaspersonal-id_jenis_identitas required">

                                            <select id="identitaspersonal-id_jenis_identitas" class="form-control" name="jenis_identitas"
                                                aria-required="true">
                                                <option value="" <?php if ($biodata->jenis_identitas == ''){ echo ' selected=""'; }?>>Pilih
                                                </option>
                                                <option value="KTP"
                                                    <?php if ($biodata->jenis_identitas == 'KTP'){ echo ' selected=""'; }?>>KTP</option>
                                                <option value="SIM"
                                                    <?php if ($biodata->jenis_identitas == 'SIM'){ echo ' selected=""'; }?>>SIM</option>
                                                <option value="Passport"
                                                    <?php if ($biodata->jenis_identitas == 'Passport'){ echo ' selected=""'; }?>>Passport
                                                </option>
                                            </select>

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="">No. Identitas:</label>
                                        <div class="form-group field-identitaspersonal-no_identitas required">

                                            <input type="text" id="identitaspersonal-no_identitas" class="form-control " name="no_identitas"
                                                value="{{$biodata->no_identitas}}" placeholder="no_identitas" aria-required="true">

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label>Tempat Lahir:</label>
                                        <div class="form-group field-personal-1-tempat_lahir required">

                                            <input type="text" id="personal-1-tempat_lahir" class="form-control " name="tempat_lahir"
                                                value="{{$biodata->tempat_lahir}}" placeholder="Isi Tempat Lahir">

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="">Tanggal Lahir:</label>
                                        <div class="form-group  ">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input name="tanggal_lahir" id="datepicker" type="text" class="form-control "
                                                    value="{{$biodata->tanggal_lahir}}">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Jenis Kelamin:</label>

                                        <div class="form-group field-personal-1-jenis_kelamin required">

                                            <select id="personal-1-jenis_kelamin" class="form-control" name="jenis_kelamin">
                                                <option value="">Pilih</option>
                                                <option value="Laki-laki"
                                                    <?php if ($biodata->jenis_kelamin == 'Laki-laki'){ echo ' selected=""'; }?>>Laki-laki
                                                </option>
                                                <option value="Perempuan"
                                                    <?php if ($biodata->jenis_kelamin == 'Perempuan'){ echo ' selected=""'; }?>>Perempuan
                                                </option>
                                            </select>

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label class="">Status Perkawinan:</label>

                                        <div class="form-group field-personal-1-status_perkawinan required">

                                            <select id="personal-1-status_perkawinan" class="form-control" name="status_perkawinan">
                                                <option value="">Pilih</option>
                                                <option value="Belum Menikah" <?php if ($biodata->status_perkawinan == 'Belum
                                                                                            Menikah'){ echo ' selected=""'; }?>>Belum
                                                    Menikah</option>
                                                <option value="Menikah"
                                                    <?php if ($biodata->status_perkawinan == 'Menikah'){ echo ' selected=""'; }?>>Menikah
                                                </option>
                                            </select>

                                            <div class="help-block"></div>
                                        </div>
                                    </div> -->

                                    <!-- <div class="col-lg-4">
                                        <label>Agama:</label>
                                        <div class="form-group field-personal-1-id_agama required">

                                            <select id="personal-1-id_agama" class="form-control" name="agama">
                                                <option value="">Pilih</option>
                                                <option <?php if ($biodata->agama == 'Islam'){ echo ' selected=""'; }?> value="Islam">Islam
                                                </option>
                                                <option <?php if ($biodata->agama == 'Budha'){ echo ' selected=""'; }?> value="Budha">Budha
                                                </option>
                                                <option <?php if ($biodata->agama == 'Hindu'){ echo ' selected=""'; }?> value="Hindu">Hindu
                                                </option>
                                                <option <?php if ($biodata->agama == 'Kristen'){ echo ' selected=""'; }?> value="Kristen">
                                                    Kristen</option>
                                                <option <?php if ($biodata->agama == 'Katholik'){ echo ' selected=""'; }?>
                                                    value="Katholik">Katholik</option>
                                                <option <?php if ($biodata->agama == 'Konghucu'){ echo ' selected=""'; }?>
                                                    value="Konghucu">Konghucu</option>
                                                <option <?php if ($biodata->agama == 'Kepercayaan'){ echo ' selected=""'; }?>
                                                    value="Kepercayaan">Kepercayaan</option>
                                                <option <?php if ($biodata->agama == 'Lain-lain'){ echo ' selected=""'; }?>
                                                    value="Lain-lain">Lain-lain</option> 
                                            </select>

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Pendidikan Terakhir:</label>
                                        <div class="form-group field-personal-1-id_pendidikan required">

                                            <select id="personal-1-id_pendidikan" class="form-control" name="pendidikan_terakhir">
                                                <option value="">Pilih</option>
                                                <option
                                                    <?php if ($biodata->pendidikan_terakhir == 'Belum Sekolah'){ echo ' selected=""'; }?>
                                                    value="Belum Sekolah">Belum Sekolah</option>
                                                <option
                                                    <?php if ($biodata->pendidikan_terakhir == 'Tidak Tamat SD'){ echo ' selected=""'; }?>
                                                    value="Tidak Tamat SD">Tidak Tamat SD</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'TK'){ echo ' selected=""'; }?>
                                                    value="TK">TK</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'SD'){ echo ' selected=""'; }?>
                                                    value="SD">SD</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'SLTP'){ echo ' selected=""'; }?>
                                                    value="SLTP">SLTP</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'SLTA'){ echo ' selected=""'; }?>
                                                    value="SLTA">SLTA</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'D1'){ echo ' selected=""'; }?>
                                                    value="D1">D1</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'D3'){ echo ' selected=""'; }?>
                                                    value="D3">D3</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'D4'){ echo ' selected=""'; }?>
                                                    value="D4">D4</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'S1'){ echo ' selected=""'; }?>
                                                    value="S1">S1</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'S2'){ echo ' selected=""'; }?>
                                                    value="S2">S2</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'S3'){ echo ' selected=""'; }?>
                                                    value="S3">S3</option>
                                                <option <?php if ($biodata->pendidikan_terakhir == 'dr'){ echo ' selected=""'; }?>
                                                    value="dr">dr</option>
                                                <option
                                                    <?php if ($biodata->pendidikan_terakhir == 'Belum Tamat Sekolah'){ echo ' selected=""'; }?>
                                                    value="Belum Tamat Sekolah">Belum Tamat Sekolah</option>
                                            </select>

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div class="form-group m-form__group row">

                                    <div class="col-lg-6">
                                        <label class="">No. HP:</label>
                                        <div class="form-group field-kontakpersonal-1-kontak">

                                            <input type="text" id="kontakpersonal-1-kontak" class="form-control" name="no_hp"
                                                value="{{$biodata->no_hp}}" placeholder="Isi No. HP">

                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        {!! Form::label('img', 'Upload File (KTP/SIM/Passport) max 500 kb', ['class' => 'control-label']) !!}
                                        <img id="uploadPreview"  style="width: 150px;" />
                                        <input class="form-control" id="img" type="file" name="img" onchange="PreviewImage();" />
                                        
                                        <p class="help-block"></p>
                                        @if($errors->has('img'))
                                            <p class="help-block">
                                                {{ $errors->first('img_name') }}
                                            </p>
                                        @endif
                                    </div>
                                
                                </div> -->

                                <!-- <div class="form-group m-form__group row">
                                    <div class="col-lg-6">
                                        <label class="">Pekerjaan:</label>
                                        <div class="form-group field-personal-1-pekerjaan required">
                                            <select id="personal-1-pekerjaan" class="form-control" name="pekerjaan">
                                                <option <?php if ($biodata->pekerjaan == ''){ echo ' selected=""'; }?> value="">Pilih
                                                    Pekerjaan</option>

                                                <option <?php if ($biodata->pekerjaan == 'Akademik/Pengajar'){ echo ' selected=""'; }?>
                                                    value="Akademik/Pengajar">Akademik/Pengajar</option>

                                                <option <?php if ($biodata->pekerjaan == 'Ibu Rumah Tangga'){ echo ' selected=""'; }?>
                                                    value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>

                                                <option <?php if ($biodata->pekerjaan == 'Karyawan BUMN'){ echo ' selected=""'; }?>
                                                    value="Karyawan BUMN">Karyawan BUMN</option>

                                                <option <?php if ($biodata->pekerjaan == 'Karyawan Swasta'){ echo ' selected=""'; }?>
                                                    value="Karyawan Swasta" data-select2-id="6">

                                                    Karyawan Swasta</option>
                                                <option <?php if ($biodata->pekerjaan == 'Lain-lain'){ echo ' selected=""'; }?>
                                                    value="Lain-lain">Lain-lain</option>

                                                <option
                                                    <?php if ($biodata->pekerjaan == 'Pegawai Negeri Sipil (PNS)'){ echo ' selected=""'; }?>
                                                    value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)
                                                </option>

                                                <option <?php if ($biodata->pekerjaan == 'Pelajar / Mahasiswa'){ echo ' selected=""'; }?>
                                                    value="Pelajar / Mahasiswa">Pelajar / Mahasiswa</option>

                                                <option
                                                    <?php if ($biodata->pekerjaan == 'Pemerintahan / Militer'){ echo ' selected=""'; }?>
                                                    value="Pemerintahan / Militer">Pemerintahan / Militer</option>

                                                <option <?php if ($biodata->pekerjaan == 'Pensiunan'){ echo ' selected=""'; }?>
                                                    value="Pensiunan">Pensiunan</option>

                                                <option <?php if ($biodata->pekerjaan == 'Petani'){ echo ' selected=""'; }?>
                                                    value="Petani">Petani</option>
                                                <option <?php if ($biodata->pekerjaan == 'Profesional ( Dokter, Pengacara,
                                                                                            Dll )'){ echo ' selected=""'; }?> value="Profesional ( Dokter, Pengacara,
                                                                                            Dll )">Profesional ( Dokter, Pengacara,
                                                    Dll )</option>

                                                <option <?php if ($biodata->pekerjaan == 'Tidak Bekerja'){ echo ' selected=""'; }?>
                                                    value="Tidak Bekerja">Tidak Bekerja</option>

                                                <option <?php if ($biodata->pekerjaan == 'Wiraswasta'){ echo ' selected=""'; }?>
                                                    value="Wiraswasta">Wiraswasta</option>
                                            </select>

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Alamat tempat tinggal sekarang:</label>
                                        <div class="form-group field-personal-1-alamat_sementara required">

                                            <input type="text" id="personal-1-alamat_sementara" class="form-control" name="alamat"
                                                value="{{$biodata->alamat}}">

                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- INFORMASI MODEL DEPARTEMEN -->
                                <div class="m-form__heading"></div>
                            </div>
                        </div>
                    </div>
                                <!--end::Portlet-->
                                
                                <br>

                                <script>
                                $(document).ready(function() {
                                    var id_kab = '';
                                    var id_kec = '';
                                    $.post("index.php?r=pasien/list_kabupaten&id=" + $('#select-provinsi')
                                    .val(),
                                        function(data) {
                                            $("select#pasien-kabupaten").html(data);
                                            $('#pasien-kabupaten').val(id_kab);
                                            $.post("index.php?r=pasien/list_kecamatan&id=" + id_kab,
                                                function(data) {
                                                    $("select#pasien-kecamatan").html(data);
                                                    $('#pasien-kecamatan').val(id_kec);
                                                }
                                            );
                                        }
                                    );
                                });
                                </script>

                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
$('#datepicker').datepicker({
    format: 'DD-MM-YYYY'
});
</script>
@endsection
@section('scripts')
@parent

@endsection