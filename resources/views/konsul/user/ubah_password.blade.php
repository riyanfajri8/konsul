    @extends('konsul.layouts.app')

    @section('content')
    <style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

.container {
    max-width: 1400px !important;
}

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
    </style>
    <div class="container mt-4" >
        <div id="myCarousel" class="carousel slide" >
            <div calss="row">
            @if(session('success'))
                <div class="alert alert-success">
                    {!!  session('success')  !!}
                </div>
            @endif
                @include('konsul.user.menu')
                <div class="col-xl-9 col-lg-8">
                    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                        
                        <div class="tab-content">
                            <div class="card-body">
                                <form action="{{ route('auth.change_password') }}?psikolog=1" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                                        <label for="current_password">Current password *</label>
                                        <input type="password" id="current_password" name="current_password"
                                            class="form-control" required>
                                        @if($errors->has('current_password'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('current_password') }}
                                        </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                                        <label for="new_password">New password *</label>
                                        <input type="password" id="new_password" name="new_password" class="form-control"
                                            required>
                                        @if($errors->has('new_password'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('new_password') }}
                                        </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('new_password_confirmation') ? 'has-error' : '' }}">
                                        <label for="new_password_confirmation">New password confirmation *</label>
                                        <input type="password" id="new_password_confirmation" name="new_password_confirmation"
                                            class="form-control" required>
                                        @if($errors->has('new_password_confirmation'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('new_password_confirmation') }}
                                        </em>
                                        @endif
                                    </div>
                                    <div>
                                        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @endsection
@section('scripts')
@parent

@endsection