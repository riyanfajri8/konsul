@extends('konsul.layouts.app')

@section('content')

<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}
/* .container { max-width: 1400px !important; } */

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
    border-radius: 100px;
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: #555;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
    
    <div class="container mt-4" >
        <div class="row" style="margin-top:150px;">
            @include('konsul.user.menu')
            <div class="col-md-9">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                role="tablist">
                            </ul>
                        </div>
                        <div class="m-portlet__head-tools">
                        </div>
                        <!-- start head tools portlet -->
                        <!-- end head tools portlet -->
                    </div>
                    <div class="tab-content">
                        <div class="container mt-4" id="laporancuti">


                            <div class="site-index">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-9" style="color:#555;">
                                            <h1 style="font-weight: bold">PsyQonsul | Beranda</h1>

                                            <i class="lead">Integrated Konsultasi Information System</i>
                                            <hr style="margin-left:0;">
                                            Selamat Petang, <b>{{ Auth::user()->name }}</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection