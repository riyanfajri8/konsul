@extends('konsul.layouts.app')

@section('content')
<?php

use App\Lib;
?>
<style>
.wrn {
    color: blue;
}

.gambar {
    position: relative;
    width: 100%;
}

/* .container {
    max-width: 1400px !important; */
}

.image {
    display: block;
    width: 100%;
    height: auto;
}

.overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    background-color: rgba(121, 0, 0, 0.1);
}

.gambar:hover .overlay {
    opacity: 1;
}

.text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
}
</style>
<div class="container mt-4" >
    <div id="myCarousel" class="carousel slide" >
        <div class="row" style="margin-top:150px;">

            @include('konsul.user.menu')
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                role="tablist">
                            </ul>
                        </div>
                        <div class="m-portlet__head-tools">

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a class="btn btn-success"
                                            href="{{url('/klien/update-profil')}}"><i
                                                class="fa fa-edit"></i> Update</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <!-- start head tools portlet -->
                        <!-- end head tools portlet -->
                    </div>
                    <div class="tab-content">
                        <div class="container mt-4" id="laporancuti">

                            <style>
                            table tr th {
                                background: white !important;
                            }
                            </style>
                            <div class="pegawai-view">

                                <div class="row">
                                    
                                    <div class="col-xl-12 col-lg-12">
                                        <!-- <div class="row">
                                            <div class="col-xl-12 col-lg-12">
                                            @if(session('success'))
                                                <div class="alert alert-success">
                                                    {!!  session('success')  !!}
                                                </div>
                                            @endif
                                                
                                                <div class="m-portlet">
                                                    <div class="m-portlet__body m-portlet__body--no-padding">
                                                        <div class="row m-row--no-padding m-row--col-separator-xl">

                                                            <div class="col-md-12 col-lg-12 col-xl-5">

                                                                
                                                                <div class="m-widget1">
                                                                    <div class="m-widget1__item">
                                                                        <div
                                                                            class="row m-row--no-padding align-items-center">
                                                                            <div class="col">
                                                                                <h3 class="m-widget1__title">Nama
                                                                                    Lengkap / Email Login :</h3>
                                                                                <span
                                                                                    class="m-widget1__number m--font-warning ">{{ Auth::user()->name }} / {{ Auth::user()->email }}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="m-widget1__item">
                                                                        <div
                                                                            class="row m-row--no-padding align-items-center">
                                                                            <div class="col">
                                                                                <h3 class="m-widget1__title">Gender /
                                                                                    Tgl Lahir</h3>
                                                                                <span
                                                                                    class="m-widget1__number m--font-success ">{{$biodata->jenis_kelamin}}
                                                                                    /{{date('d-M-Y', strtotime($biodata->tanggal_lahir))}} </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                
                                                            </div>
                                                            <div class="col-md-12 col-lg-12 col-xl-7">

                                                                
                                                                <div class="m-widget1">
                                                                    <div class="m-widget1__item">
                                                                        <div
                                                                            class="row m-row--no-padding align-items-center">
                                                                            <div class="col">
                                                                                <h3 class="m-widget1__title">Alamat:
                                                                                </h3>
                                                                                <span
                                                                                    class="m-widget1__number m--font-warning ">{{$biodata->alamat}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="m-widget1__item">
                                                                        <div
                                                                            class="row m-row--no-padding align-items-center">
                                                                            <div class="col">
                                                                                <h3 class="m-widget1__title">Nomor HP:
                                                                                </h3>
                                                                                <span
                                                                                    class="m-widget1__number m--font-success ">{{$biodata->no_hp}}</span>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="m-widget1__item">
                                                                        <div
                                                                            class="row m-row--no-padding align-items-center">
                                                                            <div class="col">
                                                                                <h3 class="m-widget1__title">No.
                                                                                    Identitas KTP :</h3>
                                                                                <span
                                                                                    class="m-widget1__number m--font-success ">{{$biodata->no_identitas}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                
                                                            </div>
                                                            <div class="col-md-12 col-lg-12 col-xl-3">

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                

                                            </div>
                                        </div> -->

                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-xl-12 col-lg-12">
                                                    @if(session('success'))
                                                        <div class="alert alert-success">
                                                            {!!  session('success')  !!}
                                                        </div>
                                                    @endif
                                                <!--begin::Portlet-->
                                                <div class="m-portlet m-portlet--tabs">
                                                
                                                    <div class="m-portlet__body">
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="m_tabs_6_1"
                                                                role="tabpanel">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="m-section">
                                                                            <div class="m-section__content">
                                                                                <table
                                                                                    class="table m-table m-table--head-separator-secondary">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Nama Lengkap</th>
                                                                                            <th>:</th>
                                                                                            <th>{{$biodata->nama}}</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Email</th>
                                                                                            <th>:</th>
                                                                                            <th>
                                                                                            {{ Auth::user()->email }}
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Nomor
                                                                                                HP</th>
                                                                                            <th>:
                                                                                            </th>
                                                                                            <th>
                                                                                            {{$biodata->no_hp}} </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Usia</th>
                                                                                            <th>:</th>
                                                                                            <th>
                                                                                            27 tahun </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <!-- <thead>
                                                                                        <tr>
                                                                                            <th class="text-right">Nomor Identitas</th>
                                                                                            <th class="text-center">:
                                                                                            </th>
                                                                                            <th class="text-left">{{$biodata->no_identitas}}</th>
                                                                                        </tr>
                                                                                    </thead> -->
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Jenis
                                                                                                Kelamin</th>
                                                                                            <th>:
                                                                                            </th>
                                                                                            <th>
                                                                                            {{$biodata->jenis_kelamin}}</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <!-- <thead>
                                                                                        <tr>
                                                                                            <th class="text-right">
                                                                                                Tempat &amp; Tgl Lahir
                                                                                            </th>
                                                                                            <th class="text-center">:
                                                                                            </th>
                                                                                            <th class="text-left">{{$biodata->tanggal_lahir}} ,
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead> -->
                                                                                    
                                                                                    <!-- <thead>
                                                                                        <tr>
                                                                                            <th class="text-right">Agama
                                                                                            </th>
                                                                                            <th class="text-center">:
                                                                                            </th>
                                                                                            <th class="text-left">{{$biodata->agama}}
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead> -->
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div class="m-section">
                                                                            <div class="m-section__content">
                                                                                <table
                                                                                    class="table m-table m-table--head-separator-secondary">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                Status Perkawinan</th>
                                                                                            <th>:
                                                                                            </th>
                                                                                            <th>{{$biodata->status_perkawinan}}</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                Pendidikan Terakhir</th>
                                                                                            <th>:
                                                                                            </th>
                                                                                            <th>{{$biodata->pendidikan_terakhir}}
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                Pekerjaan</th>
                                                                                            <th>:
                                                                                            </th>
                                                                                            <th>
                                                                                            {{$biodata->pekerjaan}}</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                Kota tempat tinggal saat ini</th>
                                                                                            <th>:
                                                                                            </th>
                                                                                            <th>{{$biodata->alamat}}</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            
                                                            <div class="tab-pane" id="m_tabs_6_3" role="tabpanel">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="m-section">
                                                                            <div class="m-section__content">
                                                                                <table
                                                                                    class="table m-table m-table--head-separator-secondary">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th class="text-right">
                                                                                                Pekerjaan</th>
                                                                                            <th class="text-center">:
                                                                                            </th>
                                                                                            <th class="text-left">
                                                                                            {{$biodata->pekerjaan}}</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Portlet-->
                                            </div>
                                        </div><!-- end row -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
@parent

@endsection